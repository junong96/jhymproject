<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h1>DB 데이터 등록</h1>
<hr>
<h1>종목 데이터 등록</h1>
 <div data-type="stock">
	<div class="form-group">
		<label for="stockfile">
			<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;&nbsp;첨부 파일(UTF-8 로 저장된 .txt, .tsv, .csv 파일)
		</label>
		<input type="file" id="stockFile">
		<button type="button" class="btn btn-default deletefile" style="display:none">
			파일 삭제
		</button>
	</div>
	
	<button type="button" class="btn btn-default subStock subFile">
		<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>&nbsp;&nbsp;발송
	</button>
	
	<div class="alert" role="alert" style="display: none;"></div>
 </div>
 
<hr>
<h1>매매 데이터(수량) 등록</h1>
 <div data-type="investorAmount">
	<div class="form-group">
		<label for="investorFile">
			<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;&nbsp;첨부 파일(UTF-8 로 저장된 .txt, .tsv, .csv 파일)
		</label>
		<input type="file" id="investorFile">
		<button type="button" class="btn btn-default deletefile" style="display:none">
			파일 삭제
		</button>
	</div>
	<div>
		<p><h5 style="color:red;">DB에 저장된 데이터가 많아질수록 로딩이 오래 지속될 수 있습니다.</h5><br>날짜를 선택해주세요. 선택하지 않을 시 오늘 날짜로 데이터가 입력 됩니다.<br>동일한 날짜에 기존 데이터가 존재 할 시 기존 데이터는 삭제되고 첨부 파일의 데이터로 새로 등록되어집니다.</p>
		<input type="date" id="dateStock">
	</div>
	<button type="button" class="btn btn-default subInvestor subFile">
		<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>&nbsp;&nbsp;발송
	</button>
	
	<div class="alert" role="alert" style="display: none;"></div>
 </div>
 
 <hr>
<h1>매매 데이터(금액) 등록</h1>
 <div data-type="investorPrice">
	<div class="form-group">
		<label for="investorFile">
			<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;&nbsp;첨부 파일(UTF-8 로 저장된 .txt, .tsv, .csv 파일)
		</label>
		<input type="file" id="investorFile">
		<button type="button" class="btn btn-default deletefile" style="display:none">
			파일 삭제
		</button>
	</div>
	<div>
		<p><h5 style="color:red;">! 매매 데이터(수량)을 먼저 등록한 후 매매 데이터(금액) 을 등록해주세요. 순서가 바뀔 시 매매 데이터(금액)이 등록 동작이 무시됩니다.</h5><br> 날짜를 선택해주세요. 선택하지 않을 시 오늘 날짜로 데이터가 입력 됩니다.<br>동일한 날짜에 기존 데이터가 존재 할 시 기존 데이터는 삭제되고 첨부 파일의 데이터로 새로 등록되어집니다.</p>
		<input type="date" id="dateStock">
	</div>
	<button type="button" class="btn btn-default subInvestor subFile">
		<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>&nbsp;&nbsp;발송
	</button>
	
	<div class="alert" role="alert" style="display: none;"></div>
 </div>
 
 
 <hr>
<h1>오늘의 상승 키워드 데이터 등록</h1>
 <div data-type="todayKeyword">
	<div class="form-group">
		<label for="todayKeywordFile">
			<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;&nbsp;첨부 파일(UTF-8 로 저장된 .txt, .tsv, .csv 파일)
		</label>
		<input type="file" id="todayKeywordFile">
		<button type="button" class="btn btn-default deletefile" style="display:none">
			파일 삭제
		</button>
	</div>
	<div>
		<p>날짜를 선택해주세요. 선택하지 않을 시 오늘 날짜로 데이터가 입력 됩니다.<br>동일한 날짜에 기존 데이터가 존재 할 시 기존 데이터는 삭제되고 첨부 파일의 데이터로 새로 등록되어집니다.</p>
		<input type="date" id="dateStock">
	</div>
	<button type="button" class="btn btn-default subTodayKeyword subFile">
		<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>&nbsp;&nbsp;발송
	</button>
	
	<div class="alert" role="alert" style="display: none;"></div>
 </div>
 
 
 <hr>
<h1>시간외 상승 키워드 데이터 등록</h1>
 <div data-type="overtimeKeyword">
	<div class="form-group">
		<label for="overtimeKeywordFile">
			<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;&nbsp;첨부 파일(UTF-8 로 저장된 .txt, .tsv, .csv 파일)
		</label>
		<input type="file" id="overtimeKeywordFile">
		<button type="button" class="btn btn-default deletefile" style="display:none">
			파일 삭제
		</button>
	</div>
	<div>
		<p>날짜를 선택해주세요. 선택하지 않을 시 오늘 날짜로 데이터가 입력 됩니다.<br>동일한 날짜에 기존 데이터가 존재 할 시 기존 데이터는 삭제되고 첨부 파일의 데이터로 새로 등록되어집니다.</p>
		<input type="date" id="dateStock">
	</div>
	<button type="button" class="btn btn-default subOvertimeKeyword subFile">
		<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>&nbsp;&nbsp;발송
	</button>
	
	<div class="alert" role="alert" style="display: none;"></div>
 </div>
 
 