<%--
    작성자 : 이창신(ycs318@naver.com)
    작성일 : 2018. 9. 14.
    설명 : 관리자 페이지에서 상황에 맞는 모달창을 추가하는 모듈
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% request.setCharacterEncoding("UTF-8"); %>

