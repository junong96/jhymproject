
<%@page import="util.CONFIG_VALUE"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);
%>

<html>
<head>
<title>주식 서비스 관리자 페이지</title>
<style type="text/css">
table {
	text-align: center;
	margin: auto;
	padding-top: 15%;
}
</style>
</head>
<body>
	<table>
		<tbody>
			<tr>
				<td><p>주식 서비스 관리자 페이지</p></td>
			</tr>
			<tr>
				<td class="input_area"><input id="id" type="text"
					placeholder="아이디" tabindex="1"></td>
				<td rowspan="2" class="button_area"><input class="loginBtn"
					type="button" value="로그인" tabindex="3"></td>
			</tr>
			<tr>
				<td class="input_area"><input id="password" type="password"
					placeholder="비밀번호" tabindex="2"></td>
			</tr>
		</tbody>
	</table>

	<script src="${values.getJQUERY_URL()}"></script>
	<script src="../js/values.js?v=1.3"></script>
	<script src="../js/admin/login/Account.js"></script>
	<script>
		var account = new Account();
	</script>
</body>
</html>