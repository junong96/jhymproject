<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">종목 노트 관리(관리자)</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 20px;"><label for="stockName">검색
								: </label> <input type="text" id="stockName" placeholder="검색어 입력"><br>
							<select name="stockList" multiple size="100" id="stockList"
							style="width: 250px; height: 600px;">
						</select></td>
						<td style="padding: 0 30px 0 30px;">종목 노트<br>
						<select name="stockNoteList" multiple size="100"
							id="stockNoteList" style="width: 250px; height: 400px;">
						</select></td>
						<td style="padding: 20px">
							<table>
								<tr>
									<td style="padding-bottom: 15px;">제목 <br>
									<input type="text" id="title" placeholder="제목을 입력해주세요."
										size="50"></td>
								</tr>
								<tr>
									<td style="padding-bottom: 15px;">내용 <br>
									<textarea id="contents" placeholder="내용을 입력해주세요." cols="50"
											rows="25"></textarea></td>
								</tr>
							</table> <br>
						<button id="insertBtn" style="margin: 10px; width: 50px;">등록</button>&nbsp;
							<button id="modifyBtn" style="margin: 10px; width: 50px;">수정</button>&nbsp;
							<button id="deleteBtn" style="margin: 10px; width: 50px;">삭제</button>
						</td>

					</tr>

					<tr>
						<td colspan=3>
							<p style="text-align: left; margin-top: 20px;">
								<span
									style="font-size: 24px; margin-top: 20px; margin-bottom: 10px;">설명</span><br>
								1. 종목을 선택 하면 해당 종목에 등록되어 있는 관리자가 등록한 종목 노트(종목 정보)가 표시됩니다.<br>
								2. 종목 노트에서 특정 노트 선택 시 우측에 제목, 내용이 나타납니다.<br><br>
								  
								등록 - 종목에서 특정 노트 선택 -> 제목, 내용 작성 후 등록버튼클릭 시 새 노트가 작성됩니다.<br>
								수정 - 종목에서 특정 노트 선택 -> 종목 노트에서 특정 노트 선택 -> 내용 수정 후 수정 버튼 클릭 시 해당 노트가 수정됩니다.<br>
								삭제 - 종목에서 특정 노트 선택 -> 종목 노트에서 특정 노트 선택 -> 삭제 버튼 클릭 시 해당 노트가 삭제됩니다.<br>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>