<%--
    작성자 : 이창신(ycs318@naver.com)
    작성일 : 2018. 9. 14.
    설명 :
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="sideGroup">
<div class="clickable stockKeyword"> <a href="?p=stockKeyword">키워드 관리(종목-키워드)</a></div>
<div class="clickable keywordStock"> <a href="?p=keywordStock">키워드 관리(키워드-종목)</a></div>
<div class="clickable keywordAddRequest"> <a href="?p=keywordAddRequest">키워드 추가 요청 관리</a></div>
</div>

<div class="sideGroup">
<div class="clickable insertStock"> <a href="?p=insertStock">종목 관리</a></div>
<div class="clickable stockNote"> <a href="?p=stockNote">종목 노트 관리</a></div>
<div class="clickable fileManage"> <a href="?p=fileManage">DB 데이터 등록</a></div>
</div>
 <script src="${values.getJQUERY_URL()}"></script>
