<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">키워드 관리(키워드-종목)</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 30px;">
							<h3>키워드 항목</h3> <label for="keywordList">검색 : </label> <input
							type="text" id="keywordName" placeholder="검색어 입력"><br>
							<select name="keywordList" multiple size="100" id="keywordList"
							style="width: 250px; height: 600px;">
						</select> <br>
							<button id="keywordDeleteBtn"
								style="padding: 5px; margin-top: 15px; width: 50px;">
								삭제</button>
						</td>

						<td style="padding-right: 30px;">
							<h3>선택된 종목</h3> <select name="selectStockList" multiple
							size="100" id="selectStockList"
							style="width: 250px; height: 300px;">
						</select> <br>
							<button id="clearBtn" style="padding: 5px; margin-top: 15px;">
								비우기</button>
							<h3>매핑된 키워드</h3> <select name="mappingKeyword" multiple
							size="100" id="mappingKeyword"
							style="width: 250px; height: 200px;">
						</select> <br> <br>
							<div class="autocomplete">
								<label for="insertKeyword"> 키워드 추가 : </label> <input type="text"
									id="insertKeyword">
							</div> <br>
							<p style="margin-top: 15px;">
								<button id="totalInsertBtn" style="padding: 5px; margin: 10px;">저장
									[전체]</button>
								&nbsp;&nbsp;
								<button id="selectedInsertBtn"
									style="padding: 5px; margin: 10px;">저장 [선택]</button>
							</p>
							<p style="margin-top: 15px;">
								<button id="totalDeleteBtn" style="padding: 5px; margin: 10px;">삭제
									[전체]</button>
								&nbsp;&nbsp;
								<button id="selectedDeleteBtn"
									style="padding: 5px; margin: 10px;">삭제 [선택]</button>
							</p>
						</td>

						<td style="padding-right: 30px;">
							<h3>종목 리스트</h3> <label for="stockName">검색 : </label> <input
							type="text" id="stockName" placeholder="검색어 입력"><br>
							<select name="stockList" multiple size="100" id="stockList"
							style="width: 250px; height: 600px;">
						</select>
						</td>
					</tr>
					<tr>
						<td colspan=5>
							<p style="text-align: left; margin-top: 20px;">
								<span
									style="font-size: 24px; margin-top: 20px; margin-bottom: 10px;">설명</span><br>
								1. 키워드 목록 에서 특정 키워드 선택 시 해당 키워드와 매핑되어 있는 종목 목록이 선택된 종목에 표시됩니다.<br><br>
								2. 선택된 종목 에서 특정 종목 선택 시 해당 종목에 매핑되어 있는 키워드 목록이 매핑된 키워드에 표시됩니다.<br><br>
								3. 선택된 종목 에서 특정 종목을 더블 클릭하여 선택을 해제할 수 있습니다.<br><br>
								4. 키워드 추가에
								키워드 입력 후 엔터 버튼 입력 시 매핑 시킬 키워드 목록에 추가 시킬 수 있습니다. (DB에 바로 추가되지
								않습니다. 저장 버튼 클릭 시에만 DB에 추가되어집니다.)<br> 매핑 시킬 키워드 목록에 직접 추가한
								키워드에 한해서 더블클릭으로 매핑 시킬 키워드 목록에서 제외시킬 수 있습니다. <br> <br>
								저장 [전체] -선택된 종목에 존재하는 모든 종목에 대하여 매핑된 키워드에 존재하는 키워드 를 매핑 시킵니다.<br><br>
								저장 [선택] - 선택된 종목에 선택 되어 있는 종목에 대하여 매핑된 키워드에 존재하는 키워드 를 매핑 시킵니다.<br><br>
								삭제 [전체] - 선택된 종목에 존재하는 모든 종목에 대하여 매핑된 키워드에 선택된 키워드 만 매핑 해제 시킵니다.<br><br>
								삭제 [선택] - 선택된 종목에 선택 되어 있는 종목에 대하여 매핑된 키워드에 선택된 키워드 만 매핑 해제
								시킵니다.<br>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>