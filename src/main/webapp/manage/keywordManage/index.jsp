<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">키워드 관리(종목-키워드)</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 30px;">
							<h3>종목 리스트</h3> <label for="stockName">검색 :
						</label> <input type="text" id="stockName" placeholder="검색어 입력"><br>
							<select name="stockList" multiple size="100" id="stockList"
							style="width: 250px; height: 600px;">

						</select>
						</td>
						<td style="padding-right: 30px;">
							<h3>선택된 종목</h3> <select name="selectStockList" multiple
							size="100" id="selectStockList"
							style="width: 250px; height: 550px;">
						</select> <br>
							<button id="clearBtn" style="margin: 10px; width: 100px;">비우기</button>
						</td>
						<td style="padding-right: 30px;">
							<h3>매핑된 키워드</h3> <select name="keywords" multiple size="30"
							id="keywords" style="width: 250px; height: 200px;">
						</select>
							<br>
							<button id="deleteBtn" style="padding: 5px; margin:10px;">삭제</button>
							<br>
							<h3>추가 할 키워드</h3> <select name="keywordList" multiple
							size="100" id="keywordList"
							style="width: 250px; height: 200px;">
						</select> <br> <br> 
						<div class="autocomplete">
						<label for="insertKeyword"> 키워드 추가 :
						</label> <input type="text" id="insertKeyword"> 
						</div><br>
							<button id="insertBtn" style="padding: 5px; margin:10px;">저장</button> <br> <br> <br>
						</td>
	
							
						<td style="padding-left: 50px;">
							<h3>종목/키워드 상세보기</h3>
							<select name="selectStockKeywordView" size="100"
							id="selectStockKeywordView"
							style="width: 800px; height: 600px; overflow: scroll;">

						</select>
						</td>
					</tr>
					<tr>
						<td colspan=5>
							<p style="text-align: left; margin-top: 20px;">
								1. 종목을 더블클릭 하면 선택된 종목에 추가되게 됩니다.<br> <br>
								2. 선택된 종목에서 종목 더블 클릭시 선택이 해제됩니다.<br> <br>
								3. 선택된 종목에 종목이 있는 상태에서 키워드 추가에 키워드를 입력 후 엔터 버튼 입력 시 추가 할 키워드 목록에 키워드가 추가되어지고,<br>
								이후 저장 버튼 클릭 시 추가 할 키워드 목록에 있는 키워드들이 선택된 종목에 있는 모든 종목들에 추가됩니다.<br><br>
								4. 선택된 종목에 종목이 있는 상태에서 매핑된 키워드에 키워드 선택 후 삭제 시 해당 종목들에 키워드
								연결이 해제됩니다. // ctrl, shift 키로 다중선택 가능<br>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>