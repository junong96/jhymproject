<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr()); pageContext.setAttribute("values", configValues);%>

<%
	Boolean isLogin = (Boolean) session.getAttribute("isLogin");
	Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");
	String nickname = (String) session.getAttribute("nickname");
	Integer id = (Integer) session.getAttribute("id");
	
	if(isAdmin == null || isAdmin == false){
		response.sendRedirect("index.jsp");
	}
	
	pageContext.setAttribute("id", id);
	pageContext.setAttribute("nickname", nickname);
	pageContext.setAttribute("isAdmin", isAdmin);
	pageContext.setAttribute("isLogin", isLogin);
%>

<html>
<head>
	<title>Stock</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="../css/stock/main.css?v=1.7">
	<%@ include file="cssSelector.jsp" %>
</head>
<body>
<header>Stock</header>
<%@ include file="modalSelector.jsp" %>
<section class="body">
	<section class="side_menu">
		<%@ include file="sideMenu.jsp" %>
	</section>
	<section class="contents">
		<%@ include file="contentsSelector.jsp" %>
	</section>
</section>

<script src="${values.getJQUERY_URL()}"></script>
<!-- <script src="${values.getJQUERY_UI_URL()}"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
<script src="../js/lib/jquery.blockUI.min.js"></script>
<script src="../js/values.js?v=1.1"></script>
<script>
	var userID = "${id}";
</script>
<%@ include file="jsSelector.jsp" %>
</body>
</html>