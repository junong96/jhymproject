<%--
    작성자 : 이창신(ycs318@naver.com)
    작성일 : 2018. 9. 14.
    설명 : 관리자 페이지에서 상황에 맞는 기본내용을 보여주는 모듈
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% request.setCharacterEncoding("UTF-8"); %>

<c:choose>
	<c:when test="${param.p eq 'fileManage'}"> <c:import url="fileManage/index.jsp"></c:import> </c:when>
	<c:when test="${param.p eq 'stockKeyword'}"> <c:import url="keywordManage/index.jsp"></c:import> </c:when>
	<c:when test="${param.p eq 'keywordStock'}"> <c:import url="keywordManage2/index.jsp"></c:import> </c:when>
	<c:when test="${param.p eq 'insertStock'}"> <c:import url="insertStock/index.jsp"></c:import> </c:when>
	<c:when test="${param.p eq 'stockNote'}"> <c:import url="stockNote/index.jsp"></c:import> </c:when>
	<c:when test="${param.p eq 'keywordAddRequest'}"> <c:import url="keywordAddRequest/index.jsp"></c:import> </c:when>	
	<c:otherwise>
		<div>좌측의 사이드 메뉴에서 메뉴를 선택해주세요.</div>
	</c:otherwise>
</c:choose>