<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">키워드 추가 요청 관리</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 20px;"><label for="stockName">검색
								: </label> <input type="text" id="stockName" placeholder="검색어 입력"><br>
							<select name="stockList" multiple size="100" id="stockList" 
							style="width: 250px; height: 600px;">
						</select></td>
						<td style="padding: 0 30px 0 30px;">요청 키워드<br> <select
							name="keywordAddRequestList" multiple size="50" id="keywordAddRequestList"
							style="width: 250px; height: 400px;">
						</select><br>
							<button id="insertBtn" style="margin: 10px; width: 50px;">등록</button>&nbsp;
							<button id="deleteBtn" style="margin: 10px; width: 50px;">삭제</button>
						</td>
					</tr>
					<tr>
						<td colspan=3>
							<p style="text-align: left; margin-top: 20px;">
								<span
									style="font-size: 24px; margin-top: 20px; margin-bottom: 10px;">설명</span><br>
								1. 종목을 선택 하면 해당 종목에 대하여 추가 요청 들어온 키워드 목록이 나타납니다.<br>
								2. 요청 키워드에서 키워드 선택 후 등록 시 해당 종목에 키워드들이 등록 되어지고, 등록된 키워드 들은 요청 키워드에서 삭제 되어집니다.<br>  
								3. 요청 키워드에서 키워드 선택 후 삭제 시 해당 종목에 대한 선택한 키워드의 요청을 삭제합니다.<br>
									
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>