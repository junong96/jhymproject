<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">종목 관리</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 30px;"><label for="stockName">검색
								: </label> <input type="text" id="stockName" placeholder="검색어 입력"><br> <select
							name="stockList" multiple size="100" id="stockList"
							style="width: 250px; height: 600px;">
						</select></td>
						<td style="padding : 20px">
							<table>
								<tr >
									<td style="padding-bottom:15px;">종목 <br><input type="text" id="name" placeholder="종목명" ></td>
								</tr>
								<tr>
									<td style="padding-bottom:15px;">코드 <br><input type="text" id="code" placeholder="영문자, 숫자 6자 조합" maxlength="6" size="20"></td>
								<tr>
									<td style="padding-bottom:15px;">업종 <br><textarea cols="20" rows="10" id="sectors" placeholder="종목을 적어주세요."></textarea></td>
								</tr>
								<tr>
									<td style="padding-bottom:15px;">주요품목 <br><textarea cols="20" rows="10" id="product" placeholder="주요 품목을 적어주세요. "></textarea></td>
								</tr>
								<tr>
									<td style="padding-bottom:15px;">타입 <br><select id="type"><option value="KP">코스피</option><option value="KD">코스닥</option></select>
								</tr>
							</table>
							<br><button id="insertBtn" style="margin:10px; width:50px;">등록</button>&nbsp;<button id="deleteBtn" style="margin:10px; width:50px;">삭제</button>
						</td>
						<td style="padding-left: 30px;">
						DB에 없는 종목<br><select
							name="noneExistStockList" multiple size="100"
							id="noneExistStockList" style="width: 250px; height: 300px;">
						</select></td>
					</tr>
					<tr>
						<td colspan=3>
							<p style="text-align: left; margin-top: 20px;">
								<span style="font-size: 24px; margin-top: 20px; margin-bottom: 10px;">설명</span><br>
								1. DB에 없는 종목 - 매매 파일 업로드 시 DB에 존재하지 않았던 종목들 목록이 표시됩니다.<br>
								2. 등록 시 종목, 코드, 타입은 필수로 입력해야 합니다.<br>
								3. 좌측 종목 리스트 클릭시 종목의 정보가 표시됩니다. 내용을 수정하여 등록 버튼 클릭 시 업데이트 됩니다.<br>
								4. 기존 종목 수정 시 종목, 코드중 1가지만 수정하여 등록 시에는 업데이트 되어지나 두 정보 모두 같이 수정하여 등록 시에는 새롭게 추가되어집니다.<br> 
							</p> 
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>