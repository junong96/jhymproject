<%--
    작성자 : 이창신(ycs318@naver.com)
    작성일 : 2018. 9. 14.
    설명 : 관리자 페이지의 상황에 맞는 JS파일을 추가하는 모듈
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% request.setCharacterEncoding("UTF-8"); %>

<script src="../js/lib/jquery.blockUI.min.js"></script>



<c:choose>
	<c:when test="${param.p eq 'stockList'}">
	</c:when>
	<c:when test="${param.p eq 'stockKeyword'}">
		<script src="../js/admin/keywordManage/KeywordManage.js"></script>
		<script src="../js/admin/autocomplate.js"></script>
		<script src="../js/functions.js"></script>
		<script type="text/javascript">
			var keywordManage = new KeywordManage();
		</script>
	</c:when>
	<c:when test="${param.p eq 'keywordStock'}">
		<script src="../js/admin/keywordManage2/KeywordManage2.js"></script>
		<script src="../js/functions.js"></script>
		<script src="../js/admin/autocomplate.js"></script>
		<script type="text/javascript">
			var keywordManage2 = new KeywordManage2();
		</script>
	</c:when>
	<c:when test="${param.p eq 'fileManage'}">
		<script src="../js/admin/fileManage/FileManage.js"></script>
		<script src="../js/functions.js"></script>
		<script type="text/javascript">
			var fileManage = new FileManage();
		</script>
	</c:when>
	<c:when test="${param.p eq 'insertStock'}">
		<script src="../js/admin/insertStock/InsertStock.js"></script>
		<script src="../js/functions.js"></script>
		<script type="text/javascript">
			var insertStock = new InsertStock();
		</script>
	</c:when>
	<c:when test="${param.p eq 'stockNote'}">
		<script src="../js/admin/stockNote/StockNote.js"></script>
		<script src="../js/functions.js"></script>
		<script type="text/javascript">
			var stockNote = new StockNote();
		</script>
	</c:when>
	<c:when test="${param.p eq 'keywordAddRequest'}">
		<script src="../js/admin/keywordAddRequest/KeywordAddRequest.js"></script>
		<script src="../js/functions.js"></script>
		<script type="text/javascript">
			var keywordAddRequest = new KeywordAddRequest();
		</script>
	</c:when>
</c:choose>
