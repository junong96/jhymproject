<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);

Boolean isLogin = (Boolean) session.getAttribute("isLogin");
Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");
String nickname = (String) session.getAttribute("nickname");
%>
<c:import url="common/checkStatus.jsp"></c:import>
<html lang="ko">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>주식 정보 project</title>
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap"
	rel="stylesheet">
<link href="../css/base.css" rel="stylesheet">
<link href="../css/index.css" rel="stylesheet">
<link href="../css/supplyNdemand.css" rel="stylesheet">
</head>

<body>
	<div id="wrap">
		<c:import url="common/menu.jsp">
			<c:param name="type" value="supplyNdemand"></c:param>
		</c:import>
		<!-- gnb -->

		<div class="popupMask"></div>
		<!--
        21.04.20
        popupMask 터치 시, .popupMemo{display:none;}
        -->
		<div class="popupWrap popupMemo">
			<h3 class="popupTitle">메모 등록</h3>
			<div class="close">
				<img src="../images/ico_close.png" alt="팝업 닫기 버튼">
			</div>
			<!--
            21.04.22
            .close 버튼 터치 시 팝업창 닫기
            -->
			<div class="popupContent">
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="제목을 입력해 주세요."
						value="">
				</div>
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="내용을 입력해 주세요."
						value="">
				</div>
				<!-- inputKeyword -->
				<button>등록</button>
			</div>
			<!-- popupContent -->
		</div>
		<!-- popupMemo -->

		<div class="popupWrap popupUrl">
			<h3 class="popupTitle">링크 등록</h3>
			<div class="close">
				<img src="../images/ico_close.png" alt="팝업 닫기 버튼">
			</div>
			<!--
            21.04.22
            .close 버튼 터치 시 팝업창 닫기
            -->
			<div class="popupContent">
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="제목을 입력해 주세요."
						value="">
				</div>
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="url을 추가해 주세요."
						value="">
				</div>
				<!-- inputKeyword -->
				<button>등록</button>
			</div>
			<!-- popupContent -->
		</div>
		<!-- popupUrl -->

		<div class="right">
			<header id="header">
				<div class="inner">
					<h2 class="pageTit">수급 포착</h2>
					<c:import url="common/profile.jsp"></c:import>
					<!-- utill -->
				</div>
				<!--inner-->
			</header>

			<div id="container">
				<div class="inner">
					<div class="search">
						<!-- filter -->
						<div class="datePicker">
							<img src="../images/ico_left.png" class="before" alt="왼쪽 화살표 버튼">
							<span class="current_date"></span> <img
								src="../images/ico_right.png" class="after" alt="오른쪽 화살표 버튼">
							<!--
                                21.04.20 
                                .before 버튼 터치 시, 전 날 날짜로 이동
                                .after 버튼 터치 시, 다음 날 날짜로 이동
                                -->
						</div>
						<!-- datePicker -->
					</div>
					<!-- searchTab -->
				</div>
				<!-- search -->
			</div>
			<!-- inner -->
			<div class="content">
				<div class="backG">
					<div class="wBox">
						<div class="con_01"></div>
						<!-- con_01 -->
					</div>
					<!-- wBox -->
					<div class="con_02">
						<div class="memoTab">
							<ul>
								<li class="infoTab" style="display: none;">종목 정보</li>
								<li class="myMemoTab on">내 메모</li>
							</ul>
						</div>
						<!--
                            21.05.07
                            관리자가 입력한 종목 정보가 없을 경우, 
                            .memoTab ul li.infoTab{display: none;}, 
                            <li class="memoTab">내 메모</li> 클래스에 on 추가
                            -->
						<!-- memoTab -->
						<div class="wBoxWrap">
							<div class="adminInfoWrap"></div>
							<!-- adminInfoWrap -->
							<!--
                                21.05.07
                                관리자가 작성한 종목 정보
                                <li class="infoTab">종목 정보</li> 클래스에 on 추가 시
                                -->
							<div class="myMemoWrap">
								<div class="gBox">
									<!--21.05.14 wBox에서 gBox로 수정-->
									<div class="myMemo memoNodata">
										<p>오른쪽의 [+] 버튼을 클릭해 해당 종목에 대한 메모/url을 둥록해 주세요.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- myMemoWrap -->
						<!--
                                <li class="memoTab">내 메모</li> 클래스에 on 추가 시
                                -->
					</div>
					<!-- con_02 -->
					<!--
                        21.05.14
                        메모 구조 추가
                        -->
				</div>
				<!-- back -->
			</div>
			<!-- content -->
			<c:import url="common/footer.jsp"></c:import>
		</div>
		<!-- container -->
	</div>

	<div class="fab">
		<span class="fab-action-button"> <i
			class="fab-action-button__icon"><img class="fabBtn"
				src="../images/ico_add.png" alt="내 메모 추가"></i>

		</span>
		<ul class="fab-buttons">
			<li class="fab-buttons__item memoBtn"><a href="#"
				class="fab-buttons__link" data-tooltip="메모 등록"> <i
					class="icon-material icon-material_memo"></i>
			</a></li>
			<!--
                21.05.07
                .memoBtn 터치 시,
                .popupMemo{display:block;}
                -->
			<li class="fab-buttons__item urlBtn"><a href="#"
				class="fab-buttons__link" data-tooltip="링크 등록"> <i
					class="icon-material icon-material_url"></i>
			</a></li>
			<!--
                21.05.07
                .urlBtn 터치 시,
                .popupUrl{display:block;}
                -->
		</ul>
	</div>
	<!-- fab -->
	<!--
        21.05.14
        fab 구조 추가
        -->


	<script src="${values.getJQUERY_URL()}"></script>
	<script src="${values.getJQUERY_UI_URL()}"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="../js/lib/moment-with-locales.min.js"></script>
	<script src="../js/lib/moment-timezone-with-data.min.js"></script>
	<script src="../js/lib/jquery.blockUI.min.js"></script>
	<script src="../js/functions.js"></script>
	<script src="../js/functions2.js"></script>
	<script src="../js/values.js?v=1.1"></script>
	<script src="../js/main/SupplyNdemand.js"></script>
	<script src="../js/main/Profile.js"></script>
	<script src="../js/main/MainPageLink.js"></script>


	<script>
		var supplyNdemand = new SupplyNdemand();
		var mainPageLink = new MainPageLink();
		var profile = new Profile();
	</script>
</body>
</html>

