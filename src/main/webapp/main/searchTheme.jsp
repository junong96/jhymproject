<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);

if (request.getRequestURI().equals("/stockInfo/")) {
	pageContext.setAttribute("prePath", "");
} else {
	pageContext.setAttribute("prePath", "../");
}
%>
<c:import url="common/checkStatus.jsp"></c:import>
<html lang="ko">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>주식 정보</title>
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap"
	rel="stylesheet">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="${prePath}css/base.css" rel="stylesheet">
<link href="${prePath}css/index.css" rel="stylesheet">
<link href="${prePath}css/searchTheme.css" rel="stylesheet">
</head>

<body>
	<div id="wrap">
		<div class="popupMask"></div>
		<!--
        21.04.20
        popupMask 터치 시, .popupWrap{display:none;}
        -->
		<div class="popupWrap addKeywordWrap">
			<h3 class="popupTitle">키워드 추가 요청</h3>
			<div class="close">
				<img src="${prePath}images/ico_close.png" alt="팝업 닫기 버튼">
			</div>
			<!--
            21.04.22
            .close 버튼 터치 시 팝업창 닫기
            -->
			<div class="popupContent">
				<div class="inputKeyword">
					<input type="text" id="" alt="" name=""
						placeholder="키워드를 입력 후 엔터를 눌러주세요." value="">
					<!--
                    21.04.20
                    '키워드' 검색어 입력 후, space bar 누르면 
                    .inputKeyword .addedKeyword{display: block;}추가
                            
                    Backspace 누르면 바로 앞에 있는 해당 키워드 지워짐 
                    -->
					<div class="alertExist display_none">
						<p>이미 존재하는 키워드입니다. 다른 키워드를 입력해 주세요.</p>
					</div>
					<!--
                    21.04.20
                    해당 종목에 이미 있는 키워드를 입력 후 enter 눌렀을 경우,
                    .inputKeyword .alertExist p{display: block}
                    -->

				</div>
				<!-- inputKeyword -->
				<div class="addedKeyword">
					<ul>

					</ul>
				</div>

				<!--
                    21.04.20
                    검색어 입력 후, Enter 치기 전에는 
                    .search .addedKeyword{display: none;}
                        
                    검색어 입력 후, Enter 시
                    .search .addedKeyword{display: block;} 됨
                        
                    X 버튼 클릭 시, 해당 키워드 삭제됨
                -->

				<!-- addedKeyword -->
				<button>추가 요청</button>
				<!--
                21.04.20
                버튼 터치 시, 추후 있을 관리자페이지 웹사이트로 정보 이동?
                -->
			</div>
			<!-- popupContent -->
		</div>
		<!-- popupWrap -->

		<div class="popupMask"></div>
		<!--
        21.04.20
        popupMask 터치 시, .popupMemo{display:none;}
        -->
		<div class="popupWrap popupMemo">
			<h3 class="popupTitle">메모 등록</h3>
			<div class="close">
				<img src="${prePath}images/ico_close.png" alt="팝업 닫기 버튼">
			</div>
			<!--
            21.04.22
            .close 버튼 터치 시 팝업창 닫기
            -->
			<div class="popupContent">
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="제목을 입력해 주세요."
						value="">
				</div>
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="내용을 입력해 주세요."
						value="">
				</div>
				<!-- inputKeyword -->
				<button>등록</button>
			</div>
			<!-- popupContent -->
		</div>
		<!-- popupMemo -->

		<div class="popupWrap popupUrl">
			<h3 class="popupTitle">링크 등록</h3>
			<div class="close">
				<img src="${prePath}images/ico_close.png" alt="팝업 닫기 버튼">
			</div>
			<!--
            21.04.22
            .close 버튼 터치 시 팝업창 닫기
            -->
			<div class="popupContent">
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="제목을 입력해 주세요."
						value="">
				</div>
				<div class="inputKeyword">
					<input type="text" alt="" name="" placeholder="url을 추가해 주세요."
						value="">
				</div>
				<!-- inputKeyword -->
				<button>등록</button>
			</div>
			<!-- popupContent -->
		</div>
		<!-- popupUrl -->

		<!-- gnb -->
		<c:import url="common/menu.jsp">
			<c:param name="type" value="searchTheme"></c:param>
		</c:import>
		<!-- gnb -->

		<div class="right">
			<header id="header">
				<div class="inner">
					<h2 class="pageTit">키워드 검색</h2>
					<c:import url="common/profile.jsp"></c:import>
					<!-- utill -->
				</div>
				<!--inner-->
			</header>

			<div id="container">
				<div class="inner">
					<div class="search">
						<div class="searchTab">
							<div class="filter type">
								<button class="active" data-type="keyword">키워드</button>
								<button data-type="stock">종목명</button>
							</div>
							<div class="filter logic">
								<button class="active" data-type="and">AND</button>
								<button data-type="or">OR</button>
							</div>
						</div>
						<!-- searchTab -->
						<!--
                        21.04.20
                        - 탭이 선택되면 class="active" 추가
                        - <button>종목명</button> 탭이 선택될 경우, 무조건 <button>OR</button>만 "active"됨
                        -->
						<div class="searchInput">
							<input type="text" class="searchValue" style="ime-mode: active;"
								name="" placeholder="검색어를 입력해 주세요." value="">
							<div class="addedkeyword">
								<ul>
								</ul>
							</div>
							<span id="searchBtn"><img class="searchImg"
								src="${prePath}images/ico_search.png" alt="검색 버튼"></span>

						</div>
						<!-- searchInput -->
						<div class="addedKeyword searchedKeyword">
							<ul>

							</ul>
						</div>

					</div>
					<!-- search -->
				</div>
				<!-- inner -->
				<div class="content">
					<div class="backG">
						<div class="wBox">
							<div class="con_01">
								검색에 대한 설명이 들어갈 예정입니다. <br> <br> 검색어가 없는 상태에서 검색 시 전체
								종목이 표시됩니다. <br> <br> 키워드로 검색 시 키워드 입력 후 space bar 누를 시
								검색할 키워드 리스트에 추가됩니다.
							</div>
							<!-- con_01 -->
						</div>
						<!-- wBox -->

						<div class="con_02">
							<div class="memoTab">
								<ul>
									<li class="infoTab">종목 정보</li>
									<li class="myMemoTab">내 메모</li>
								</ul>
							</div>
							<!--
                            21.05.07
                            관리자가 입력한 종목 정보가 없을 경우, 
                            .memoTab ul li.infoTab{display: none;}, 
                            <li class="memoTab">내 메모</li> 클래스에 on 추가
                            -->
							<!-- memoTab -->
							<div class="wBoxWrap">
								<div class="adminInfoWrap"></div>
								<!-- adminInfoWrap -->
								<!--
                                21.05.07
                                관리자가 작성한 종목 정보
                                <li class="infoTab">종목 정보</li> 클래스에 on 추가 시
                                -->
								<div class="myMemoWrap">
									<!--
                                        21.05.07
                                        1. 작성한 메모가 없을 경우
                                        2. 메모가 있을 경우
                                        모두 하단에 위치
                                        -->
								</div>
							</div>
							<!-- myMemoWrap -->
							<!--
                                <li class="memoTab">내 메모</li> 클래스에 on 추가 시
                                -->
						</div>
						<!-- wBoxWrap -->
					</div>
					<!-- con_02 -->

				</div>
				<!-- backG -->
				<c:import url="common/footer.jsp"></c:import>
			</div>
			<!-- content -->
		</div>
		<!-- container -->
	</div>
	<!-- right -->

	<div class="fab">
		<span class="fab-action-button"> <i
			class="fab-action-button__icon"><img class="fabBtn"
				src="${prePath}images/ico_add.png" alt="내 메모 추가"></i>

		</span>
		<ul class="fab-buttons">
			<li class="fab-buttons__item memoBtn"><a href="#"
				class="fab-buttons__link" data-tooltip="메모 등록"> <i
					class="icon-material icon-material_memo"></i>
			</a></li>
			<!--
                21.05.07
                .memoBtn 터치 시,
                .popupMemo{display:block;}
                -->
			<li class="fab-buttons__item urlBtn"><a href="#"
				class="fab-buttons__link" data-tooltip="링크 등록"> <i
					class="icon-material icon-material_url"></i>
			</a></li>
			<!--
                21.05.07
                .urlBtn 터치 시,
                .popupUrl{display:block;}
                -->
		</ul>
	</div>
	<!-- fab -->
	<script>
		var prePath = "${prePath}";
	</script>
	<script src="${values.getJQUERY_URL()}"></script>
	<script src="${values.getJQUERY_UI_URL()}"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="${prePath}js/lib/moment-with-locales.min.js"></script>
	<script src="${prePath}js/lib/moment-timezone-with-data.min.js"></script>
	<script src="${prePath}js/lib/jquery.blockUI.min.js"></script>
	<script src="${prePath}js/functions.js"></script>
	<script src="${prePath}js/functions2.js"></script>
	<script src="${prePath}js/values.js?v=1.1"></script>
	<script src="${prePath}js/main/SearchTheme.js"></script>
	<script src="${prePath}js/main/MainPageLink.js"></script>
	<script src="${prePath}js/main/Profile.js"></script>
	<script src="${prePath}js/main/autocomplate.js"></script>


	<script>
		var searchTheme = new SearchTheme();
		var mainPageLink = new MainPageLink();
		var profile = new Profile();
	</script>
</body>
</html>

