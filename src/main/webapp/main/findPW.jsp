<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);
%>
<html lang="ko">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
    <title>주식 정보 project</title>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="css/base.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
</head>

<body>
    <div id="wrap">
        <div id="loginHeader">
            <img class="logo" alt="로고">
        </div>
        <!-- loginHeader -->
        <div id="container">
            <div class="inner2">
                <div class="loginWrap">
                    <div class="header">
                        <h2>비밀번호 변경</h2>
                        <p>비밀번호를 재설정할 이메일 주소를 입력해 주세요.</p>
                    </div>
                    <form>
                        <div class="inputData">
                            <input type="email" placeholder="이메일" value="" required>
                        </div>
                        <div class="inputData">
                            <input class="loginBtn" type="submit" value="이메일 전송">
                        </div>
                    </form>
                </div>
                <!-- loginWrap -->
            </div>
            <!-- inner2 -->
        </div>
        <!-- container -->
        <footer id="footer">

        </footer>
    </div>

	<script src="${values.getJQUERY_URL()}"></script>
	<script src="${values.getJQUERY_UI_URL()}"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="../js/lib/moment-with-locales.min.js"></script>
	<script src="../js/lib/moment-timezone-with-data.min.js"></script>
	<script src="../js/lib/jquery.blockUI.min.js"></script>
	<script src="../js/functions.js"></script>
	<script src="../js/functions2.js"></script>
	<script src="../js/values.js?v=1.1"></script>
	<script src="../js/main/SearchTheme.js"></script>
	<script src="../js/main/MainPageLink.js"></script>
	<script src="../js/autocomplate.js"></script>


	<script>
	var searchTheme = new SearchTheme();
	</script>