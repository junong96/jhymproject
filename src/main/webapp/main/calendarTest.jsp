<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);

Boolean isLogin = (Boolean) session.getAttribute("isLogin");
Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");
String nickname = (String) session.getAttribute("nickname");
%>
<c:import url="common/checkStatus.jsp"></c:import>
<html lang="ko">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>주식 정보 project</title>
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap"
	rel="stylesheet">
<link href="../css/base.css" rel="stylesheet">
<link href="../css/index.css" rel="stylesheet">
<link href="../css/calendar/main.min.css" rel="stylesheet">
<link href="../css/supplyNdemand.css" rel="stylesheet">
</head>

<body>
	<div id="wrap">
		<c:import url="common/menu.jsp">
			<c:param name="type" value="calendar"></c:param>
		</c:import>
		<!-- gnb -->

		<div class="popupMask"></div>
		<!--
        21.04.20
        popupMask 터치 시, .popupMemo{display:none;}
        -->

		<div class="right">
			<header id="header">
				<div class="inner">
					<h2 class="pageTit">캘린더</h2>
					<c:import url="common/profile.jsp"></c:import>
					<!-- utill -->
				</div>
				<!--inner-->
			</header>

			<div id="container">
				<div class="content">
					<div class="backG">
						<div class="wBox">
							<div class="con_01" id="calendarDiv"></div>
							<!-- con_01 -->
						</div>
						<!-- wBox -->
						<div class="con_02"></div>
						<!-- con_02 -->
						<!--
                        21.05.14
                        메모 구조 추가
                        -->
					</div>
					<!-- back -->
				</div>
				<!-- content -->
			</div>
			<!-- container -->
			<footer id="footer"> </footer>
		</div>

		<!-- fab -->
		<!--
        21.05.14
        fab 구조 추가
        -->
	</div>
	<script src="${values.getJQUERY_URL()}"></script>
	<script src="${values.getJQUERY_UI_URL()}"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="../js/lib/moment-with-locales.min.js"></script>
	<script src="../js/lib/moment-timezone-with-data.min.js"></script>
	<script src="../js/lib/jquery.blockUI.min.js"></script>
	<script src="../js/functions.js"></script>
	<script src="../js/functions2.js"></script>
	<script src="../js/values.js?v=1.1"></script>
	<script src="../js/fullcalendar/main.min.js"></script>
	<script src="../js/fullcalendar/locales-all.min.js"></script>
	<script src="../js/main/StockCalendar.js"></script>
	<script src="../js/main/Profile.js"></script>
	<script src="../js/main/MainPageLink.js"></script>


	<script>
		var stockCalendar = new StockCalendar();
		var mainPageLink = new MainPageLink();
		var profile = new Profile();
	</script>