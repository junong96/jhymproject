<%@page import="util.CONFIG_VALUE"%>
<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setCharacterEncoding("UTF-8");
CONFIG_VALUE configValues = new CONFIG_VALUE(request.getLocalAddr());
pageContext.setAttribute("values", configValues);
%>
<c:import url="common/checkStatus.jsp"></c:import>
<html lang="ko">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>주식 정보 project</title>
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap"
	rel="stylesheet">
<link href="../css/base.css" rel="stylesheet">
<link href="../css/index.css" rel="stylesheet">
<link href="../css/todayKeyword.css" rel="stylesheet">
</head>

<body>
	<div id="wrap">
		<c:import url="common/menu.jsp">
			<c:param name="type" value="afterhoursKeyword"></c:param>
		</c:import>
		<!-- gnb -->
		<div class="right">
			<header id="header">
				<div class="inner">
					<h2 class="pageTit">시간외 상승 키워드</h2>
					<c:import url="common/profile.jsp"></c:import>
					<!-- utill -->
				</div>
				<!--inner-->
			</header>

			<div id="container">
				<div class="inner">
					<div class="content">
						<div class="datePicker">
							<img src="../images/ico_left.png" class="before" alt="왼쪽 화살표 버튼">
							<span class="current_date"></span> <img
								src="../images/ico_right.png" class="after display_none"
								alt="오른쪽 화살표 버튼">
							<!--
                                21.04.20
                                .before 버튼 터치 시, 전 날 날짜로 이동
                                .after 버튼 터치 시, 다음 날 날짜로 이동
                                -->
						</div>
						<div class="con_01 tableSmall">

							<table class="dataTable">
								<tr class="tableTitle">
									<th class="w25p">종목명</th>
									<th class="w20p">현재가</th>
									<th class="w20p">등락폭</th>
									<th class="w20p">등락률</th>
									<th class="w15p">거래량</th>
								</tr>
							</table>
							<!--
                            21.04.20
                            주가가 상승일 때 
                            - 세번째, 네번째 td에 class="txtREd" 추가
                            - 세번째 td에 ico_up.png 이미지 추가
                            
                            주가가 하락일 때
                            - 세번째, 네번째 td에 class="txtBlue" 추가
                            - 세번째 td에 ico_down.png 이미지 추가
                            -->
						</div>

						<div class="con_02 ranking">
							<ul class="top5">

							</ul>
							<ul class="top10">

							</ul>
						</div>
					</div>
					<!-- content -->
				</div>
				<!-- inner -->
			</div>
			<!-- container -->
			<c:import url="common/footer.jsp"></c:import>
		</div>
	</div>
	<script src="${values.getJQUERY_URL()}"></script>
	<script src="${values.getJQUERY_UI_URL()}"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="../js/lib/moment-with-locales.min.js"></script>
	<script src="../js/lib/moment-timezone-with-data.min.js"></script>
	<script src="../js/lib/jquery.blockUI.min.js"></script>
	<script src="../js/functions.js"></script>
	<script src="../js/functions2.js"></script>
	<script src="../js/values.js?v=1.1"></script>
	<script src="../js/main/AfterhoursKeyword.js"></script>
	<script src="../js/main/Profile.js"></script>
	<script src="../js/main/MainPageLink.js"></script>


	<script>
		var afterhoursKeyword = new AfterhoursKeyword();
		var mainPageLink = new MainPageLink();
		var profile = new Profile();
	</script>
</body>
</html>
