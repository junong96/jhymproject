<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<div id="gnb">
	<img class="logo" alt="로고">
	<ul class="menu">
		<li data-type="searchTheme">키워드 검색</li>
		<li data-type="todayKeyword">오늘의 상승 키워드</li>
		<li data-type="afterhoursKeyword">시간외 상승 키워드</li>
		<li data-type="supplyNdemand">수급 포착</li>
		<li data-type="topStock">매수 상위 종목</li>
		<li data-type="calendar">캘린더</li>
	</ul>
</div>

<script>
	var menuType = "${param.type}";
</script>