<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

	<div class="utill preLogin">
		<div class="profile"></div>
		<div class="utillBtn">
			<button class="loginBtn">로그인</button>
			<button class="joinBtn">회원가입</button>
		</div>
	</div>

	<div class="utill aftLogin">
		<div class="profile ">안녕하세요 ${nickname}님</div>
		<div class="utillBtn">
			<button class="logoutBtn">로그아웃</button>
		</div>
	</div>
