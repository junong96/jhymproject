<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%
	Boolean isLogin = (Boolean) session.getAttribute("isLogin");
	Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");
	String nickname = (String) session.getAttribute("nickname");
	Integer id = (Integer) session.getAttribute("id");
	
	pageContext.setAttribute("id", id);
	pageContext.setAttribute("nickname", nickname);
	pageContext.setAttribute("isAdmin", isAdmin);
	pageContext.setAttribute("isLogin", isLogin);
%>

<script>
	var isLogin = "${isLogin}";
	var isAdmin = "${isAdmin}";
	var userID = "${id}";
	var nickname = "${nickname}";
</script>