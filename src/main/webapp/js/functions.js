/**
 * 
 */

/**
 * 숫자에 3자리마다 콤마를 붙여줌
 * 
 * @param value
 *            숫자
 * @returns #,###
 */
function addComma(value) {
	if (value == undefined) return 0;
	else return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function autoLink(contents) {
	if (contents != undefined) {
		var regURL = /(http|https):\/\/([-/.a-zA-Z0-9_~#%$?&]+)/;
		contents = contents.replace(regURL, '<a href="$1://$2" target="_blank">$1://$2</a>');
	}

	return contents;
}

function calcMeasurementTime(measurementTime) {
	var oneHourInSecond = 3600;
	var oneMinuteInsecond = 60;

	var hour = Math.floor(measurementTime / oneHourInSecond);
	measurementTime = measurementTime % oneHourInSecond;
	var minute = Math.floor(measurementTime / oneMinuteInsecond);
	var second = Math.floor(measurementTime % oneMinuteInsecond);

	if (hour < 10) {
		hour = "0" + hour;
	}
	if (minute < 10) {
		minute = "0" + minute;
	}
	if (second < 10) {
		second = "0" + second;
	}

	return hour + ":" + minute + ":" + second;
}

function callScript(data, isKR) {
	data = JSON.stringify(data);

	if (isKR) {
		data = unescape(encodeURIComponent(data));
	}

	YkTime.scriptPopup(btoa(data));
}

function replaceToBr(text) {
	if (text != undefined) text = text.replace(/\n/g, "<br>");
	return text;
}

function replaceBrToN(text) {
	if (text != undefined) text = text.replace(/<br>/g, "\n");
	return text;
}

var getParameters = function(paramName) {
	// 리턴값을 위한 변수 선언
	var returnValue;

	// 현재 URL 가져오기
	var url = location.href;

	// get 파라미터 값을 가져올 수 있는 ? 를 기점으로 slice 한 후 split 으로 나눔
	var parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');

	// 나누어진 값의 비교를 통해 paramName 으로 요청된 데이터의 값만 return
	for (var i = 0; i < parameters.length; i++) {
		var varName = parameters[i].split('=')[0];
		if (varName.toUpperCase() == paramName.toUpperCase()) {
			returnValue = parameters[i].split('=')[1];
			return decodeURIComponent(returnValue);
		}
	}
}

function isImageFile(filename) {
	var filenameExtensionRexEx = /\.(gif|jpg|jpeg|png)$/;

	return filenameExtensionRexEx.test(filename);
}

function isFile(filename) {
	var filenameExtensionRexEx = /\.(avi|mpg|asf|mp3|wav|mp4|mov|zip|txt|rtf|hwp|pdf|doc|docx|ppt|pptx|xlsx|xls|xml)$/;

	return filenameExtensionRexEx.test(filename);
}

function blockUI() {
	$.blockUI({
		message: "<div class=\"loading_wrapper\"><img src=\"" + DOMAIN + "/images/etc/loading.gif\"></div>",
		css: { border: 0, "background-color": "rgba(0, 0, 0, 0)" }
	});
}

function unblockUI() {
	$.unblockUI();
}

Date.prototype.format = function(f) {
	if (!this.valueOf()) return " ";

	if (typeof phases == "undefined") {
		phases = {
			"일요일": "일요일",
			"월요일": "월요일",
			"화요일": "화요일",
			"수요일": "수요일",
			"목요일": "목요일",
			"금요일": "금요일",
			"토요일": "토요일",
		};
	}

	var weekName = [phases["일요일"], phases["월요일"], phases["화요일"], phases["수요일"], phases["목요일"], phases["금요일"], phases["토요일"]];
	var d = this;

	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
		switch ($1) {
			case "yyyy": return d.getFullYear();
			case "yy": return (d.getFullYear() % 1000).zf(2);
			case "MM": return (d.getMonth() + 1).zf(2);
			case "dd": return d.getDate().zf(2);
			case "E": return weekName[d.getDay()];
			case "HH": return d.getHours().zf(2);
			case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case "mm": return d.getMinutes().zf(2);
			case "ss": return d.getSeconds().zf(2);
			case "a/p": return d.getHours() < 12 ? "오전" : "오후";
			default: return $1;
		}
	});
};

String.prototype.string = function(len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function(len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function(len) { return this.toString().zf(len); };

$(document).ajaxStart(blockUI).ajaxStop(unblockUI);

function showGroupInfoPopup(token, isMember, isPublic, isPreview) {
	if (isMember == undefined) isMember = false;
	if (isPublic == undefined) isPublic = false;
	if (isPreview == undefined) isPreview = false;

	callScript({
		"type": "joinGroup",
		"token": token,
		"isMember": isMember,
		"isPublic": isPublic,
		"isPreview": isPreview
	});
}

function telValidator(args) {
	if (/^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}/.test(args)) {
		return true;
	}
	return false;
}

function pwValidator(args) {
	if (/^(?=.*[a-zA-z])(?=.*[0-9])(?=.*[$`~!@$!%*#^?&\\(\\)\-_=+])(?!.*[^a-zA-z0-9$`~!@$!%*#^?&\\(\\)\-_=+]).{8,30}$/.test(args)) {
		return true;
	}
	return false;
}

function emailValidator(args) {
	
	if (/^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/.test(args)) {
		return true;
	}
	return false;
}

function checkConsonant(text) {
	const pattern = /([^가-힣A-Za-z0-9\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\"\x20])/i;
	return pattern.test(text);
}




