/**
 * 
 */
function Join() {
	this._init();
}


Join.prototype._init = function() {
	this.signupBtnClickEvent();
	this.addInputChangeEvent();
	this.addMoreBtnClickEvent();
	this.addTermsCheckBtnClickEvent();
	this.addAcceptAllBtnClickEvent();

	this._terms1Check = false;
	this._terms2Check = false;
}

Join.prototype.signupBtnClickEvent = function() {
	$(".signupBtn").off("click").click(function() {

		if (!$(this).hasClass("loginBtn")) {
			return;
		}

		var email = $(".email").val();
		var pw1 = $(".pw1").val();
		var pw2 = $(".pw2").val();
		var nickname = $(".nickname").val();
		var tel = $(".tel").val();

		if (!emailValidator(email)) {
			$(".email").parent().addClass("errorInput");
			$(".email").parent().append("<span class=\"errorMessage\">이메일 형식이 유효하지 않습니다.</span>");
			$(".email").focus();
			return;
		} else if (!pwValidator(pw1)) {
			$(".pw1").parent().addClass("errorInput");
			$(".pw1").parent().append("<span class=\"errorMessage\">비밀 번호가 유효하지 않습니다. 비밀번호는 영문자, 숫자 특수문자로 구성되어져야 하며 숫자, 특수문자 최소 1개 이상 포함되어야 합니다.</span>");
			$(".pw1").focus();
			return;
		} else if (pw1 != pw2) {
			$(".pw2").parent().addClass("errorInput");
			$(".pw2").parent().append("<span class=\"errorMessage\">비밀번호가 일치하지 않습니다.</span>");
			$(".pw2").focus();
			return;
		} else if (!telValidator(tel)) {
			$(".tel").parent().addClass("errorInput");
			$(".tel").parent().append("<span class=\"errorMessage\">전화번호 형식이 유효하지 않습니다.</span>");
			$(".tel").focus();
			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/main/signup",
			method: "POST",
			data: "email=" + email + "&password=" + pw1 + "&nickname=" + nickname + "&phoneNumber=" + tel,
			success: function() {
				alert("회원가입을 성공했습니다.");
				location.replace(DOMAIN + "/")
			},
			error: function(xhr, status, error) {
				if (xhr.status == 406) {
					alert("이메일 혹은 닉네임이 중복되었습니다. 다시 설정해주세요.");
				} else if (xhr.status == 403) {
					alert("패스워드중 사용 불가능한 문자가 포함되어져있습니다. 패스워드를 다시 설정해주세요.");
				} else {
					alert("회원가입 도중 에러가 발생했습니다. 잠시 후 다시 시도해주세요.");
				}
			}
		}); // end ajax
	});
}

Join.prototype.addInputChangeEvent = function() {
	$(".email, .pw1, .pw2").keyup(function(event) {
		if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9!.@$!%*#^?&\\(\\)\-_=+]/gi, ''));
		}
	});

	$(".tel").keyup(function(event) {
		if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^0-9-]/gi, ''));
		}
	});

	$("input").keyup(function() {
		if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
			$(this).parent().removeClass("errorInput");
			$(this).parent().find("span").remove();
		}

	});

	$("input").keyup(function() {
		if ($(this).hasClass("email")) {
			if (!emailValidator($(this).val())) {
				$(".email").parent().addClass("errorInput");
				$(".email").parent().append("<span class=\"errorMessage\">이메일 형식이 유효하지 않습니다.</span>");
				$(".email").focus();
			}
		} else if ($(this).hasClass("pw1")) {
			if (!pwValidator($(this).val())) {
				$(".pw1").parent().addClass("errorInput");
				$(".pw1").parent().append("<span class=\"errorMessage\">비밀 번호가 유효하지 않습니다. 비밀번호는 영문자, 숫자 특수문자로 구성되어져야 하며 숫자, 특수문자 최소 1개 이상 포함되어야 합니다.</span>");
				$(".pw1").focus();
			}

			if ($(".pw2").val() != "" && $(".pw2").val() != $(this).val()) {
				$(".pw1").parent().addClass("errorInput");
				$(".pw1").parent().append("<span class=\"errorMessage\">비밀번호가 일치하지 않습니다.</span>");
				$(".pw1").focus();
			}else{
				$(".pw1").parent().removeClass("errorInput");
				$(".pw1").parent().find("span").remove();
				$(".pw2").parent().removeClass("errorInput");
				$(".pw2").parent().find("span").remove();
			}
		} else if ($(this).hasClass("pw2")) {
			if ($(".pw1").val() != $(this).val()) {
				$(".pw2").parent().addClass("errorInput");
				$(".pw2").parent().append("<span class=\"errorMessage\">비밀번호가 일치하지 않습니다.</span>");
				$(".pw2").focus();
			}else{
				$(".pw1").parent().removeClass("errorInput");
				$(".pw1").parent().find("span").remove();
				$(".pw2").parent().removeClass("errorInput");
				$(".pw2").parent().find("span").remove();
			}
		} else if ($(this).hasClass("tel")) {
			if (!telValidator($(this).val())) {
				$(".tel").parent().addClass("errorInput");
				$(".tel").parent().append("<span class=\"errorMessage\">형식에 맞지 않는 번호입니다. 정확한 전화번호를 입력해 주세요.</span>");
				$(".tel").focus();
			}
		}
		join.checkInputEmptyAndTermsCheck();
	});

}

Join.prototype.checkInputEmptyAndTermsCheck = function() {
	var email = $(".email").val();
	var pw1 = $(".pw1").val();
	var pw2 = $(".pw2").val();
	var nickname = $(".nickname").val();
	var tel = $(".tel").val();

	var isError = $("form .inputData").hasClass("errorInput");
	if (email != "" && pw1 != "" && pw2 != "" && nickname != "" && tel != "" && !isError && this._terms1Check && this._terms2Check) {
		$(".signupBtn").addClass("loginBtn");
	} else {
		$(".signupBtn").removeClass("loginBtn");
	}

}

Join.prototype.addMoreBtnClickEvent = function() {
	$(".terms li:not(.AcceptAll) .moreCon").off("click").click(function() {
		$(this).parent().find(".errorMessage").remove();
		$(this).parent().find("textarea").removeClass("display_none");
	});
}

Join.prototype.addAcceptAllBtnClickEvent = function() {
	$(".terms li.AcceptAll .checkBtn").off("click").click(function() {
		$(".terms").find(".errorMessage").remove();
		$(".terms").find("textarea").removeClass("display_none");
		$(this).find("img").attr("src", "../images/join/ico_allCheck_active.png");
		$(".terms li:not(.AcceptAll) .checkBtn").find("img").attr("src", "../images/join/ico_check_active.png");
		join._terms1Check = true;
		join._terms2Check = true;
		join.checkInputEmptyAndTermsCheck();
	});
}


Join.prototype.addTermsCheckBtnClickEvent = function() {
	$(".terms li:not(.AcceptAll) .checkBtn").off("click").click(function() {
		if ($(this).parent().find("textarea").hasClass("display_none")) {
			if ($(this).parent().find(".errorMessage").length == 0) {
				$(this).parent().append("<p class=\"errorMessage\">내용 보기를 클릭하여 약관을 확인해 주세요.</p>");
			}
			return;
		} else {
			$(this).parent().find(".errorMessage").remove();
			if ($(this).hasClass("terms1")) {
				join._terms1Check = true;
			} else {
				join._terms2Check = true;
			}
			$(this).find("img").attr("src", "../images/join/ico_check_active.png");
		}
		
		if(join._terms1Check && join._terms2Check) $(".terms li.AcceptAll .checkBtn").find("img").attr("src", "../images/join/ico_allCheck_active.png");
	});
}


