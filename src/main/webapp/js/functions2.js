/**
 * 
 */

function setUserToken(token) {
	this.userToken = token;
}

function setVersion(appVersion) {
	this.appVersion = appVersion;
}

/**
 * 숫자에 3자리마다 콤마를 붙여줌
 * 
 * @param value
 *            숫자
 * @returns #,###
 */
function addComma(value) {
	if(value == undefined) return 0;
	else return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var getParameters = function (paramName) {
    // 리턴값을 위한 변수 선언
    var returnValue;

    // 현재 URL 가져오기
    var url = location.href;

    // get 파라미터 값을 가져올 수 있는 ? 를 기점으로 slice 한 후 split 으로 나눔
    var parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');

    // 나누어진 값의 비교를 통해 paramName 으로 요청된 데이터의 값만 return
    for (var i = 0; i < parameters.length; i++) {
        var varName = parameters[i].split('=')[0];
        if (varName.toUpperCase() == paramName.toUpperCase()) {
            returnValue = parameters[i].split('=')[1];
            return decodeURIComponent(returnValue);
        }
    }
}

function isName(name) {
	var nameRegEx = /[^a-zA-Z가-힣]+/;
	
	return !nameRegEx.test(name);
}

//사용자가 입력한 이메일을 검증하는 메서드
function isEmail(email) {
	// 사용자가 입력한 이메일을 검증할 정규식
	var emailRegEx = /^[0-9a-zA-Z]([-_.,]|[0-9a-zA-Z])*@([0-9a-zA-Z]+)\.([0-9a-zA-Z]\.*)+$/;
	
	return emailRegEx.test(email);
}

function isTel(tel) {
	var telRegEx = /[^0-9]+/;
	
	return !telRegEx.test(tel);
}

function chooseImage(encoding){
    
    versionImage = encoding;
    $(".versionFile").click();
}

var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

function nl2br(contents) {
	return contents.replace(/\n/g, "<br>");  
}

function autoLink(contents) {
	var regURL = /(http|https):\/\/([-/.a-zA-Z0-9_~#%$?&]+)/;
	
	return contents.replace(regURL, '<a href="$1://$2" target="_blank">$1://$2</a>');
}

function callScript(data, isKR) {
	data = JSON.stringify(data);
	
	if(isKR) {
		data = unescape(encodeURIComponent(data));
	}
	
	YkTime.scriptPopup(btoa(data));
}

function jsonToString(data, isKR) {
	data = JSON.stringify(data);
	
	if(isKR) {
		data = unescape(encodeURIComponent(data));
	}
	
	return btoa(data);
}

Date.prototype.format = function(f) {
	if (!this.valueOf()) return " ";
	
	if(typeof phases == "undefined") {
		phases = {
			"일요일": "일요일",
			"월요일": "월요일",
			"화요일": "화요일",
			"수요일": "수요일",
			"목요일": "목요일",
			"금요일": "금요일",
			"토요일": "토요일",
		};
	}
	
	var weekName = [phases["일요일"], phases["월요일"], phases["화요일"], phases["수요일"], phases["목요일"], phases["금요일"], phases["토요일"]];
	var d = this;
	
	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
		switch ($1) {
			case "yyyy": return d.getFullYear();
			case "yy": return (d.getFullYear() % 1000).zf(2);
			case "MM": return (d.getMonth() + 1).zf(2);
			case "dd": return d.getDate().zf(2);
			case "E": return weekName[d.getDay()];
			case "HH": return d.getHours().zf(2);
			case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case "mm": return d.getMinutes().zf(2);
			case "ss": return d.getSeconds().zf(2);
			case "a/p": return d.getHours() < 12 ? "오전" : "오후";
			default: return $1;
		}
	});
};
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

function dateDiff(_date1, _date2) {
    var diffDate_1 = _date1 instanceof Date ? _date1 : new Date(_date1);
    var diffDate_2 = _date2 instanceof Date ? _date2 : new Date(_date2);
 
    diffDate_1 = new Date(diffDate_1.getFullYear(), diffDate_1.getMonth()+1, diffDate_1.getDate());
    diffDate_2 = new Date(diffDate_2.getFullYear(), diffDate_2.getMonth()+1, diffDate_2.getDate());
 
    var diff = diffDate_2.getTime() - diffDate_1.getTime();
    diff = Math.ceil(diff / (1000 * 3600 * 24));
 
    return diff;
}

function quickSort(arr) {
	if(arr.length == 0 ) {
		return [];
	}
	
	var middle = arr[0];
	var len = arr.length;
	var left = []
	var right = [];
	
	for(var i=1; i<len; ++i) {
		if(arr[i] < middle) {
			left.push(arr[i]);
		} else {
			right.push(arr[i]);
		}
	}
	
	return quickSort(left).concat(middle, quickSort(right));
}
function showGroupInfoPopup(token, isMember, isPublic, isPreview) {
	if(isMember == undefined) isMember = false;
	if(isPublic == undefined) isPublic = false;
	if(isPreview == undefined) isPreview = false;
	
	callScript({
		"type": "joinGroup",
		"token": token,
		"isMember": isMember,
		"isPublic": isPublic,
		"isPreview": isPreview
	});
}

function htmlToText(txt) {
	txt = txt.replace(/</gi, "&lt;");
	txt = txt.replace(/>/gi, "&gt;");
	return txt;
}

function convertCode(code) {
	var newCode
	switch (String(code).length) {
		case 1:
			newCode = "00000" + code;
			break;
		case 2:
			newCode = "0000" + code;
			break;
		case 3:
			newCode = "000" + code;
			break;
		case 4:
			newCode = "00" + code;
			break;
		case 5:
			newCode = "0" + code;
			break;
		default:
			newCode = code;
	}
	return newCode;
}

function doKeywordMapping(list, topParentKeyword) {
	for (var i = 0; i < list.length; i++) {
		var mainKeyword = list[i];
		var mainStockCodeArr = $(mainKeyword).data("stockcodearr");

		var fullMatchArr = new Array(); //완전 매치;
		var someMatchArr = new Array(); //일부 매치;
		for (var j = i + 1; j < list.length; j++) {
			var compareKeyword = list[j];
			var compareStockCodeArr = $(compareKeyword).data("stockcodearr");

			var interSectionArr = mainStockCodeArr.filter(x => compareStockCodeArr.includes(x));

			if (interSectionArr.length == mainStockCodeArr.length) {
				fullMatchArr.push(compareKeyword);
			} else if (interSectionArr.length / mainStockCodeArr.length > 0.5) {
				someMatchArr.push(compareKeyword);
			}else if (topParentKeyword != undefined){
				//잉여
				someMatchArr.push(compareKeyword);
			}
		}

		fullMatchArr.forEach(function(element) {
			var keyword = $(element).find(".keywordName").text();
			var mainKeywordText = $(mainKeyword).find(".keywordName").text();
			$(mainKeyword).find(".keywordName").text(mainKeywordText + "/" + keyword);

			list.splice(list.indexOf(element), 1);
			$(element).remove();
			//처음에 완전 일치 시 최상위에 붙고 삭제 되어짐
		});

		if (someMatchArr.length > 0) {
			$(mainKeyword).find(".keywordName").text($(mainKeyword).find(".keywordName").text() + "-");
		}

		if (someMatchArr.length == 0) {
			if (topParentKeyword != undefined) break;
		} else if (someMatchArr.length == 1) {
			var keyword = $(someMatchArr[0]).find(".keywordName").text();
			var mainKeywordText = $(mainKeyword).find(".keywordName").text();
			$(mainKeyword).find(".keywordName").text(mainKeywordText + keyword);
			
			var someStockCodeArr = $(someMatchArr[0]).data("stockcodearr");
			var tempStockCodeArr = mainStockCodeArr.concat(someStockCodeArr);
			var filterSumStockCodeArr = tempStockCodeArr.filter((item, pos) => tempStockCodeArr.indexOf(item) === pos);
			$(mainKeyword).removeAttr("data-stockcodearr")
			$(mainKeyword).removeData("stockcodearr")
			$(mainKeyword).attr("data-stockcodearr",JSON.stringify(filterSumStockCodeArr));
			
			list.splice(list.indexOf(someMatchArr[0]), 1);
			$(someMatchArr[0]).remove();
		} else {
			someMatchArr.forEach(function(element) {
				list.splice(list.indexOf(element), 1);
				$(element).remove();
			});
			
			doKeywordMapping(someMatchArr, mainKeyword);
		}
	}

	if (topParentKeyword != undefined) {
		var topParentKeywordText = $(topParentKeyword).find(".keywordName").text();
		var mainKeywordText = $(mainKeyword).find(".keywordName").text();
	
		var topParentStockCodeArr = $(topParentKeyword).data("stockcodearr");
		var mainStockCodeArr = typeof $(mainKeyword).data("stockcodearr") == "object" ? $(mainKeyword).data("stockcodearr") : JSON.parse($(mainKeyword).data("stockcodearr"));
		
		var tempStockCodeArr = topParentStockCodeArr.concat(mainStockCodeArr);
		var filterSumStockCodeArr = tempStockCodeArr.filter((item, pos) => tempStockCodeArr.indexOf(item) === pos);
		
		$(topParentKeyword).removeAttr("data-stockcodearr")
		$(topParentKeyword).removeData("stockcodearr")
		$(topParentKeyword).attr("data-stockcodearr",JSON.stringify(filterSumStockCodeArr));
			
		
		list.splice(list.indexOf(mainKeyword), 1);
		$(mainKeyword).remove();
		$(topParentKeyword).find(".keywordName").text(topParentKeywordText + mainKeywordText);
	}
}
