/**
 * 
 */

function MainPageLink(){
	this._init();
}

MainPageLink.prototype._init = function() {
	this.currentMenuAddOnClass();
	this.addMenuListClickEvent();
}

MainPageLink.prototype.addMenuListClickEvent = function() {
	$(".menu li").off("click").click(function(){
		var type = $(this).data("type");
		
		switch(type){
			case "searchTheme":
			location.href= DOMAIN+"/main/searchTheme.jsp";
			break;
			case "todayKeyword":
			location.href= DOMAIN+"/main/todayKeyword.jsp";
			break;
			case "afterhoursKeyword":
			location.href= DOMAIN+"/main/afterhoursKeyword.jsp";
			break;
			case "supplyNdemand":
			location.href= DOMAIN+"/main/supplyNdemand.jsp";
			break;
			case "topStock":
			location.href= DOMAIN+"/main/topStock.jsp";
			break;
			case "calendar":
			location.href= DOMAIN+"/main/calendarTest.jsp";
			break;
			default:
			break;
		}
	});
}

MainPageLink.prototype.currentMenuAddOnClass = function() {
	$(".menu li").each(function(idx, item){
		if($(item).data("type") == menuType){
			$(item).addClass("on");
		}
	});
}