/**
 * 
 */

function AfterhoursKeyword() {
	this._init();

}

AfterhoursKeyword.prototype._init = function() {
	this._oneTupleHtmlTag = "   <tr data-code=\"(stockCode)\"> " +
		"   <td class=\"stockTit\">(stockName)" +
		" <ul class=\"hoverMenu\"> " +
		"     <li class=\"showStockDetail\">종목 상세정보</li>" +
		"     <li>(naverUrl)</li>" +
		"     <li>(daumUrl)</li>" +
		"     <li class=\"hoverKeywordList\"></li>" +
		"     </ul>" +
		"      </td>" +
		"  <td>(cur_price)</td> " +
		"   <td (updownPriceColor)><img src=\"(icon_image)\">(updown_price)</td> " +
		"   <td (updownRateColor)>(updown_rate)</td> " +
		"    <td>(volumn)</td> " +
		"</tr>";
	this._rankingTupleHtmlTag = "<li data-id=\"(keywordId)\" data-stockcodearr=\'(stockCodeArr)\'><span>(ranking)</span><span class=\"keywordName\">(keywordName)[(amount)]</span></li>";
	this._simpleKeywordTag = "<span>(keywordName)</span>";
	this._noneDataDayIndex = 0;
	var localDate = moment();
	this.getSubBusinessDay(localDate);
	this._fixedLocalDate = moment(localDate);


	this.setLocalDateInfo(localDate);

	this.loadAfterhoursKeywordList();
	this.addDatePickerChangeEvent();
	this.addKeyboardEvent2();
}

AfterhoursKeyword.prototype.loadAfterhoursKeywordList = function() {
	$(".content .dataTable tbody tr:not(.tableTitle)").remove();
	var date = this._localDateInfo.format("yyyy-MM-DD");
	$(".current_date").text(date);

	$.ajax({
		url: DOMAIN + "/stock/stockManage/overtimeKeyword/keywordData",
		method: "GET",
		data: "&date=" + date,
		dataType: "json",
		success: function(keywordDataListObj) {
			afterhoursKeywordDataList = keywordDataListObj.overtimeKeywordDataList;
			topKeywordList = keywordDataListObj.topKeywordList;

			if (afterhoursKeyword._fixedLocalDate.diff(afterhoursKeyword._localDateInfo.format("yyyy-MM-DD"), 'd') > 0) {
				$(".after").removeClass("display_none");
				afterhoursKeyword.addKeyboardEvent();
			} else {
				$(".after").addClass("display_none");
				afterhoursKeyword.addKeyboardEvent2();
			}

			if (afterhoursKeywordDataList.length != 0) {
				afterhoursKeyword._noneDataDayIndex = 0;
				afterhoursKeyword.setAfterhoursKeywordDataAmount(afterhoursKeywordDataList.length);
				afterhoursKeyword.setAfterhoursKeywordData(afterhoursKeywordDataList);
				afterhoursKeyword.showAfterhoursKeywordData(afterhoursKeywordDataList);
			} else {
				afterhoursKeyword._noneDataDayIndex += 1;
				if (afterhoursKeyword._noneDataDayIndex > 10) {
					afterhoursKeyword._noneDataDayIndex = 0;
					afterhoursKeyword.setAfterhoursKeywordDataAmount(0);
					afterhoursKeyword.showAfterhoursKeywordData();
					return;
				}

				if (afterhoursKeyword._currentMovingDirection == "add") {
					if (afterhoursKeyword._fixedLocalDate.diff(afterhoursKeyword._localDateInfo.format("yyyy-MM-DD"), 'd') == 0) {
						afterhoursKeyword._noneDataDayIndex = 0;
						afterhoursKeyword.setAfterhoursKeywordDataAmount(0);
						afterhoursKeyword.showAfterhoursKeywordData();
						return;
					}

					afterhoursKeyword._localDateInfo.add(1, "d");
					afterhoursKeyword.getAddBusinessDay(afterhoursKeyword._localDateInfo);
					afterhoursKeyword.loadAfterhoursKeywordList();
				} else {
					afterhoursKeyword._localDateInfo.subtract(1, "d");
					afterhoursKeyword.getSubBusinessDay(afterhoursKeyword._localDateInfo);
					afterhoursKeyword.loadAfterhoursKeywordList();
				}


			}

			if (topKeywordList.length != 0 && topKeywordList.length != undefined) {
				afterhoursKeyword.setTopKeywordDataAmount(topKeywordList.length);
				afterhoursKeyword.setTopKeywordData(topKeywordList);
				afterhoursKeyword.showTopKeywordData(topKeywordList);
			} else {
				afterhoursKeyword.setTopKeywordDataAmount(0);
				afterhoursKeyword.showTopKeywordData();
			}

		}, // end success
		error: function() {
			alert("오늘의 상승 키워드 목록을 불러오지 못했습니다");
		}
	}); // end ajax	
}

AfterhoursKeyword.prototype.showTopKeywordData = function(list) {
	$(".ranking .top5, .top10").empty();
	if (list == undefined) {
		$(".ranking .top5").append("<li>데이터가 존재하지 않습니다.</li>");
		return;
	}

	function compare(a, b) {
		return a.amount > b.amount ? -1 : a.amount < b.amount ? 1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < 50; i++) {
		var keywordObj = list[i];
		if (keywordObj == undefined) break;

		if (keywordObj.amount <= 1) continue;

		if (i < 10) {
			$(".ranking .top5").append(this._rankingTupleHtmlTag.replace("(ranking)", i + 1)
				.replace("(keywordName)", keywordObj.keyword)
				.replace("(keywordId)", keywordObj.id)
				.replace("(stockCodeArr)", JSON.stringify(keywordObj.stockCodeArr))
				.replace("(amount)", keywordObj.amount)
				.replace("(sumRate)", keywordObj.sumRate));
		} else {
			$(".ranking .top10").append(this._rankingTupleHtmlTag.replace("(ranking)", i + 1)
				.replace("(keywordName)", keywordObj.keyword)
				.replace("(keywordId)", keywordObj.id)
				.replace("(stockCodeArr)", JSON.stringify(keywordObj.stockCodeArr))
				.replace("(amount)", keywordObj.amount)
				.replace("(sumRate)", keywordObj.sumRate));
		}
	}

	var rankingKeywordList = Array.from(new Set($(".ranking ul li")));


	doKeywordMapping(rankingKeywordList)

	$($(".ranking li span:not(.keywordName)")).each(function(index, item) {
		$(item).text(index + 1);
	});

	this.addKeywordRankingClickEvent();
}

AfterhoursKeyword.prototype.showAfterhoursKeywordData = function(list) {
	if (list == undefined) {
		$(".content .dataTable tbody").append(" <tr><td colspan=\"5\">데이터가 존재하지 않습니다.</td></tr>");
		return;
	}

	function compare(a, b) {
		return a.updownRate > b.updownRate ? -1 : a.updownRate < b.updownRate ? 1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var dataObj = list[i];
		$(".content .dataTable tbody").append(this._oneTupleHtmlTag.replace("(stockName)", dataObj.name)
			.replace("(cur_price)", addComma(dataObj.curPrice))
			.replace("(updown_price)", addComma(dataObj.updownPrice))
			.replace("(stockCode)", dataObj.code)
			.replace("(naverUrl)", "<a href=\"https://finance.naver.com/item/main.nhn?code=" + convertCode(dataObj.code) + "\" target=\"_blank\">네이버증권</a>")
			.replace("(daumUrl)", "<a href=\"https://finance.daum.net/quotes/A" + convertCode(dataObj.code) + "#home\" target=\"_blank\">다음증권</a>")
			.replace("(updownPriceColor)", dataObj.updownRate > 0 ? "class=\"txtRed\"" : "class=\"txtBlue\"")
			.replace("(icon_image)", dataObj.updownRate > 0 ? dataObj.updownRate >= 29.5 ? "../images/ico_arrow_up.png" : "../images/ico_up.png" : dataObj.updownRate <= -29.5 ? "../images/ico_arrow_down.png" : "../images/ico_down.png")
			.replace("(updown_rate)", Number(dataObj.updownRate).toFixed(2) + "%")
			.replace("(updownRateColor)", dataObj.updownRate > 0 ? "class=\"txtRed\"" : "class=\"txtBlue\"")
			.replace("(volumn)", addComma(dataObj.volume)));
	}

	this.addStockNameClickEvent();
}

AfterhoursKeyword.prototype.addDatePickerChangeEvent = function() {
	$(".datePicker .before, .after").off("click").click(function() {
		var localDate = afterhoursKeyword.getLocalDateInfo();
		if ($(this).hasClass("after")) {
			localDate.add(1, "d");
			afterhoursKeyword.getAddBusinessDay(localDate);
			afterhoursKeyword._currentMovingDirection = "add";
		} else {
			localDate.subtract(1, "d");
			afterhoursKeyword.getSubBusinessDay(localDate);
			afterhoursKeyword._currentMovingDirection = "sub";
		}

		afterhoursKeyword.loadAfterhoursKeywordList();
	});
}

AfterhoursKeyword.prototype.addKeywordRankingClickEvent = function() {
	$(".ranking li").off("click").click(function(e) {
		e.stopPropagation();
		var kid = $(this).data("id");
		var keywordIncludStockList = $(this).data("stockcodearr");
		
		if (afterhoursKeyword._currentRankingKeywordId == kid) {
			$(".dataTable tr").removeClass("includeKeywordLine");
			afterhoursKeyword._currentRankingKeywordId = 0;
			return;
		} else {
			$(".dataTable tr").removeClass("includeKeywordLine");
		}

		afterhoursKeyword._currentRankingKeywordId = kid;

		$(".dataTable tr").each(function(index, item) {
			if (keywordIncludStockList.includes(String($(item).data("code")))) {
				$(item).addClass("includeKeywordLine");
			}
		});
		$(window).scrollTop($(".includeKeywordLine").position().top);
	});
}

AfterhoursKeyword.prototype.addStockNameClickEvent = function() {
	$(".dataTable tr:not(.tableTitle) td:nth-child(1)").off("click").click(function(e) {
		e.stopPropagation();
		var code = $(this).parent().data("code");
		var td = $(this);
		afterhoursKeyword._currentStockCode = code;

		$.ajax({
			url: DOMAIN + "/stock/stockManage/selectStockKeywordList",
			method: "GET",
			dataType: "json",
			data: "&code=" + code,
			success: function(keywordList, status) {
				$(".hoverMenu").css("display", "none");
				td.find(".hoverMenu li:last-child").empty();
				if (status == "success") {
					for (var i = 0; i < keywordList.length; i++) {
						td.find(".hoverMenu li:last-child").append(afterhoursKeyword._simpleKeywordTag.replace("(keywordName)", keywordList[i]));
					}
					td.find(".hoverMenu").css("display", "block");
				} else {
					td.find(".hoverMenu").css("display", "block");
				}

			}, // end success
			error: function() {
				alert("키워드 리스트를 불러오지 못했습니다");
			}
		}); // end ajax	
	});

	$("*:not(.dataTable tr:not(.tableTitle) td:nth-child(1))").click(function() {
		$(".hoverMenu").css("display", "none");
	});
}

AfterhoursKeyword.prototype.addKeyboardEvent = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		} else if (e.keyCode == 39) {
			$(".datePicker .after").click();
		}
	});
}

AfterhoursKeyword.prototype.addKeyboardEvent2 = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		}
	});
}

AfterhoursKeyword.prototype.getSubBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.subtract(2, "d");
	} else if (localDate.day() == 6) {
		localDate.subtract(1, "d");
	}
}

AfterhoursKeyword.prototype.getAddBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.add(1, "d");
	} else if (localDate.day() == 6) {
		localDate.add(2, "d");
	}
}

AfterhoursKeyword.prototype.setAfterhoursKeywordDataAmount = function(amount) {
	this._afterhoursKeywordDataListArgAmount = amount;
}

AfterhoursKeyword.prototype.setAfterhoursKeywordData = function(list) {
	this._afterhoursKeywordDataList = list;
}

AfterhoursKeyword.prototype.setTopKeywordDataAmount = function(amount) {
	this._topKeywordDataListArgAmount = amount;
}

AfterhoursKeyword.prototype.setTopKeywordData = function(list) {
	this._topKeywordDataList = list;
}

AfterhoursKeyword.prototype.setLocalDateInfo = function(localDate) {
	this._localDateInfo = localDate;
}

AfterhoursKeyword.prototype.getLocalDateInfo = function() {
	return this._localDateInfo;
}				
