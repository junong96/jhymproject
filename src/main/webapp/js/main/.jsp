<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<table class="table table-hover">
	<thead>
		<tr>

		</tr>
	</thead>
	<thead>
	</thead>
	<tbody>
		<tr>
			<td>
				<h3 class="title">keywordManage</h3>
				<table class="stockTable">
					<tr>
						<td style="padding-right: 30px;">
							<h3>Please choose Stock</h3> <label for="stockName">검색
								: </label> <input type="text" id="stockName"><br> <select
							name="stockList" multiple size="100" id="stockList"
							style="width: 250px; height: 600px;">

						</select>
						</td>
						<td style="padding-right: 30px;">
							<h3>Selected Stock</h3> <select name="selectStockList" multiple
							size="100" id="selectStockList"
							style="width: 250px; height: 600px;">
						</select>
						<br>
						<button id="clearBtn" style="margin:10px; width:100px;">비우기</button>
						</td>
						<td style="padding-right: 30px;">
							<h3>Keywords</h3> <select name="keywords" multiple size="30"
							id="keywords" style="width: 250px; height: 200px;">
						</select>

							<h3>Input Keyword</h3> <textarea id="keywordList" rows="10"
								cols="30"
								placeholder="키워드를 입력해주세요. 각 키워드는 공백으로 구분합니다. ex)keyword1 keyword2.."></textarea>
							<div class="inputResult"></div>
						</td>
						<td>
							<button id="insertBtn">삽입</button> <br> <br> <br>
							<button id="deleteBtn">삭제</button>
						</td>
						<td style="padding-left: 50px;">
							<h3>Keyword Detail</h3><select name="selectStockKeywordView" 
							size="100" id="selectStockKeywordView"
							style="width: 800px; height: 600px; overflow: scroll;">

						</select>
						</td>
					</tr>
					<tr>
						<td colspan=5>
							<p style="text-align: left; margin-top: 20px;">
								종목을 더블클릭 하면 Selected Stock 에 추가되게 됩니다.<br>
								Selected Stock 에서 종목 더블 클릭시 선택이 해제됩니다.<br> Selected Stock 에
								종목이 있는 상태에서 Input Keyword 에 키워드를 입력 후 입력 버튼 클릭 시 키워드가
								추가됩니다. <br> Selected Stock 에 종목이 있는 상태에서 keywords 에 키워드 선택
								후 삭제 시 해당 종목에 키워드 연결이 해제됩니다. // ctrl, shift 키로 다중선택 가능<br>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>