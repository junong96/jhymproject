/**
 * 
 */

function Profile() {
	this._init();
}

Profile.prototype._init = function() {
	this.showProfileArea();
	this.addLogoutBtnClickEvent();
	this.addLoginBtnClickEvent();
	this.addSignupBtnClickEvent();
}

Profile.prototype.showProfileArea = function() {
	if (isLogin == "true") {
		$(".aftLogin").css("display", "block");
	} else {
		$(".preLogin").css("display", "block");
	}
}

Profile.prototype.addLogoutBtnClickEvent = function() {
	$(".logoutBtn").off("click").click(function() {
		$.ajax({
			url: DOMAIN + "/stock/main/logout",
			method: "POST",
			success: function() {
				location.replace("");
			},
			error: function() {
				alert("로그아웃 처리 중 오류가 발생하였습니다. 다시 시도해주세요.");
			}
		}); // end ajax
	});
}

Profile.prototype.addLoginBtnClickEvent = function() {
	$(".loginBtn").off("click").click(function() {
		sessionStorage.setItem("prevLocation",location.href);
		location.href = DOMAIN + "/index.jsp";
	});
}

Profile.prototype.addSignupBtnClickEvent = function() {
	$(".joinBtn").off("click").click(function() {
		location.href = DOMAIN + "/main/join.jsp";
	});
}