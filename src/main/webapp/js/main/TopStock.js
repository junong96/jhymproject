/**
 * 
 */

function TopStock() {
	this._init();

}

TopStock.prototype._init = function() {
	this._oneTupleHtmlTag = "   <tr data-code=\"(stockCode)\"> " +
		"   <td class=\"stockTit\">(stockName) " +
		" <ul class=\"hoverMenu\"> " +
		"     <li class=\"showStockDetail\">종목 상세정보</li>" +
		"     <li>(naverUrl)</li>" +
		"     <li>(daumUrl)</li>" +
		"     <li class=\"hoverKeywordList\"></li>" +
		"     </ul>" +
		"    </td> " +
		"  <td>(cur_price)</td> " +
		"   <td (updownPriceColor)><img src=\"(icon_image)\">(updown_price)</td> " +
		"   <td (updownRateColor)>(updown_rate)</td> " +
		"    <td>(sum)</td> " +
		"</tr>";
	this._rankingTupleHtmlTag = "<li data-id=\"(keywordId)\" data-stockcodearr=\'(stockCodeArr)\'><span>(ranking)</span><span class=\"keywordName\">(keywordName)[(amount)]</span></li>";
	this._simpleKeywordTag = "<span>(keywordName)</span>";
	this._noneDataDayIndex = 0;
	var localDate = moment();
	this.getSubBusinessDay(localDate);
	this._fixedLocalDate = moment(localDate);


	this.setLocalDateInfo(localDate);
	this.loadTopStockList();
	this.addDatePickerChangeEvent();
	this.searchConditionChangeEvent();
	this.addKeyboardEvent2();
}

TopStock.prototype.loadTopStockList = function() {
	$(".content .dataTable tbody tr:not(.tableTitle)").remove();
	var date = this._localDateInfo.format("yyyy-MM-DD");
	$(".current_date").text(date);

	$.ajax({
		url: DOMAIN + "/stock/stockManage/topStock",
		method: "GET",
		data: "&date=" + date + this.getSearchConfition(),
		dataType: "json",
		success: function(keywordDataListObj) {
			topStockDataList = keywordDataListObj.topStockDataList;
			topKeywordList = keywordDataListObj.topKeywordList;

			if (topStock._fixedLocalDate.diff(topStock._localDateInfo.format("yyyy-MM-DD"), 'd') > 0) {
				$(".after").removeClass("display_none");
				topStock.addKeyboardEvent();
			} else {
				$(".after").addClass("display_none");
				topStock.addKeyboardEvent2();
			}

			if (topStockDataList.length != 0) {
				topStock._noneDataDayIndex = 0;
				topStock.setTopStockDataAmount(topStockDataList.length);
				topStock.setTopStockData(topStockDataList);
				topStock.showTopStockData(topStockDataList);
			} else {
				topStock._noneDataDayIndex += 1;
				if (topStock._noneDataDayIndex > 10) {
					topStock._noneDataDayIndex = 0;
					topStock.setTopStockDataAmount(0);
					topStock.showTopStockData();
					return;
				}

				if (topStock._currentMovingDirection == "add") {
					if (topStock._fixedLocalDate.diff(topStock._localDateInfo.format("yyyy-MM-DD"), 'd') == 0) {
						topStock._noneDataDayIndex = 0;
						topStock.setTopStockDataAmount(0);
						topStock.showTopStockData();
						return;
					}

					topStock._localDateInfo.add(1, "d");
					topStock.getAddBusinessDay(topStock._localDateInfo);
					topStock.loadTopStockList();
				} else {
					topStock._localDateInfo.subtract(1, "d");
					topStock.getSubBusinessDay(topStock._localDateInfo);
					topStock.loadTopStockList();
				}


			}

			if (topKeywordList.length != 0 && topKeywordList.length != undefined) {
				topStock.setTopKeywordDataAmount(topKeywordList.length);
				topStock.setTopKeywordData(topKeywordList);
				topStock.showTopKeywordData(topKeywordList);
			} else {
				topStock.setTopKeywordDataAmount(0);
				topStock.showTopKeywordData();
			}

		}, // end success
		error: function() {
			alert("매수 급등 목록을 불러오지 못했습니다");
		}
	}); // end ajax	
}

TopStock.prototype.showTopKeywordData = function(list) {
	$(".ranking .top5, .top10").empty();
	if (list == undefined) {
		$(".ranking .top5").append("<li>데이터가 존재하지 않습니다.</li>");
		return;
	}

	function compare(a, b) {
		return a.amount > b.amount ? -1 : a.amount < b.amount ? 1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < 50; i++) {
		var keywordObj = list[i];
		if (keywordObj == undefined) break;

		if (keywordObj.amount <= 1) continue;

		if (i < 10) {
			$(".ranking .top5").append(this._rankingTupleHtmlTag.replace("(ranking)", i + 1)
				.replace("(keywordName)", keywordObj.keyword)
				.replace("(keywordId)", keywordObj.id)
				.replace("(amount)", keywordObj.amount)
				.replace("(stockCodeArr)", JSON.stringify(keywordObj.stockCodeArr))
				.replace("(sumRate)", keywordObj.sumRate));
		} else {
			$(".ranking .top10").append(this._rankingTupleHtmlTag.replace("(ranking)", i + 1)
				.replace("(keywordName)", keywordObj.keyword)
				.replace("(keywordId)", keywordObj.id)
				.replace("(amount)", keywordObj.amount)
				.replace("(stockCodeArr)", JSON.stringify(keywordObj.stockCodeArr))
				.replace("(sumRate)", keywordObj.sumRate));
		}
	}
	var rankingKeywordList = Array.from(new Set($(".ranking ul li")));


	doKeywordMapping(rankingKeywordList)

	$($(".ranking li span:not(.keywordName)")).each(function(index, item) {
		$(item).text(index + 1);
	});

	this.addKeywordRankingClickEvent();

}

TopStock.prototype.showTopStockData = function(list) {
	if (list == undefined) {
		$(".content .dataTable tbody").append(" <tr><td colspan=\"5\">데이터가 존재하지 않습니다.</td></tr>");
		return;
	}
	if (this._type == "amount") {
		$(".tableTitle th:last-child").text("순매수")
	} else {
		$(".tableTitle th:last-child").text("순매수(억)")
	}

	for (var i = 0; i < list.length; i++) {
		var dataObj = list[i];
		$(".content .dataTable tbody").append(this._oneTupleHtmlTag.replace("(stockName)", dataObj.name)
			.replace("(cur_price)", addComma(dataObj.curPrice))
			.replace("(stockCode)", dataObj.code)
			.replace("(naverUrl)", "<a href=\"https://finance.naver.com/item/main.nhn?code=" + convertCode(dataObj.code) + "\" target=\"_blank\">네이버증권</a>")
			.replace("(daumUrl)", "<a href=\"https://finance.daum.net/quotes/A" + convertCode(dataObj.code) + "#home\" target=\"_blank\">다음증권</a>")
			.replace("(updown_price)", addComma(dataObj.updownPrice))
			.replace("(updownPriceColor)", dataObj.updownRate > 0 ? "class=\"txtRed\"" : dataObj.updownRate == 0 ? "class=\"txtBlack\"" : "class=\"txtBlue\"")
			.replace("(icon_image)", dataObj.updownRate == 0 ? "../images/ico_zero.png" : dataObj.updownRate > 0 ? dataObj.updownRate >= 29.5 ? "../images/ico_arrow_up.png" : "../images/ico_up.png" : dataObj.updownRate <= -29.5 ? "../images/ico_arrow_down.png" : "../images/ico_down.png")
			.replace("(updown_rate)", Number(dataObj.updownRate).toFixed(2) + "%")
			.replace("(updownRateColor)", dataObj.updownRate > 0 ? "class=\"txtRed\"" : dataObj.updownRate == 0 ? "class=\"txtBlack\"" : "class=\"txtBlue\"")
			.replace("(sum)", addComma(dataObj.sum)));
	}

	this.addStockNameClickEvent();
}

TopStock.prototype.addDatePickerChangeEvent = function() {
	$(".datePicker .before, .after").off("click").click(function() {
		var localDate = topStock.getLocalDateInfo();
		if ($(this).hasClass("after")) {
			localDate.add(1, "d");
			topStock.getAddBusinessDay(localDate);
			topStock._currentMovingDirection = "add";
		} else {
			localDate.subtract(1, "d");
			topStock.getSubBusinessDay(localDate);
			topStock._currentMovingDirection = "sub";
		}

		topStock.loadTopStockList();
	});
}

TopStock.prototype.addKeyboardEvent = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		} else if (e.keyCode == 39) {
			$(".datePicker .after").click();
		}
	});
}

TopStock.prototype.addKeywordRankingClickEvent = function() {
	$(".ranking li").off("click").click(function(e) {
		e.stopPropagation();
		var kid = $(this).data("id");
		var keywordIncludStockList = $(this).data("stockcodearr");
		
		if (topStock._currentRankingKeywordId == kid) {
			$(".dataTable tr").removeClass("includeKeywordLine");
			topStock._currentRankingKeywordId = 0;
			return;
		} else {
			$(".dataTable tr").removeClass("includeKeywordLine");
		}

		topStock._currentRankingKeywordId = kid;

		$(".dataTable tr").each(function(index, item) {
			if (keywordIncludStockList.includes(String($(item).data("code")))) {
				$(item).addClass("includeKeywordLine");
			}
		});
		$(window).scrollTop($(".includeKeywordLine").position().top);
	});
}

TopStock.prototype.addStockNameClickEvent = function() {
	$(".dataTable tr:not(.tableTitle) td:nth-child(1)").off("click").click(function(e) {
		e.stopPropagation();
		var code = $(this).parent().data("code");
		var td = $(this);
		topStock._currentStockCode = code;

		$.ajax({
			url: DOMAIN + "/stock/stockManage/selectStockKeywordList",
			method: "GET",
			dataType: "json",
			data: "&code=" + code,
			success: function(keywordList, status) {
				$(".hoverMenu").css("display", "none");
				td.find(".hoverMenu li:last-child").empty();
				if (status == "success") {
					for (var i = 0; i < keywordList.length; i++) {
						td.find(".hoverMenu li:last-child").append(topStock._simpleKeywordTag.replace("(keywordName)", keywordList[i]));
					}
					td.find(".hoverMenu").css("display", "block");
				} else {
					td.find(".hoverMenu").css("display", "block");
				}

			}, // end success
			error: function() {
				alert("키워드 리스트를 불러오지 못했습니다");
			}
		}); // end ajax	
	});

	$("*:not(.dataTable tr:not(.tableTitle) td:nth-child(1))").click(function() {
		$(".hoverMenu").css("display", "none");
	});
}

TopStock.prototype.getSearchConfition = function() {
	var type = $(".radioWrap input.type:checked").val();
	var stockType = $(".radioWrap input.stockType:checked").val();
	this._type = type;
	var searchConditionCheckBoxArr = $(".radioWrap .filter button.active");
	var searchConditionArr = new Array();
	if (type == "amount") {
		for (var i = 0; i < searchConditionCheckBoxArr.length; i++) {
			searchConditionArr.push(searchConditionCheckBoxArr[i].value);
		}
	} else {
		for (var i = 0; i < searchConditionCheckBoxArr.length; i++) {
			searchConditionArr.push(searchConditionCheckBoxArr[i].value + "_p");
		}
	}

	return "&type=" + type + "&stockType=" + stockType + "&searchConditionArr=" + encodeURIComponent(JSON.stringify(searchConditionArr));
}

TopStock.prototype.searchConditionChangeEvent = function() {
	$("input[type=checkbox], input[type=radio].type, input[type=radio].stockType").off("change").change(function() {
		topStock.loadTopStockList();
	});

	$(".searchTab .filter button").off("click").click(function() {
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
		} else {
			$(this).addClass("active");
		}

		if ($(".searchTab .filter button.active").length == 0) {
			$(this).addClass("active");
			return;
		}
		topStock.loadTopStockList();
	});
}

TopStock.prototype.addKeyboardEvent2 = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		}
	});
}

TopStock.prototype.getSubBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.subtract(2, "d");
	} else if (localDate.day() == 6) {
		localDate.subtract(1, "d");
	}
}

TopStock.prototype.getAddBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.add(1, "d");
	} else if (localDate.day() == 6) {
		localDate.add(2, "d");
	}
}

TopStock.prototype.setTopStockDataAmount = function(amount) {
	this._topStockDataListArgAmount = amount;
}

TopStock.prototype.setTopStockData = function(list) {
	this._topStockDataList = list;
}

TopStock.prototype.setTopKeywordDataAmount = function(amount) {
	this._topKeywordDataListArgAmount = amount;
}

TopStock.prototype.setTopKeywordData = function(list) {
	this._topKeywordDataList = list;
}

TopStock.prototype.setLocalDateInfo = function(localDate) {
	this._localDateInfo = localDate;
}

TopStock.prototype.getLocalDateInfo = function() {
	return this._localDateInfo;
}				
