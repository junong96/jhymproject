/**
 * 
 */

function StockCalendar() {
	this._init();

}

StockCalendar.prototype._init = function() {
	var initialLocaleCode = 'ko';
	var calendarEl = document.getElementById('calendarDiv');
	this.calendar = new FullCalendar.Calendar(calendarEl, {
		initialView: 'dayGridMonth',
		headerToolbar: {
			left: 'prev,next today',
			center: 'title',
			right: 'dayGridMonth,dayGridWeek,dayGridDay,listMonth'
		},
		locale: initialLocaleCode,
		buttonIcons: true,
		navLinks: true,  //날짜 클릭해서 이동
		editable: true, //이벤트 수정 가능  여부
		dayMaxEvents: true, // 특정 날 이벤트 표시 제한
		selectable: true,
		selectMirror: false,
		select: function(arg) {
		},
		eventClick: function(arg) {
		},
		events: [
			{
				title: 'All Day Event',
				start: '2021-05-01'
			},
			{
				title: 'Long Event',
				start: '2021-05-07',
				end: '2021-05-10'
			},
			{
				groupId: 999,
				title: 'Repeating Event',
				start: '2021-05-09'
			},
			{
				groupId: 999,
				title: 'Repeating Event',
				start: '2021-05-16T16:00:00'
			},
			{
				title: 'Conference',
				start: '2021-05-11',
				end: '2021-05-13'
			},
			{
				title: 'Meeting',
				start: '2021-05-12T10:30:00',
				end: '2021-05-12T12:30:00'
			},
			{
				title: 'Lunch',
				start: '2021-05-12T12:00:00'
			},
			{
				title: 'Meeting',
				start: '2021-05-12T14:30:00'
			},
			{
				title: 'Happy Hour',
				start: '2021-05-12T17:30:00'
			},
			{
				title: 'Dinner',
				start: '2021-05-12T20:00:00'
			},
			{
				title: 'Birthday Party',
				start: '2021-05-13T07:00:00'
			},
			{
				title: 'Click for Google',
				url: 'http://google.com/',
				start: '2021-05-28'
			}
		]
	});

	this.calendar.render(); // 캘린더 구현
	
	this.addPrevAndNextBtnClickEvent();
}

StockCalendar.prototype.addPrevAndNextBtnClickEvent = function(){
	$(".fc-prev-button, fc-next-button").off("click").click(function(){
		console.log(stockCalendar.calendar.getDate().format("yyyy-MM-dd"));		
	});
}
