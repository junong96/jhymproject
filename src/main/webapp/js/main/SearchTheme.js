/**
 * 
 */

function SearchTheme() {
	this._init();
	this._searchType = "keyword";
	this._searchLogic = "and";
	this._searchInputTagWidth = $(".searchValue").width();
	var akOffsetLeft = $(".searchInput .addedkeyword").offset().left;
	var svOffsetLeft = $(".searchValue").offset().left;
	this._offsetSub = akOffsetLeft - svOffsetLeft;
}

SearchTheme.prototype._init = function() {
	$.ajax({
		url: DOMAIN + "/stock/stockManage/stockAndKeywordList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				searchTheme._keywordNameList = list.keywordList;
				searchTheme._stockNameList = list.stockList;
				autocomplete.setAutocomplete($(".searchValue").get(0), searchTheme._keywordNameList);
			} // end if
			else {
				searchTheme._stockNameList = new Array();
				searchTheme._keywordNameList = new Array();
			}
		}, // end success
		error: function() {
			alert("주식, 키워드 품목을 불러오지 못했습니다");
		}
	}); // end ajax	

	this._htmlTag = "	<div class=\"stock\" data-code=\"(stockCode)\"> " +
		"	<div class=\"conTitle\"> " +
		"			<h3>(stockName)</h3> " +
		" <ul class=\"hoverMenu\"> " +
		"     <li class=\"showStockNote\">종목 노트</li>" +
		"     <li class=\"showStockDetail\">종목 상세정보</li>" +
		"     <li>(naverUrl)</li>" +
		"     <li>(daumUrl)</li>" +
		"     </ul>" +
		"	</div> " +
		"	<div class=\"conKeyword\"> " +
		"	</div> " +
		"	</div>";
	this._keywordHtmlTag = " <span (onChecked) class=\"keywordArg\" data-kid=\"(keywordID)\">(keywordName)</span>";

	this._addKeywordHtmlTag = "<li>(keywordName)<span><img src=\"" + prePath + "images/ico_delete.png\" alt=\"키워드 삭제 버튼\"></span></li>";

	this._addHtmlTag = "<span class=\"add\"><img src=\"" + prePath + "images/ico_add.png\" alt=\"키워드 추가 버튼\"></span>"

	this._keywordLiTagHtml = "<li>(keywordName)<span class=\"removeKeyword\"><img src=\"" + prePath + "images/ico_delete.png\" alt=\"키워드 삭제 버튼\"></span></li>";

	this._adminWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\"> " +
		"	<div class=\"adminInfo\">" +
		"		<h4>(title)</h4>" +
		"		<p>(contents)</p> " +
		"		<span>(dateTime)</span>" +
		"	</div>" +
		"</div>";

	this._myMemoWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\" data-type=\"memo\"> " +
		"	<div class=\"myMemo\">" +
		"		<h4>(title)</h4>" +
		"	    <img class=\"more\" src=\"" + prePath + "images/ico_more_bk.png\" alt=\"메모 더보기 버튼\">" +
		"		<p class=\"contents\">(contents)</p> " +
		"		<span>(dateTime)</span>" +
		"  		<ul>" +
		"      		 <li>수정</li>" +
		"       	 <li>삭제</li>" +
		"       </ul> " +
		"	</div>" +
		"</div>";

	this._myUrlMemoWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\" data-type=\"url\"> " +
		"	<div class=\"myMemo\">" +
		"		<h4>(title)</h4>" +
		"	    <img class=\"more\" src=\"" + prePath + "images/ico_more_bk.png\" alt=\"메모 더보기 버튼\">" +
		"		<p class=\"contents\"> <a href=\"(contentsUrl)\" target=\"_blank\">(contents)</a></p> " +
		" <div class=\"timg\">" +
		"    <p class=\"ogImageUrl\"></p>" +
		"     <a href=\"(contentsUrl)\" target=\"_blank\"><div class=\"tTxt\">" +
		"        <h5 class=\"ogTitle\"></h5>" +
		"     <span class=\"ogUrl\"></span>" +
		"      </div></a>" +
		"       </div>" +
		"		<span>(dateTime)</span>" +
		"  		<ul>" +
		"      	 	<li>수정</li>" +
		"        	<li>삭제</li>" +
		"       </ul> " +
		"	</div>" +
		"</div>";

	this._defaultWbox = "<div class=\"gBox\">" +
		"<div class=\"myMemo memoNodata\">" +
		"	<p>오른쪽의 [+] 버튼을 클릭해 해당 종목에 대한 메모/url을 둥록해 주세요.</p>" +
		"</div>";
	this._offset = 0;
	this._searchedKeywordArr = new Array();
	this._currentSearchKeywordArr = new Array();
	this.addSearchTypeClickEvent();
	this.addSearchBtnClickEvent();
	this.addSearchAreaSpaceEvent();

	if (getCookie("searchedKeywordArr") != undefined) {
		this._searchedKeywordArr = getCookie("searchedKeywordArr").split(',');
	}

	for (var i = 0; i < this._searchedKeywordArr.length; i++) {
		$(".searchedKeyword ul").append(this._keywordLiTagHtml.replace("(keywordName)", this._searchedKeywordArr[i]));
	}

	this.addKeywordDeleteBtnEvent();
	this.addKeywordClickEvent();

	this.addMemoTabClickEvent();
	this.addInputMemoTabClickEvent();
	this.addInputUrlMemoTabClickEvent();
	this.addSubmitBtnClickEvent();
	this.addKeywordAddRequestAreaEvent();
}

SearchTheme.prototype.loadStockList = function() {
	$(".content .con_01").empty();
	var dataStr = this._searchDataStr;
	$.ajax({
		url: DOMAIN + "/stock/stockManage/stockList",
		method: "GET",
		data: dataStr,
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				searchTheme.setStockAmount(list.length);
				searchTheme.setStockList(list);
				var spliceArr = searchTheme.spliceList();
				searchTheme.showStockList(spliceArr);
			} // end if
			else {
				searchTheme.setStockAmount(0);
				searchTheme.showStockList();
			}

			$(".backG>.wBox:nth-child(1)").css("width", "100%");
			$(".backG .con_02").css("display", "none");
			$(".fab").css("display", "none");
			$(".con_01 div").removeClass("currentViewStock");
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax	
}

SearchTheme.prototype.showStockList = function(list) {
	if (list == undefined) {
		$(".content .con_01").text("검색 결과가 존재하지 않습니다.");
		return;
	}

	searchTheme.loadList = list;

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$(".content .con_01").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(stockName)", stockObj.name)
			.replace("(naverUrl)", "<a href=\"https://finance.naver.com/item/main.nhn?code=" + convertCode(stockObj.code) + "\" target=\"_blank\">네이버증권</a>")
			.replace("(daumUrl)", "<a href=\"https://finance.daum.net/quotes/A" + convertCode(stockObj.code) + "#home\" target=\"_blank\">다음증권</a>"));
		var keywordArr = stockObj.keywordArr;

		for (var j = 0; j < keywordArr.length; j++) {
			var keyword = keywordArr[j];

			var filterList = searchTheme._currentSearchKeywordArr.filter(function(obj) {
				return obj.toUpperCase() == keyword.keyword.toUpperCase();
			});

			$(".content .stock .conKeyword:last").append(this._keywordHtmlTag.replace("(onChecked)", filterList.length > 0 ? "class=\"on\"" : "")
				.replace("(keywordID)", keyword.KID).replace("(keywordName)", keyword.keyword));
		}
		$(".content .stock .conKeyword:last").append(this._addHtmlTag);
	}
	this._offset = $(".content .stock").length;

	if ($(".content .stock").length < this._allAmount) {
		$(window).scroll(scrollEvent); // end scroll
	} // end if

	//키워드 클릭 시 검색에 추가
	$(".keywordArg").off("click").click(function() {
		if (searchTheme._searchType == "keyword") {
			if (searchTheme._currentSearchKeywordArr.includes($(this).text().toUpperCase())) {
				return;
			}

			$(".searchInput .addedkeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", $(this).text()));
			searchTheme.setSearchInputPosition();
			searchTheme.addAddedKeywordDeleteBtnEvent();
			//TODO 바로 검색 할지는 상의 필요
			$("#searchBtn").click();
		}
	});

	if ($(".backG").height() <= $(".con_01").height()) {
		$(".backG").height($(".con_01").height());
	}

	this.addBtnEventListener();
	this.addShowHoverMenuEvent();
	this.addHoverMenuListClickEvent();
}

function scrollEvent() {
	var $window = $(window);
	var scrollTop = $window.scrollTop();
	var documentHeight = $(document).height();
	var windowHeight = $(window).height();

	var middle = (documentHeight - windowHeight) / 2;

	if (scrollTop >= middle) {
		$(window).off("scroll", scrollEvent);
		if (searchTheme.loadList.length == 50) {
			blockUI();
			var spliceArr = searchTheme.spliceList();
			searchTheme.showStockList(spliceArr);
			unblockUI();
		}
	} // end if
}

SearchTheme.prototype.addSearchBtnClickEvent = function() {
	$("#searchBtn").off("click").click(function() {
		var dataStr = "";

		if (searchTheme._searchType == "keyword") {
			dataStr += "&type=keyword";

			var searchKeywordTagArr = $(".searchInput .addedkeyword li");
			if (searchKeywordTagArr.length > 0) {
				var searchKeywordArr = new Array();
				for (var i = 0; i < searchKeywordTagArr.length; i++) {
					var searchKeywordName = searchKeywordTagArr.eq(i).text().trim();
					if (!searchKeywordArr.includes(searchKeywordName.toUpperCase())) {
						searchKeywordArr.push(searchKeywordName.toUpperCase());
					}

					if (!searchTheme._searchedKeywordArr.includes(searchKeywordName.toUpperCase())) {
						$(".searchedKeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", searchKeywordName));
						searchTheme.pushSearchedKeyword(searchKeywordName.toUpperCase());
					}

				}//end of For

				searchTheme.addKeywordDeleteBtnEvent();
				searchTheme._currentSearchKeywordArr = searchKeywordArr;
				dataStr += "&keywords=" + encodeURIComponent(JSON.stringify(searchKeywordArr));


				setCookie("searchedKeywordArr", searchTheme._searchedKeywordArr, 1);

			}
		} else {
			dataStr += "&type=stock";
			if ($(".searchValue").val().trim() != "") {
				var searchWord = $(".searchValue").val().trim();
				dataStr += "&searchWord=" + encodeURIComponent(searchWord);
				if (!searchTheme._searchedKeywordArr.includes(searchWord)) {
					$(".searchedKeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", searchWord));
					searchTheme.pushSearchedKeyword(searchWord);
				};
				setCookie("searchedKeywordArr", searchTheme._searchedKeywordArr, 1);
				searchTheme._currentSearchKeywordArr = [];
				searchTheme.addKeywordDeleteBtnEvent();
			}
		}

		searchTheme.addKeywordClickEvent();

		if (searchTheme._searchLogic == "and") {
			dataStr += "&logic=and";
		} else {
			dataStr += "&logic=or";
		}
		searchTheme._searchDataStr = dataStr;
		searchTheme.loadStockList();
	});
}

SearchTheme.prototype.addSearchTypeClickEvent = function() {
	$(".searchTab .type button").off("click").click(function() {
		if ($(this).data("type") == "stock") {
			$(".searchInput .addedkeyword ul").empty();
			$(".searchValue").css("width", "100%");
			$(".searchTab .logic button").removeClass();
			$(".searchTab .logic button:eq(1)").addClass("active");
			searchTheme._searchLogic = "or";
		}
		$(".searchInput .searchValue").val("");
		$(".searchTab .type button").removeClass();
		$(this).addClass("active");
		searchTheme._searchType = $(this).data("type");
		if ($(this).data("type") == "stock") {
			autocomplete.setAutocomplete($(".searchValue").get(0), searchTheme._stockNameList);
		} else {
			autocomplete.setAutocomplete($(".searchValue").get(0), searchTheme._keywordNameList);
		}

	});

	$(".searchTab .logic button").off("click").click(function() {
		if (searchTheme._searchType == "stock") {
			$(".searchTab .logic button").removeClass();
			$(".searchTab .logic button:eq(1)").addClass("active");
			searchTheme._searchLogic = $(".searchTab .logic button:eq(1)").data("type");
			return;
		}

		$(".searchTab .logic button").removeClass();
		$(this).addClass("active");
		searchTheme._searchLogic = $(this).data("type");
	});
}

SearchTheme.prototype.addSearchAreaSpaceEvent = function() {

	$(".searchValue").on("keydown", function(e) {
		if (e.keyCode == 13) {
			setTimeout(function() {
				if ($("#autocomplete-list div").length == 0) {
					if (searchTheme._searchType == "keyword") {
						var keywordValue = $(".searchValue").val().trim();

						if (checkConsonant(keywordValue)) {
							alert("키워드에 자음, 모음, \' 은 포함될 수 없습니다.");
							return;
						}


						if (keywordValue.trim() != "") {
							/*if ($(".searchInput .addedkeyword li").length >= 10) {
								alert("키워드는 최대 10개 까지만 설정 가능합니다.");
								return;
							}*/

							var isExists = false;
							$(".searchInput .addedkeyword ul li").each(function(index, item) {
								if ($(item).text().trim().toUpperCase() == keywordValue.toUpperCase()) {
									isExists = true;
								}
							})

							if (isExists) {
								setTimeout(function() {
									$(".searchValue").val("");
								}, 0.01);
								return;
							}

							$(".searchInput .addedkeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", keywordValue));
							e.preventDefault();
							setTimeout(function() {
								$(".searchValue").val("");
							}, 0.01);

							searchTheme.setSearchInputPosition();
							searchTheme.addAddedKeywordDeleteBtnEvent();
						}
					}
				}
			}, 0.5);
			setTimeout(function() {
				if ($("#autocomplete-list div").length == 0) {
					$("#searchBtn").click();
				}
			}, 5);
		}
		else if (e.key == " ") {
			if (searchTheme._searchType == "keyword") {
				var keywordValue = $(".searchValue").val().trim();

				if (checkConsonant(keywordValue)) {
					e.preventDefault();
					alert("키워드에 자음, 모음, \' 은 포함될 수 없습니다.");
					return;
				}


				if (keywordValue.trim() != "") {
					/*
					if ($(".searchInput .addedkeyword li").length >= 10) {
						alert("키워드는 최대 10개 까지만 설정 가능합니다.");
						return;
					}*/
					var isExists = false;
					$(".searchInput .addedkeyword ul li").each(function(index, item) {
						if ($(item).text().trim().toUpperCase() == keywordValue.toUpperCase()) {
							isExists = true;
						}
					})

					if (isExists) {
						setTimeout(function() {
							$(".searchValue").val("");
						}, 0.01);
						return;
					}

					$(".searchInput .addedkeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", keywordValue));
					e.preventDefault();
					setTimeout(function() {
						$(".searchValue").val("");
					}, 0.01);

					searchTheme.setSearchInputPosition();
					searchTheme.addAddedKeywordDeleteBtnEvent();
					closeAllLists2();
				}
			}
		} else if ($(".searchValue").val() == "" && e.keyCode == 8) {
			if (searchTheme._searchType == "keyword") {
				e.preventDefault();
				searchTheme._currentSearchKeywordArr.splice(searchTheme._currentSearchKeywordArr.indexOf($(".searchInput .addedkeyword li:last-child").text().toUpperCase()), 1);

				$(".searchInput .addedkeyword li:last-child").remove();
				searchTheme.setSearchInputPosition();
				if ($(".searchInput .addedkeyword li").length == 0) {
					$(".searchValue").css("width", "100%");
				}
			}
		}
	});
}


SearchTheme.prototype.spliceList = function() {
	var spliceArr = this._stockList.splice(0, 50);
	return spliceArr;
}

SearchTheme.prototype.pushSearchedKeyword = function(keyword) {
	this._searchedKeywordArr.push(keyword);
}

SearchTheme.prototype.setSearchInputPosition = function() {
	var currentWidth = this._searchInputTagWidth - (this._offsetSub) / 2 - $(".searchInput .addedkeyword").width();
	$(".searchValue").css("width", Math.round((currentWidth / this._searchInputTagWidth) * 100) + "%");
}

SearchTheme.prototype.setStockList = function(list) {
	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	this._stockList = list;
}

SearchTheme.prototype.setStockAmount = function(amount) {
	this._allAmount = amount;
}

SearchTheme.prototype.refreshSearchedKeywordCookie = function($keywordObj) {
	var keyword = $keywordObj.parent().text();
	this._searchedKeywordArr.splice(this._searchedKeywordArr.indexOf(keyword), 1);
	if (this._searchedKeywordArr.length != 0) {
		setCookie("searchedKeywordArr", this._searchedKeywordArr, 1);
	} else {
		setCookie("searchedKeywordArr", '', -1);
	}
}

SearchTheme.prototype.addKeywordDeleteBtnEvent = function() {
	$(".searchedKeyword ul .removeKeyword").off("click").click(function() {
		searchTheme.refreshSearchedKeywordCookie($(this));
		$(this).parent().remove();
	});
}

SearchTheme.prototype.addKeywordClickEvent = function() {
	$(".searchedKeyword li").off("click").click(function() {
		if (searchTheme._searchType == "keyword") {
			if (searchTheme._currentSearchKeywordArr.includes($(this).text().toUpperCase())) {
				return;
			}
			$(".searchInput .addedkeyword ul").append(searchTheme._keywordLiTagHtml.replace("(keywordName)", $(this).text()));
			searchTheme.setSearchInputPosition();
			searchTheme.addAddedKeywordDeleteBtnEvent();
			$("#searchBtn").click();
		} else {
			$(".searchValue").val($(this).text());
			$("#searchBtn").click();
		}
	});
}

SearchTheme.prototype.addAddedKeywordDeleteBtnEvent = function() {
	$(".searchInput .addedkeyword ul .removeKeyword").off("click").click(function() {
		$(this).parent().remove();
		searchTheme._currentSearchKeywordArr.splice(searchTheme._currentSearchKeywordArr.indexOf($(this).parent().text().toUpperCase()), 1);
		searchTheme.setSearchInputPosition();
		if ($(".searchInput .addedkeyword li").length == 0) {
			$(".searchValue").css("width", "100%");
		}
	});
}

SearchTheme.prototype.addBtnEventListener = function() {
	$(".add").off("click").click(function() {
		if (userID == "" || userID == undefined) {
			sessionStorage.setItem("prevLocation", location.href);
			alert("로그인 후에 이용이 가능합니다.");
			location.href = DOMAIN + "/index.jsp";
			return;
		}

		var code = $(this).parent().parent().data("code");
		searchTheme._currentSelectStock = $(this).parent().parent();
		searchTheme._currentSelectStockCode = code;

		$(".addKeywordWrap").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".addKeywordWrap .inputKeyword input[type=text]").focus();
	});

	$(".popupMask, .popupWrap .close").off("click").click(function() {
		$(".popupWrap").css("display", "none");
		$(".popupMask").css("display", "none");
		$(".popupWrap .inputKeyword input[type=text]").val("");
		$(".popupWrap .addedKeyword ul").empty();
	});

	/*  enter key 
		$(".popupMemo .inputKeyword, .popupUrl .inputKeyword").off("keydown").keydown(function(e) {
			if (e.keyCode == 13) {
				setTimeout(function() {
					if (searchTheme.addMemoType == "memo") {
						$(".popupMemo button").click();
					} else {
						$(".popupUrl button").click();
					}
				}, 0.5);
				e.stopPropagation();
			}
		});
	*/
	$(document.body).keydown(function(e) {
		e.stopPropagation();
		if (e.keyCode == 27) {
			$(".popupMask").click();
		}
	});
}

SearchTheme.prototype.addShowHoverMenuEvent = function() {
	$(".conTitle h3").off("click").click(function(e) {
		e.stopPropagation();
		$(".hoverMenu").css("display", "none");
		$(this).parent().find(".hoverMenu").css("display", "block");
	});

	$("*:not(.conTitle h3)").click(function() {
		$(".hoverMenu").css("display", "none");
	});
}

SearchTheme.prototype.addHoverMenuListClickEvent = function() {
	$(".hoverMenu li:nth-child(1)").off("click").click(function() {
		if (userID == "" || userID == undefined) {
			alert("로그인 후에 이용이 가능합니다.");
			location.href = DOMAIN + "/index.jsp";
			return;
		}


		searchTheme._currentStock = $(this).parent().parent().parent();
		$(".fab").css("display", "block");

		$(".backG>.wBox:nth-child(1)").css("width", "75%");
		$(".backG .con_02").css("width", "calc(100% - 75% - .5em)");
		$(".backG .con_02").css("display", "inline-block");

		var defaultHeight = $("#container .inner").height() + $("#header").height();

		$(window).scroll(function() {
			var position = $(window).scrollTop();
			if (defaultHeight < position) {
				$(".con_02").stop().animate({ "top": position - defaultHeight + "px" }, 300);
			} else {
				$(".con_02").stop().animate({ "top": $(".backG").css("padding-top") }, 300);
			}
		});


		var code = $(this).parent().parent().parent().data("code");
		searchTheme._currentSelectStockCode = code;
		//TODO WORK 지연 시간 때문에 몇개씩 가져오는 식으로 변경해야 함 
		$.ajax({
			url: DOMAIN + "/stock/stockNote/stockNoteAmount",
			method: "GET",
			headers: $.extend(null, { "userId": userID, "code": code }),
			dataType: "json",
			success: function(publicPrivateStockNoteObj) {
				searchTheme._publicStockNoteAmount = publicPrivateStockNoteObj.publicAmount;
				searchTheme._privateStockNoteAmount = publicPrivateStockNoteObj.privateAmount;

				$(".myMemoWrap").removeClass("display_none");
				$(".adminInfoWrap").removeClass("display_none");
				$(".adminInfoWrap").empty();
				$(".myMemoWrap").empty();

				$(".memoTab li").removeClass("on");

				if (searchTheme._publicStockNoteAmount == 0) {
					$(".memoTab li:nth-child(1)").css("display", "none");
					$(".memoTab li:nth-child(2)").addClass("on");
					$(".memoTab .adminInfoWrap").addClass("display_none");
				} else {
					$(".memoTab li:nth-child(1)").css("display", "inline-block");
					$(".memoTab li:nth-child(2)").addClass("on");
					$(".wBoxWrap .adminInfoWrap").addClass("display_none");
				}

				searchTheme.loadStockNoteList(code);
				$(window).scroll();
			}, // end success
			error: function() {
				alert("종목 노트 정보를 불러오지 못했습니다");
			}
		}); // end ajax	

	});

}

//TODO WORK scroll Event넣기
SearchTheme.prototype.loadStockNoteList = function(code) {
	$.ajax({
		url: DOMAIN + "/stock/stockNote/stockNoteList",
		method: "GET",
		headers: $.extend(null, { "userId": userID, "code": code }),
		dataType: "json",
		data: "&offset=" + $(".myMemoWrap .gBox").length,
		success: function(publicPrivateStockNoteObj) {
			searchTheme._privateStockNoteList = publicPrivateStockNoteObj.privateStockNoteList;

			if (searchTheme._publicStockNoteAmount != 0 && $(".myMemoWrap .gBox").length == 0) {
				searchTheme._publicStockNoteList = publicPrivateStockNoteObj.publicStockNoteList;
				searchTheme.showPublicStockNoteList(searchTheme._publicStockNoteList);
			}

			searchTheme.showPrivateStockNoteList(searchTheme._privateStockNoteList);
		}, // end success
		error: function() {
			alert("종목 노트 정보를 불러오지 못했습니다");
		}
	}); // end ajax	
}

SearchTheme.prototype.showPrivateStockNoteList = function(privateStockNoteList) {
	if (privateStockNoteList.length != 0) {
		for (var i = 0; i < privateStockNoteList.length; i++) {
			var privateStockNote = privateStockNoteList[i];
			if (privateStockNote.isURL) {
				$(".myMemoWrap").append(searchTheme._myUrlMemoWbox.replace("(title)", privateStockNote.title)
					.replace("(contents)", privateStockNote.contents)
					.replace("(contentsUrl)", privateStockNote.contents)
					.replace("(contentsUrl)", privateStockNote.contents)
					.replace("(stockNoteId)", privateStockNote.id)
					.replace("(dateTime)", privateStockNote.dateTime));

				searchTheme.getOgDataAndMapping($(".myMemoWrap .gBox:last-child"), privateStockNote.contents);
			} else {
				$(".myMemoWrap").append(searchTheme._myMemoWbox.replace("(title)", privateStockNote.title)
					.replace("(contents)", privateStockNote.contents)
					.replace("(stockNoteId)", privateStockNote.id)
					.replace("(dateTime)", privateStockNote.dateTime));
			}
		}
	} else {
		$(".myMemoWrap").append(searchTheme._defaultWbox);
	}


	this.addStockNoteMoreIconClickEvent();

	if ($(".myMemoWrap .gBox").length < searchTheme._privateStockNoteAmount) {
		$(".con_02").scroll(scrollEventForMemo); // end scroll
	} // end if
}

SearchTheme.prototype.getOgDataAndMapping = function(stockNote, url) {
	$.ajax({
		url: DOMAIN + "/stock/stockNote/ogDataMapping",
		method: "GET",
		headers: $.extend(null, { "url": url }),
		dataType: "json",
		success: function(ogDataObj) {
			$(stockNote).find(".ogUrl").text(ogDataObj.ogUrl);
			$(stockNote).find(".ogTitle").text(ogDataObj.ogTitle);
			$(stockNote).find(".ogImageUrl").css("background-image", "url(" + ogDataObj.ogImageUrl + ")");
		}, // end success
		error: function() {
			alert("종목 노트 정보를 불러오지 못했습니다");
		}
	}); // end ajax	
	unblockUI();
}

function scrollEventForMemo() {
	var $window = $(".con_02");
	var scrollTop = $window.scrollTop();
	var documentHeight = $(".myMemoWrap").height();
	var windowHeight = $(window).height();

	var middle = (documentHeight - windowHeight) / 2;

	if (scrollTop >= middle) {
		$(".con_02").off("scroll", scrollEventForMemo);
		if (searchTheme._privateStockNoteList.length == 10) {
			searchTheme.loadStockNoteList(searchTheme._currentSelectStockCode);
		}
	} // end if
}

SearchTheme.prototype.showPublicStockNoteList = function(publicStockNoteList) {

	for (var i = 0; i < publicStockNoteList.length; i++) {
		var publicStockNote = publicStockNoteList[i];
		$(".adminInfoWrap").append(searchTheme._adminWbox.replace("(title)", publicStockNote.title)
			.replace("(contents)", publicStockNote.contents)
			.replace("(stockNoteId)", publicStockNote.id)
			.replace("(dateTime)", publicStockNote.dateTime));
	}
}

SearchTheme.prototype.addMemoTabClickEvent = function() {
	$(".memoTab li").off("click").click(function() {
		if ($(this).hasClass("infoTab")) {
			$(".myMemoWrap").addClass("display_none");
			$(".adminInfoWrap").removeClass("display_none");
			$(".memoTab li").removeClass("on");
			$(".memoTab .infoTab").addClass("on");
		} else {
			$(".adminInfoWrap").addClass("display_none");
			$(".myMemoWrap").removeClass("display_none");
			$(".memoTab li").removeClass("on");
			$(".memoTab .myMemoTab").addClass("on");
		}
	});
}


SearchTheme.prototype.addInputUrlMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(1)").off("click").click(function() {
		$(".popupMemo").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupMemo .popupTitle").text("메모 등록");
		$(".popupMemo button").text("등록");
		$(".popupMemo .inputKeyword input[type=text]:eq(0)").focus();
		searchTheme.addMemoType = "memo";
		searchTheme.popupType = "write";
	});

}


SearchTheme.prototype.addInputMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(2)").off("click").click(function() {
		$(".popupUrl").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupUrl .popupTitle").text("링크 등록");
		$(".popupUrl button").text("등록");
		$(".popupUrl .inputKeyword input[type=text]:eq(0)").focus();
		searchTheme.addMemoType = "url";
		searchTheme.popupType = "write";
	});
}

SearchTheme.prototype.addStockNoteMoreIconClickEvent = function() {
	$(".myMemo .more").off("click").click(function(e) {
		e.stopPropagation();
		$(this).parent().find("ul").css("display", "block");
	});

	$("*:not(.myMemo .more)").click(function() {
		$(".myMemo ul").css("display", "none");
	});

	$(".myMemo ul li:nth-child(1)").off("click").click(function() {
		var stockNote = $(this).parent().parent().parent();

		if (stockNote.data("type") == "url") {
			var title = stockNote.find(".myMemo h4").text();
			var contents = stockNote.find(".myMemo p a").text();
			$(".popupUrl .popupTitle").text("링크 수정");
			$(".popupUrl button").text("수정");
			$(".popupUrl").css("display", "block");
			$(".popupUrl input[type=text]:eq(0)").val(title);
			$(".popupUrl input[type=text]:eq(0)").focus();
			$(".popupUrl input[type=text]:eq(1)").val(contents);
			searchTheme.addMemoType = "url";
		} else {
			var title = stockNote.find(".myMemo h4").text();
			var contents = stockNote.find(".myMemo p").text();
			$(".popupMemo").css("display", "block");
			$(".popupMemo .popupTitle").text("메모 수정");
			$(".popupMemo button").text("수정");
			$(".popupMemo input[type=text]:eq(0)").val(title);
			$(".popupMemo input[type=text]:eq(0)").focus();
			$(".popupMemo input[type=text]:eq(1)").val(contents);
			searchTheme.addMemoType = "memo";
		}
		$(".popupMask").css("display", "block");
		searchTheme.popupType = "modify";
		searchTheme._currentStockNoteId = stockNote.data("id");
		searchTheme._currentStockNote = stockNote;
	});

	$(".myMemo ul li:nth-child(2)").off("click").click(function() {
		var stockNote = $(this).parent().parent().parent();
		var stockNoteId = $(this).parent().parent().parent().data("id");

		$.ajax({
			url: DOMAIN + "/stock/stockNote",
			method: "DELETE",
			headers: $.extend(null, { "userId": userID, "stockNoteId": stockNoteId }),
			success: function() {
				stockNote.remove();
			}, // end success
			error: function(status) {
				alert("종목 노트를 삭제하는데 실패하였습니다.");
			}
		}); // end ajax	
	});
}

SearchTheme.prototype.addSubmitBtnClickEvent = function() {
	$(".popupMemo button, .popupUrl button").off("click").click(function() {
		var title = $(this).parent().find("input[type=text]").eq(0).val();
		var contents = $(this).parent().find("input[type=text]").eq(1).val();
		var type = searchTheme.addMemoType;

		if (type == "url") {
			let regex = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (!regex.test(contents)) {
				alert("링크 형식이 올바르지 않습니다.");
				return;
			}
		} else {
			if (title == "" && contents == "") {
				alert("제목과 내용중 하나를 입력해 주세요.");
				return;
			}
		}

		if (searchTheme.popupType == "write") {
			$.ajax({
				url: DOMAIN + "/stock/stockNote",
				method: "POST",
				headers: $.extend(null, { "userId": userID, "code": searchTheme._currentSelectStockCode }),
				data: "&title=" + encodeURIComponent(title) + "&contents=" + encodeURIComponent(contents) + "&type=" + type,
				success: function(noteId) {
					$(".myMemoWrap .memoNodata").parent().remove();

					if (type == "url") {
						$(".myMemoWrap").prepend(searchTheme._myUrlMemoWbox.replace("(title)", title)
							.replace("(contents)", contents)
							.replace("(contentsUrl)", contents)
							.replace("(contentsUrl)", contents)
							.replace("(stockNoteId)", noteId)
							.replace("(dateTime)", new Date().format("yyyy-MM-dd HH:mm:ss")));

						searchTheme.getOgDataAndMapping($(".myMemoWrap .gBox:first-child"), contents);

					} else {
						$(".myMemoWrap").prepend(searchTheme._myMemoWbox.replace("(title)", title)
							.replace("(contents)", contents)
							.replace("(stockNoteId)", noteId)
							.replace("(dateTime)", new Date().format("yyyy-MM-dd HH:mm:ss")));
					}
					searchTheme._privateStockNoteAmount += 1;


					$(".popupMask").click();

				}, // end success
				error: function(status) {
					alert("종목 노트를 작성하는데 실패하였습니다.");
				}
			}); // end ajax	
		} else {
			$.ajax({
				url: DOMAIN + "/stock/stockNote",
				method: "PUT",
				headers: $.extend(null, { "userId": userID, "stockNoteId": searchTheme._currentStockNoteId, "code": searchTheme._currentSelectStockCode }),
				data: "&title=" + encodeURIComponent(title) + "&contents=" + encodeURIComponent(contents),
				success: function() {
					searchTheme._currentStockNote.find("h4").text(title);
					searchTheme._currentStockNote.find(".contents").html("<a href=\"" + contents + "\" target=\"_blank\">" + contents + "</a>");

					if (type == "url") {
						searchTheme.getOgDataAndMapping(searchTheme._currentStockNote, contents);
					}

					$(".popupMask").click();
				}, // end success
				error: function(status) {
					alert("종목 노트를 수정하는데 실패하였습니다.");
				}
			}); // end ajax	
		}


	});
}

SearchTheme.prototype.addKeywordAddRequestAreaEvent = function() {
	$(".addKeywordWrap .inputKeyword input[type=text]").off("keydown").keydown(function(e) {

		if (e.keyCode == 13) {
			setTimeout(function() {
				var text = $(".addKeywordWrap .inputKeyword input[type=text]").val().trim();
				if (text == "") {
					alert("키워드를 입력해주세요.");
					return;
				} else if (checkConsonant(text)) {
					alert("키워드에 자음, 모음, \' 은 포함될 수 없습니다.");
					return;
				}

				var isExists = false;

				$(searchTheme._currentSelectStock).find(".keywordArg").each(function(index, item) {
					if ($(item).text().toUpperCase() == text.toUpperCase()) {
						isExists = true;
					}
				});

				$(".popupWrap .addedKeyword ul li").each(function(index, item) {
					if ($(item).text().toUpperCase() == text.toUpperCase()) {
						isExists = true;
					}
				});

				if (isExists) {
					$(".alertExist p").text("이미 존재하는 키워드입니다. 다른 키워드를 입력해 주세요.");
					$(".alertExist").removeClass("display_none");
					setTimeout(function() {
						$(".alertExist").addClass("display_none");

					}, 1500);
					return;
				}

				$(".addKeywordWrap .inputKeyword input[type=text]").val("");

				$(".addKeywordWrap .addedKeyword ul").append(searchTheme._addKeywordHtmlTag.replace("(keywordName)", text));

				$(".addKeywordWrap .addedKeyword li span").off("click").click(function() {
					$(this).parent().remove();
				});
			}, 0.5);
			e.stopPropagation();
		} e
	});

	$(".addKeywordWrap button").off("click").click(function() {
		var keywordArr = new Array();
		$(".popupWrap .addedKeyword ul li").each(function(index, item) {
			keywordArr.push($(item).text().trim());
		});

		if (keywordArr.length == 0) {
			$(".alertExist p").text("키워드를 1개 이상 추가해 주세요.");
			$(".alertExist").removeClass("display_none");
			setTimeout(function() {
				$(".alertExist").addClass("display_none");
			}, 1500);

			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/stockManage/keywordAdd",
			method: "POST",
			headers: $.extend(null, { "userId": userID, "code": searchTheme._currentSelectStockCode }),
			data: "&keywords=" + encodeURIComponent(JSON.stringify(keywordArr)),
			success: function() {
				alert("키워드 추가 요청이 정상적으로 처리 되었습니다..");
				$(".popupMask").click();
			}, // end success
			error: function(status) {
				alert("키워드 추가 요청을 실패하였습니다.");
			}
		}); // end ajax	
	});
}

function setCookie(name, value, exp) {
	var date = new Date();
	date.setTime(date.getTime() + exp * 5 * 60 * 1000);
	document.cookie = name + '=' + encodeURIComponent(value) + ';expires=' + date.toUTCString() + ';path=/';
}

function getCookie(name) {
	var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	return value ? decodeURIComponent(value[2]) : null;
};

function closeAllLists2() {
	var x = document.getElementsByClassName("autocomplete-items");
	for (var i = 0; i < x.length; i++) {
		x[i].parentNode.removeChild(x[i]);
	}
}
