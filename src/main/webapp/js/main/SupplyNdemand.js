/**
 * 
 */

function SupplyNdemand() {
	this._init();

}

SupplyNdemand.prototype._init = function() {
	this._oneTupleHtmlTag = "<div class=\"row\" data-code=\"(stockCode)\">" +
		"<div class=\"conTitle\">" +
		"	<h3>(stockName)</h3>" +
		"</div>" +
		"<div class=\"sub\">" +
		"		</div>" +
		"		<div class=\"conKeyword\">" +
		"</div>" +
		"</div>";

	this._oneSubHtml = "<p>(object) 순매수 역대 (rank)번째</p>";
	this._oneKeywordHtml = "<span data-kid=\"(kid)\">(keyword)</span>";
	this._adminWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\"> " +
		"	<div class=\"adminInfo\">" +
		"		<h4>(title)</h4>" +
		"		<p>(contents)</p> " +
		"		<span>(dateTime)</span>" +
		"	</div>" +
		"</div>";

	this._myMemoWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\" data-type=\"memo\"> " +
		"	<div class=\"myMemo\">" +
		"		<h4>(title)</h4>" +
		"	    <img class=\"more\" src=\"../images/ico_more_bk.png\" alt=\"메모 더보기 버튼\">" +
		"		<p>(contents)</p> " +
		"		<span>(dateTime)</span>" +
		"  		<ul>" +
		"      		 <li>수정</li>" +
		"       	 <li>삭제</li>" +
		"       </ul> " +
		"	</div>" +
		"</div>";

	this._myUrlMemoWbox = "<div class=\"gBox\" data-id=\"(stockNoteId)\" data-type=\"url\"> " +
		"	<div class=\"myMemo\">" +
		"		<h4>(title)</h4>" +
		"	    <img class=\"more\" src=\"../images/ico_more_bk.png\" alt=\"메모 더보기 버튼\">" +
		"		<p class=\"contents\"> <a href=\"(contentsUrl)\" target=\"_blank\">(contents)</a></p> " +
		" <div class=\"timg\">" +
		"    <p class=\"ogImageUrl\"></p>" +
		"     <a href=\"(contentsUrl)\" target=\"_blank\"><div class=\"tTxt\">" +
		"        <h5 class=\"ogTitle\"></h5>" +
		"     <span class=\"ogUrl\"></span>" +
		"      </div></a>" +
		"       </div>" +
		"		<span>(dateTime)</span>" +
		"  		<ul>" +
		"      	 	<li>수정</li>" +
		"        	<li>삭제</li>" +
		"       </ul> " +
		"	</div>" +
		"</div>";

	this._defaultWbox = "<div class=\"gBox\">" +
		"<div class=\"myMemo memoNodata\">" +
		"	<p>오른쪽의 [+] 버튼을 클릭해 해당 종목에 대한 메모/url을 둥록해 주세요.</p>" +
		"</div>";
	this._noneDataDayIndex = 0;
	var localDate = moment();
	this.getSubBusinessDay(localDate);
	this._fixedLocalDate = moment(localDate);
	var localDateStr = localDate.format("yyyy.MM.DD");

	$(".current_date").text(localDateStr);
	this.setLocalDateInfo(localDate);

	this.loadSupplyNdemandAmount();
	this.addMemoTabClickEvent();
	this.addDatePickerChangeEvent();
	this.addInputMemoTabClickEvent();
	this.addInputUrlMemoTabClickEvent();
	this.addBtnEventListener();
	this.addSubmitBtnClickEvent();
	this.addKeyboardEvent2();

}

SupplyNdemand.prototype.loadSupplyNdemandAmount = function() {
	$(".con_01").empty();
	var date = this._localDateInfo.format("yyyy-MM-DD");
	$(".current_date").text(date);

	$.ajax({
		url: DOMAIN + "/stock/stockManage/supplyNdemandAmount",
		method: "GET",
		data: "&date=" + date,
		dataType: "json",
		success: function(amountInfo) {
			if (supplyNdemand._fixedLocalDate.diff(supplyNdemand._localDateInfo.format("yyyy-MM-DD"), 'd') > 0) {
				$(".after").removeClass("display_none");
				supplyNdemand.addKeyboardEvent();
			} else {
				$(".after").addClass("display_none");
				supplyNdemand.addKeyboardEvent2();
			}

			if (amountInfo.amount != 0) {
				supplyNdemand._noneDataDayIndex = 0;
				supplyNdemand.setSupplyNdemandDataAmount(amountInfo.amount);
				supplyNdemand.loadSupplyNdemandList();

			} else {
				supplyNdemand._noneDataDayIndex += 1;
				if (supplyNdemand._noneDataDayIndex > 10) {
					supplyNdemand._noneDataDayIndex = 0;
					supplyNdemand.setSupplyNdemandDataAmount(0);
					supplyNdemand.showSupplyNdemandData();
					return;
				}

				if (supplyNdemand._currentMovingDirection == "add") {
					if (supplyNdemand._fixedLocalDate.diff(supplyNdemand._localDateInfo.format("yyyy-MM-DD"), 'd') == 0) {
						supplyNdemand._noneDataDayIndex = 0;
						supplyNdemand.setSupplyNdemandDataAmount(0);
						supplyNdemand.showSupplyNdemandData();
						return;
					}

					supplyNdemand._localDateInfo.add(1, "d");
					supplyNdemand.getAddBusinessDay(supplyNdemand._localDateInfo);
					supplyNdemand.loadSupplyNdemandAmount();
				} else {
					supplyNdemand._localDateInfo.subtract(1, "d");
					supplyNdemand.getSubBusinessDay(supplyNdemand._localDateInfo);
					supplyNdemand.loadSupplyNdemandAmount();
				}
			}

			$(".backG>.wBox:nth-child(1)").css("width", "100%");
			$(".backG .con_02").css("display", "none");
			$(".fab").css("display", "none");

		}, // end success
		error: function() {
			alert("수급 포착 데이터 수량을 불러오지 못했습니다");
		}
	}); // end ajax	
}

SupplyNdemand.prototype.loadSupplyNdemandList = function() {

	var date = this._localDateInfo.format("yyyy-MM-DD");
	$.ajax({
		url: DOMAIN + "/stock/stockManage/supplyNdemand",
		method: "GET",
		data: "&date=" + date + "&offset=" + $(".con_01 .row").length,
		dataType: "json",
		success: function(supplyNdemandDataList) {
			supplyNdemand.setSupplyNdemandData(supplyNdemandDataList);
			supplyNdemand.showSupplyNdemandData(supplyNdemandDataList);

		}, // end success
		error: function() {
			alert("수급 포착 목록을 불러오지 못했습니다");
		}
	}); // end ajax	

}

SupplyNdemand.prototype.showSupplyNdemandData = function(list) {
	if (list == undefined) {
		$(".con_01").append("<div>데이터가 존재하지 않습니다.</div>");
		return;
	}

	for (var i = 0; i < list.length; i++) {
		var dataObj = list[i];
		$(".con_01").append(this._oneTupleHtmlTag.replace("(stockName)", dataObj.name)
			.replace("(stockName)", addComma(dataObj.curPrice))
			.replace("(stockCode)", dataObj.code));

		if (dataObj.foreignerRank > 0) {
			$(".row .sub:last").append(this._oneSubHtml.replace("(object)", "외국인").replace("(rank)", dataObj.foreignerRank));
		}

		if (dataObj.orgRank > 0) {
			$(".row .sub:last").append(this._oneSubHtml.replace("(object)", "기관").replace("(rank)", dataObj.orgRank));
		}

		if (dataObj.tosinRank > 0) {
			$(".row .sub:last").append(this._oneSubHtml.replace("(object)", "투신").replace("(rank)", dataObj.tosinRank));
		}

		if (dataObj.samoRank > 0) {
			$(".row .sub:last").append(this._oneSubHtml.replace("(object)", "사모펀드").replace("(rank)", dataObj.samoRank));
		}

		if (dataObj.pensionRank > 0) {
			$(".row .sub:last").append(this._oneSubHtml.replace("(object)", "연기금").replace("(rank)", dataObj.pensionRank));
		}

		for (var j = 0; j < dataObj.keywordArr.length; j++) {
			var keywordObj = dataObj.keywordArr[j];
			$(".row .conKeyword:last").append(this._oneKeywordHtml.replace("(kid)", keywordObj.kid).replace("(keyword)", keywordObj.keyword));
		}
	}

	if ($(".con_01 .row").length < this._allAmount) {
		$(window).scroll(scrollEvent); // end scroll
	} // end if

	$(".backG").height($(".con_01").height());

	this.addStockTitleClickEvent();
}

function scrollEvent() {
	var $window = $(window);
	var scrollTop = $window.scrollTop();
	var documentHeight = $(document).height();
	var windowHeight = $(window).height();

	var middle = (documentHeight - windowHeight) / 2;

	if (scrollTop >= middle) {
		$(window).off("scroll", scrollEvent);
		if (supplyNdemand._supplyNdemandDataList.length == 50) {
			blockUI();
			supplyNdemand.loadSupplyNdemandList();
			unblockUI();
		}
	} // end if
}

SupplyNdemand.prototype.addDatePickerChangeEvent = function() {
	$(".datePicker .before, .after").off("click").click(function() {
		var localDate = supplyNdemand.getLocalDateInfo();
		if ($(this).hasClass("after")) {
			localDate.add(1, "d");
			supplyNdemand.getAddBusinessDay(localDate);
			supplyNdemand._currentMovingDirection = "add";
		} else {
			localDate.subtract(1, "d");
			supplyNdemand.getSubBusinessDay(localDate);
			supplyNdemand._currentMovingDirection = "sub";
		}


		supplyNdemand.loadSupplyNdemandAmount();
	});
}

SupplyNdemand.prototype.addKeyboardEvent = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		} else if (e.keyCode == 39) {
			$(".datePicker .after").click();
		}
	});
}

SupplyNdemand.prototype.addKeyboardEvent2 = function() {

	$(document).off("keydown").on("keydown", function(e) {
		if (e.keyCode == 37) {
			$(".datePicker .before").click();
		}
	});
}

SupplyNdemand.prototype.getSubBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.subtract(2, "d");
	} else if (localDate.day() == 6) {
		localDate.subtract(1, "d");
	}
}

SupplyNdemand.prototype.getAddBusinessDay = function(localDate) {
	if (localDate.day() == 0) {
		localDate.add(1, "d");
	} else if (localDate.day() == 6) {
		localDate.add(2, "d");
	}
}

SupplyNdemand.prototype.addStockTitleClickEvent = function() {
	$(".con_01 .conTitle").off("click").click(function() {
		if (userID == "" || userID == undefined) {
			alert("로그인 후에 이용이 가능합니다.");
			sessionStorage.setItem("prevLocation", location.href);
			location.href = DOMAIN + "/index.jsp";
			return;
		}


		supplyNdemand._currentStock = $(this).parent();
		$(".fab").css("display", "block");

		$(".backG>.wBox:nth-child(1)").css("width", "75%");
		$(".backG .con_02").css("width", "calc(100% - 75% - .5em)");
		$(".backG .con_02").css("display", "inline-block");

		var defaultHeight = $("#container .inner .search").outerHeight(true) + $("#header").height();

		$(window).scroll(function() {
			var position = $(window).scrollTop();
			if (defaultHeight < position) {
				$(".con_02").stop().animate({ "top": position - defaultHeight + "px" }, 300);
			} else {
				$(".con_02").stop().animate({ "top": $(".backG").css("padding-top") }, 300);
			}
		});


		var code = $(this).parent().data("code");
		supplyNdemand._currentSelectStockCode = code;
		$.ajax({
			url: DOMAIN + "/stock/stockNote/stockNoteAmount",
			method: "GET",
			headers: $.extend(null, { "userId": userID, "code": code }),
			dataType: "json",
			success: function(publicPrivateStockNoteObj) {
				supplyNdemand._publicStockNoteAmount = publicPrivateStockNoteObj.publicAmount;
				supplyNdemand._privateStockNoteAmount = publicPrivateStockNoteObj.privateAmount;

				$(".myMemoWrap").removeClass("display_none");
				$(".adminInfoWrap").removeClass("display_none");
				$(".adminInfoWrap").empty();
				$(".myMemoWrap").empty();

				$(".memoTab li").removeClass("on");

				if (supplyNdemand._publicStockNoteAmount == 0) {
					$(".memoTab li:nth-child(1)").css("display", "none");
					$(".memoTab li:nth-child(2)").addClass("on");
					$(".memoTab .adminInfoWrap").addClass("display_none");
				} else {
					$(".memoTab li:nth-child(1)").css("display", "inline-block");
					$(".memoTab li:nth-child(2)").addClass("on");
					$(".wBoxWrap .adminInfoWrap").addClass("display_none");
				}
				supplyNdemand.loadStockNoteList(code);
				$(window).scroll();
			}, // end success
			error: function() {
				alert("종목 노트 정보를 불러오지 못했습니다");
			}
		}); // end ajax	

	});

}

SupplyNdemand.prototype.addBtnEventListener = function() {
	$(".popupMask, .popupWrap .close").off("click").click(function() {
		$(".popupWrap").css("display", "none");
		$(".popupMask").css("display", "none");
		$(".popupWrap .inputKeyword input[type=text]").val("");
		$(".popupWrap .addedKeyword ul").empty();
	});

	$(document.body).keydown(function(e) {
		if (e.keyCode == 27) {
			$(".popupMask").click();
		}
	});
}

SupplyNdemand.prototype.addInputUrlMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(1)").off("click").click(function() {
		$(".popupMemo").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupMemo .popupTitle").text("메모 등록");
		$(".popupMemo button").text("등록");
		$(".popupMemo .inputKeyword input[type=text]:eq(0)").focus();
		supplyNdemand.addMemoType = "memo";
		supplyNdemand.popupType = "write";
	});

}


SupplyNdemand.prototype.addInputMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(2)").off("click").click(function() {
		$(".popupUrl").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupUrl .popupTitle").text("링크 등록");
		$(".popupUrl button").text("등록");
		$(".popupUrl .inputKeyword input[type=text]:eq(0)").focus();
		supplyNdemand.addMemoType = "url";
		supplyNdemand.popupType = "write";
	});
}

//TODO WORK scroll Event넣기
SupplyNdemand.prototype.loadStockNoteList = function(code) {
	$.ajax({
		url: DOMAIN + "/stock/stockNote/stockNoteList",
		method: "GET",
		headers: $.extend(null, { "userId": userID, "code": code }),
		dataType: "json",
		data: "&offset=" + $(".myMemoWrap .gBox").length,
		success: function(publicPrivateStockNoteObj) {
			supplyNdemand._privateStockNoteList = publicPrivateStockNoteObj.privateStockNoteList;

			if (supplyNdemand._publicStockNoteAmount != 0 && $(".myMemoWrap .gBox").length == 0) {
				supplyNdemand._publicStockNoteList = publicPrivateStockNoteObj.publicStockNoteList;
				supplyNdemand.showPublicStockNoteList(supplyNdemand._publicStockNoteList);
			}

			supplyNdemand.showPrivateStockNoteList(supplyNdemand._privateStockNoteList);
		}, // end success
		error: function() {
			alert("종목 노트 정보를 불러오지 못했습니다");
		}
	}); // end ajax	
}

SupplyNdemand.prototype.showPrivateStockNoteList = function(privateStockNoteList) {
	if (privateStockNoteList.length != 0) {
		for (var i = 0; i < privateStockNoteList.length; i++) {
			var privateStockNote = privateStockNoteList[i];
			if (privateStockNote.isURL) {
				$(".myMemoWrap").append(supplyNdemand._myUrlMemoWbox.replace("(title)", privateStockNote.title)
					.replace("(contents)", privateStockNote.contents)
					.replace("(contentsUrl)", privateStockNote.contents)
					.replace("(contentsUrl)", privateStockNote.contents)
					.replace("(stockNoteId)", privateStockNote.id)
					.replace("(dateTime)", privateStockNote.dateTime));

				supplyNdemand.getOgDataAndMapping($(".myMemoWrap .gBox:last-child"), privateStockNote.contents);
			} else {
				$(".myMemoWrap").append(supplyNdemand._myMemoWbox.replace("(title)", privateStockNote.title)
					.replace("(contents)", privateStockNote.contents)
					.replace("(stockNoteId)", privateStockNote.id)
					.replace("(dateTime)", privateStockNote.dateTime));
			}
		}
	} else {
		$(".myMemoWrap").append(supplyNdemand._defaultWbox);
	}


	this.addStockNoteMoreIconClickEvent();

	if ($(".myMemoWrap .gBox").length < supplyNdemand._privateStockNoteAmount) {
		$(".con_02").scroll(scrollEventForMemo); // end scroll
	} // end if
}

function scrollEventForMemo() {
	var $window = $(".con_02");
	var scrollTop = $window.scrollTop();
	var documentHeight = $(".myMemoWrap").height();
	var windowHeight = $(window).height();

	var middle = (documentHeight - windowHeight) / 2;

	if (scrollTop >= middle) {
		$(".con_02").off("scroll", scrollEventForMemo);
		if (supplyNdemand._privateStockNoteList.length == 10) {
			supplyNdemand.loadStockNoteList(supplyNdemand._currentSelectStockCode);
		}
	} // end if
}

SupplyNdemand.prototype.showPublicStockNoteList = function(publicStockNoteList) {

	for (var i = 0; i < publicStockNoteList.length; i++) {
		var publicStockNote = publicStockNoteList[i];
		$(".adminInfoWrap").append(supplyNdemand._adminWbox.replace("(title)", publicStockNote.title)
			.replace("(contents)", publicStockNote.contents)
			.replace("(stockNoteId)", publicStockNote.id)
			.replace("(dateTime)", publicStockNote.dateTime));
	}
}

SupplyNdemand.prototype.addMemoTabClickEvent = function() {
	$(".memoTab li").off("click").click(function() {
		if ($(this).hasClass("infoTab")) {
			$(".myMemoWrap").addClass("display_none");
			$(".adminInfoWrap").removeClass("display_none");
			$(".memoTab li").removeClass("on");
			$(".memoTab .infoTab").addClass("on");
		} else {
			$(".adminInfoWrap").addClass("display_none");
			$(".myMemoWrap").removeClass("display_none");
			$(".memoTab li").removeClass("on");
			$(".memoTab .myMemoTab").addClass("on");
		}
	});
}


SupplyNdemand.prototype.addInputUrlMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(1)").off("click").click(function() {
		$(".popupMemo").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupMemo .popupTitle").text("메모 등록");
		$(".popupMemo button").text("등록");
		$(".popupMemo .inputKeyword input[type=text]:eq(0)").focus();
		supplyNdemand.addMemoType = "memo";
		supplyNdemand.popupType = "write";
	});

}


SupplyNdemand.prototype.addInputMemoTabClickEvent = function() {
	$(".fab .fab-buttons li:nth-child(2)").off("click").click(function() {
		$(".popupUrl").css("display", "block");
		$(".popupMask").css("display", "block");
		$(".popupUrl .popupTitle").text("링크 등록");
		$(".popupUrl button").text("등록");
		$(".popupUrl .inputKeyword input[type=text]:eq(0)").focus();
		supplyNdemand.addMemoType = "url";
		supplyNdemand.popupType = "write";
	});
}

SupplyNdemand.prototype.addStockNoteMoreIconClickEvent = function() {
	$(".myMemo .more").off("click").click(function(e) {
		e.stopPropagation();
		$(this).parent().find("ul").css("display", "block");
	});

	$("*:not(.myMemo .more)").click(function() {
		$(".myMemo ul").css("display", "none");
	});

	$(".myMemo ul li:nth-child(1)").off("click").click(function() {
		var stockNote = $(this).parent().parent().parent();

		if (stockNote.data("type") == "url") {
			var title = stockNote.find(".myMemo h4").text();
			var contents = stockNote.find(".myMemo p a").text();
			$(".popupUrl .popupTitle").text("링크 수정");
			$(".popupUrl button").text("수정");
			$(".popupUrl").css("display", "block");
			$(".popupUrl input[type=text]:eq(0)").val(title);
			$(".popupUrl input[type=text]:eq(0)").focus();
			$(".popupUrl input[type=text]:eq(1)").val(contents);
			supplyNdemand.addMemoType = "url";
		} else {
			var title = stockNote.find(".myMemo h4").text();
			var contents = stockNote.find(".myMemo p").text();
			$(".popupMemo").css("display", "block");
			$(".popupMemo .popupTitle").text("메모 수정");
			$(".popupMemo button").text("수정");
			$(".popupMemo input[type=text]:eq(0)").val(title);
			$(".popupMemo input[type=text]:eq(0)").focus();
			$(".popupMemo input[type=text]:eq(1)").val(contents);
			supplyNdemand.addMemoType = "memo";
		}
		$(".popupMask").css("display", "block");
		supplyNdemand.popupType = "modify";
		supplyNdemand._currentStockNoteId = stockNote.data("id");
		supplyNdemand._currentStockNote = stockNote;
	});

	$(".myMemo ul li:nth-child(2)").off("click").click(function() {
		var stockNote = $(this).parent().parent().parent();
		var stockNoteId = $(this).parent().parent().parent().data("id");

		$.ajax({
			url: DOMAIN + "/stock/stockNote",
			method: "DELETE",
			headers: $.extend(null, { "userId": userID, "stockNoteId": stockNoteId }),
			success: function() {
				stockNote.remove();
			}, // end success
			error: function(status) {
				alert("종목 노트를 삭제하는데 실패하였습니다.");
			}
		}); // end ajax	
	});
}

SupplyNdemand.prototype.addSubmitBtnClickEvent = function() {
	$(".popupMemo button, .popupUrl button").off("click").click(function() {
		var title = $(this).parent().find("input[type=text]").eq(0).val();
		var contents = $(this).parent().find("input[type=text]").eq(1).val();
		var type = supplyNdemand.addMemoType;

		if (type == "url") {
			let regex = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (!regex.test(contents)) {
				alert("링크 형식이 올바르지 않습니다.");
				return;
			}
		} else {
			if (title == "" && contents == "") {
				alert("제목과 내용중 하나를 입력해 주세요.");
				return;
			}
		}

		if (supplyNdemand.popupType == "write") {
			$.ajax({
				url: DOMAIN + "/stock/stockNote",
				method: "POST",
				headers: $.extend(null, { "userId": userID, "code": supplyNdemand._currentSelectStockCode }),
				data: "&title=" + encodeURIComponent(title) + "&contents=" + encodeURIComponent(contents) + "&type=" + type,
				success: function(noteId) {
					$(".myMemoWrap .memoNodata").parent().remove();

					if (type == "url") {
						$(".myMemoWrap").prepend(supplyNdemand._myUrlMemoWbox.replace("(title)", title)
							.replace("(contents)", contents)
							.replace("(contentsUrl)", contents)
							.replace("(contentsUrl)", contents)
							.replace("(stockNoteId)", noteId)
							.replace("(dateTime)", new Date().format("yyyy-MM-dd HH:mm:ss")));

						supplyNdemand.getOgDataAndMapping($(".myMemoWrap .gBox:first-child"), contents);

					} else {
						$(".myMemoWrap").prepend(supplyNdemand._myMemoWbox.replace("(title)", title)
							.replace("(contents)", contents)
							.replace("(stockNoteId)", noteId)
							.replace("(dateTime)", new Date().format("yyyy-MM-dd HH:mm:ss")));
					}
					supplyNdemand._privateStockNoteAmount += 1;


					$(".popupMask").click();

				}, // end success
				error: function(status) {
					alert("종목 노트를 작성하는데 실패하였습니다.");
				}
			}); // end ajax	
		} else {
			$.ajax({
				url: DOMAIN + "/stock/stockNote",
				method: "PUT",
				headers: $.extend(null, { "userId": userID, "stockNoteId": supplyNdemand._currentStockNoteId, "code": supplyNdemand._currentSelectStockCode }),
				data: "&title=" + encodeURIComponent(title) + "&contents=" + encodeURIComponent(contents),
				success: function() {
					supplyNdemand._currentStockNote.find("h4").text(title);
					supplyNdemand._currentStockNote.find(".contents").html("<a href=\"" + contents + "\" target=\"_blank\">" + contents + "</a>");

					if (type == "url") {
						supplyNdemand.getOgDataAndMapping(supplyNdemand._currentStockNote, contents);
					}

					$(".popupMask").click();
				}, // end success
				error: function(status) {
					alert("종목 노트를 수정하는데 실패하였습니다.");
				}
			}); // end ajax	
		}

	});
}


SupplyNdemand.prototype.getOgDataAndMapping = function(stockNote, url) {
	$.ajax({
		url: DOMAIN + "/stock/stockNote/ogDataMapping",
		method: "GET",
		headers: $.extend(null, { "url": url }),
		dataType: "json",
		success: function(ogDataObj) {
			$(stockNote).find(".ogUrl").text(ogDataObj.ogUrl);
			$(stockNote).find(".ogTitle").text(ogDataObj.ogTitle);
			$(stockNote).find(".ogImageUrl").css("background-image", "url(" + ogDataObj.ogImageUrl + ")");
		}, // end success
		error: function() {
			alert("종목 노트 정보를 불러오지 못했습니다");
		}
	}); // end ajax
	unblockUI();	
}

SupplyNdemand.prototype.setSupplyNdemandDataAmount = function(amount) {
	this._allAmount = amount;
}

SupplyNdemand.prototype.setSupplyNdemandData = function(list) {
	this._supplyNdemandDataList = list;
}

SupplyNdemand.prototype.setLocalDateInfo = function(localDate) {
	this._localDateInfo = localDate;
}

SupplyNdemand.prototype.getLocalDateInfo = function() {
	return this._localDateInfo;
}				