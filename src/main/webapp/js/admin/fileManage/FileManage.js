/**
 * 
 */
function FileManage() {
	this._init();
}

FileManage.prototype._init = function() {
	$("input[type=file]").change(function() {
		readURL(this);
	});

	this.addClickSubFileButton();
}

FileManage.prototype.addClickSubFileButton = function() {
	$(".subFile").click(function() {
		var parent = $(this).parent();
		var type = parent.data("type");
		var date = parent.find("input[type=date]").val();
		var $tsvFile = $(parent).find("input[type=file]");
		if ($tsvFile.val() == "") {
			alert("파일을 첨부해 주세요.");
			return false;
		}
		var file = $tsvFile.prop("files");

		file = file.length == 1 ? file[0] : undefined;
		if (file != undefined) {
			blob = file.slice(0, file.size, file.type);
			uploadImage = new File([blob], file.name, { "type": file.type });
		}

		formData = new FormData();
		formData.append("file", uploadImage);
		
		formData.append("date", date == undefined ? "": date);

		$.ajax({
			url: DOMAIN + "/stock/stockManage/" + type + "/insert",
			enctype: "multipart/form-data",
			method: "POST",
			data: formData,
			processData: false,
			contentType: false,
			success: function() {
				parent.find(".alert").slideUp().removeClass().addClass("alert alert-success").slideDown().html("파일에 첨부된 데이터를 정상적으로 DB에 등록하였습니다.");
			},
			error: function(e) {
				if(e.status == 406){
					parent.find(".alert").slideUp().removeClass().addClass("alert alert-danger").slideDown().html("파일에 포함된 값과 삽입하려 하는 테이블이 맞지 않습니다. 파일을 다시한번 확인해주세요. 같은 메세지가 계속해서 보일경우 웹 개발팀에 문의해주세요.");
				}else if(e.status == 403){
					parent.find(".alert").slideUp().removeClass().addClass("alert alert-danger").slideDown().html("매매(수량) 데이터 개수와 첨부한 파일의 개수가 다릅니다. 파일을 다시 한번 확인해주세요. 같은 메세지가 계속해서 보일경우 웹 개발팀에 문의해주세요.");
				}else if(e.status == 400){
					parent.find(".alert").slideUp().removeClass().addClass("alert alert-danger").slideDown().html("요청 값이 잘못 되었습니다. 첨부한 파일이 깨진 파일이 아닌지 다시 한번 확인해주세요. 같은 메세지가 계속해서 보일경우 웹 개발팀에 문의해주세요.");
				}else{
					parent.find(".alert").slideUp().removeClass().addClass("alert alert-danger").slideDown().html("서버에 문제가 발생했습니다. 같은 메세지가 계속해서 보일경우 웹 개발팀에 문의해주세요.");
				}
			}
		}); // end ajax
	});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			var $input = $(input);

			var extension = input.files[0].type.split("/")[1];
			//if (extension != "tsv" && extension != "tab-separated-values") {
			//	$input.val("");
			//	alert("tsv 파일만 첨부해주세요.");
			//	return;
			//}

			$input.next().css("display", "block");
			$input.next().off("click").on("click", function() {
				$(this).prev().val("");

				$(this).css("display", "none");

			}); // end on
		} // end function

		reader.readAsDataURL(input.files[0]);
	} else {
		$(input).next().css("display", "none");
	}
}