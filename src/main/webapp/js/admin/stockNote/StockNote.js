/**
 * 
 */

function StockNote() {
	this._init();
}

StockNote.prototype._init = function() {
	this._htmlTag = "<option class=\"stockArg\" value=\"(stockCode)\" data-keyword=\'(keywordList)\'>(StockName)</option>";
	this._stockNoteHtmlTag = "<option class=\"stockNoteArg\" value=\"(contents)\" data-id=\"(id)\">(title)</option>";
	this.loadStockList();
	this.addInsertBtnClickEvent();
	this.addModifyBtnClickEvent();
	this.addDeleteBtnClickEvent();

}

StockNote.prototype.loadStockList = function() {
	$("#stockList").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/allStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				stockNote.setStockList(list);
				stockNote.showStockList(list);
				stockNote.addStockSearchEvent();
			} // end if
			else {
				alert("주식 품목이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax
}

StockNote.prototype.showStockList = function(list) {

	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$("#stockList").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(StockName)", stockObj.name)
			.replace("(keywordList)", JSON.stringify(stockObj.keywordArr)));
	}

	$("#stockList").off("change").on("change", function(e) {
		$("#stockNoteList").empty();
		stockNote._currentStockNoteId = undefined;
		var code = $(e.target.options[e.target.selectedIndex]).val();
		stockNote._currentStockCode = code;
		$("#title").val("");
		$("#contents").val("");

		$.ajax({
			url: DOMAIN + "/stock/manage/stockNote",
			method: "GET",
			dataType: "json",
			headers: $.extend(null, { "userId": userID, "code": code }),
			success: function(list, status) {
				var publicStockNoteList = list.publicStockNoteList;
				stockNote.showStockNoteList(publicStockNoteList);
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
					alert("종목 노트를 불러오지 못했습니다");
				}
			}
		}); // end ajax
	});

}

StockNote.prototype.showStockNoteList = function(list) {
	function compare(a, b) {
		return a.title > b.title ? 1 : a.title < b.title ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockNoteObj = list[i];
		$("#stockNoteList").append(this._stockNoteHtmlTag.replace("(id)", stockNoteObj.id)
			.replace("(title)", stockNoteObj.title)
			.replace("(contents)", stockNoteObj.contents));
	}

	$("#stockNoteList").off("keydown change").on("keydown change", function(e) {
		stockNote._currentStockNoteId = $(e.target.options[e.target.selectedIndex]).data("id");
		var title = $(e.target.options[e.target.selectedIndex]).text();
		var contents = $(e.target.options[e.target.selectedIndex]).val();
		$("#title").val(title);
		$("#contents").val(contents);
	});
}


StockNote.prototype.addStockSearchEvent = function(list) {
	$("#stockName").off("keyup").keyup(function() {
		var searchTerm = $("#stockName").val();
		var list = stockNote._stockList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = stockNote.filterList(list);
		stockNote.showStockList(filterList);
	});
}

StockNote.prototype.filterList = function(list) {
	var searchTerm = $("#stockName").val();
	var list = stockNote._stockList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#stockList").empty();
	return filterList;
}

StockNote.prototype.addInsertBtnClickEvent = function() {
	$("#insertBtn").off("click").click(function() {
		if (stockNote._currentStockCode == undefined) {
			alert("종목을 선택해주세요.");
			return;
		}

		var title = $("#title").val();
		var contents = $("#contents").val();
		if (title == "") {
			alert("제목을 입력해주세요.");
			return;
		} else if (contents == "") {
			alert("내용을 입력해주세요.");
			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/manage/stockNote",
			method: "POST",
			dataType: "json",
			headers: $.extend(null, { "userId": userID, "code": stockNote._currentStockCode }),
			data: "&title=" + title + "&contents=" + contents,
			success: function() {
				if ($("#stockList option").eq(0).val() != undefined) $("#stockList").val(stockNote._currentStockCode).change();
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
						alert("종목 노트를 추가하지 못했습니다");
				}
			}
		}); // end ajax
	});
}

StockNote.prototype.addModifyBtnClickEvent = function() {
	$("#modifyBtn").off("click").click(function() {
		if (stockNote._currentStockCode == undefined) {
			alert("수정할 노트가 있는 종목을 선택해주세요.");
			return;
		}

		if (stockNote._currentStockNoteId == undefined) {
			alert("수정할 노트를 선택해주세요.");
			return;
		}

		var title = $("#title").val();
		var contents = $("#contents").val();
		if (title == "") {
			alert("제목을 입력해주세요.");
			return;
		} else if (contents == "") {
			alert("내용을 입력해주세요.");
			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/manage/stockNote",
			method: "PUT",
			headers: $.extend(null, { "userId": userID, "stockNoteId": stockNote._currentStockNoteId }),
			data: "&title=" + title + "&contents=" + contents,
			success: function() {
				if ($("#stockList option").eq(0).val() != undefined) $("#stockList").val(stockNote._currentStockCode).change();
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
						alert("종목 노트를 수정하지 못했습니다");
				}
			}
		}); // end ajax
	});
}

StockNote.prototype.addDeleteBtnClickEvent = function() {
	$("#deleteBtn").off("click").click(function() {
		if (stockNote._currentStockNoteId == undefined) {
			alert("삭제할 노트를 선택해주세요.");
			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/manage/stockNote",
			method: "DELETE",
			headers: $.extend(null, { "userId": userID, "stockNoteId": stockNote._currentStockNoteId }),
			success: function() {
				if ($("#stockList option").eq(0).val() != undefined) $("#stockList").val(stockNote._currentStockCode).change();
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
						alert("종목 노트를 삭제하지 못했습니다");
				}
			}
		}); // end ajax
	});
}

StockNote.prototype.setStockList = function(list) {
	this._stockList = list;
}

