/**
 * 
 */
function Account() {
	this._init();
}


Account.prototype._init = function() {
	$(document.body).keydown(function(e) {
		if (e.keyCode == 13) {
			$(".loginBtn").click();
		}
	});

	this.loginBtnClickEvent();
	this.signupBtnClickEvent();
}

Account.prototype.loginBtnClickEvent = function() {
	$(".loginBtn").off("click").click(function() {
		var email = $("#id").val();
		var password = $("#password").val();

		if (email.trim() == "") {
			alert("이메일을 입력하지 않았습니다.");
			return;
		} else if (password.trim() == "") {
			alert("패스워드를 입력하지 않았습니다.");
			return;
		}

		$.ajax({
			url: DOMAIN + "/stock/main",
			method: "POST",
			data: "email=" + email + "&password=" + password,
			success: function() {
					location.replace(DOMAIN + "/manage/main.jsp");	
			},
			error: function() {
				alert("아이디 또는 비밀번호가 올바르지 않습니다.");
			}
		}); // end ajax
	});
}

Account.prototype.signupBtnClickEvent = function() {
	$(".signup").off("click").click(function() {
			location.href = DOMAIN + "/main/join.jsp";
	});
}

