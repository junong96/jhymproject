/**
 * 
 */

function InsertStock() {
	this._init();
}

InsertStock.prototype._init = function() {
	this._htmlTag = "<option class=\"stockArg\" value=\"(stockCode)\" data-stock=\'(stockObj)\'>(StockName)</option>";
	this._noneExistsHtmlTag = "<option class=\"noneExistsStockArg\" value=\"(name)\">(StockName)</option>";
	this.loadStockList();
	this.loadNoneExistsStockList();
	this.addInsertBtnClickEvent();
	this.addDeleteBtnClickEvent();
}

InsertStock.prototype.loadStockList = function() {
	$("#stockList").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/onlyStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				insertStock.setStockList(list);
				insertStock.showStockList(list);
				var filterList = insertStock.filterList(list);
				insertStock.showStockList(filterList);
				insertStock.addStockSearchEvent(list);
			} // end if
			else {
				alert("주식 품목이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax
}

InsertStock.prototype.loadNoneExistsStockList = function() {
	$("#noneExistStockList").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/noneExistsStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				insertStock.showNoneExistsStockList(list);
			} // end if
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax
}

InsertStock.prototype.showStockList = function(list) {

	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$("#stockList").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(StockName)", stockObj.name)
			.replace("(stockObj)", JSON.stringify(stockObj).replace(/\'/gim, "\"")));
	}


	$("#stockList").off("keydown change").on("keydown change", function(e) {
		var stockObj = $(e.target.options[e.target.selectedIndex]).data("stock");
		$("#name").val(stockObj.name);
		$("#code").val(stockObj.code);
		$("#sectors").val(stockObj.sectors);
		$("#product").val(stockObj.product);
		$("#type").val(stockObj.type).change();
	});
}

InsertStock.prototype.showNoneExistsStockList = function(list) {

	function compare(a, b) {
		return a > b ? 1 : a < b ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var name = list[i];
		$("#noneExistStockList").append(this._noneExistsHtmlTag.replace("(name)", name)
			.replace("(StockName)", name));
	}


	$("#noneExistStockList").off("keydown change").on("keydown change", function(e) {
		var stockName = $(e.target.options[e.target.selectedIndex]).val();
		$("#name").val(stockName);
		$("#code").val("");
		$("#sectors").val("");
		$("#product").val("");
	});
}

InsertStock.prototype.addStockSearchEvent = function(list) {
	$("#stockName").off("keyup").keyup(function() {
		var searchTerm = $("#stockName").val();
		var list = insertStock._stockList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = insertStock.filterList(list);
		insertStock.showStockList(filterList);
	});
}

InsertStock.prototype.filterList = function(list) {
	var searchTerm = $("#stockName").val();
	var list = insertStock._stockList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#stockList").empty();
	return filterList;
}

InsertStock.prototype.addInsertBtnClickEvent = function() {
	$("#insertBtn").off("click").click(function() {

		var name = encodeURIComponent($("#name").val().trim());
		var code = $("#code").val().trim();
		var sectors = $("#sectors").val().trim();
		var product = $("#product").val().trim();
		var type = $("#type").val().trim();

		if (name == "") {
			alert("종목명을 입력해주세요");
			return;
		} else if (code == "" || code.length > 6) {
			if (code == "") {
				alert("종목 코드를 입력해주세요.");
			} else {
				alert("종목 코드는 6자를 넘을 수 없습니다.");
			}
			return;
		} else if (type == "") {
			alert("타입을 선택해주세요.");
			return;
		}

		var dataStr = "&name=" + name + "&code=" + code + "&sectors=" + sectors + "&product=" + product + "&type=" + type;

		$.ajax({
			url: DOMAIN + "/stock/manage/insertStock",
			method: "POST",
			data: dataStr,
			success: function(test, status, xhr) {
				$("#name").val("");
				$("#code").val("");
				$("#sectors").val("");
				$("#product").val("");
				insertStock.loadStockList();
				insertStock.loadNoneExistsStockList();
			}, // end success
			error: function() {
				alert("종목 등록에 실패하였습니다.");
			}//end error
		}); // end ajax
	});
}

InsertStock.prototype.addDeleteBtnClickEvent = function() {
	$("#deleteBtn").off("click").click(function() {
		var deleteCodeArr = $("#stockList").val();
		var confirmDelteCode = $("#code").val();

		if (deleteCodeArr == null || deleteCodeArr == undefined) {
			alert("삭제할 종목을 먼저 선택한 후 삭제버튼을 클릭해주세요.");
			return;
		} else if (deleteCodeArr.length > 1) {
			alert("삭제할 종목은 하나만 선택해주세요.");
			return;
		}

		var deleteCode = deleteCodeArr[0];
		if (deleteCode != confirmDelteCode) {
			alert("좌측 종목 리스트에서 삭제 할 종목을 다시 선택한 후 시도해주세요.");
			return;
		}

		var dataStr = "&code=" + deleteCode;

		$.ajax({
			url: DOMAIN + "/stock/manage/deleteStock",
			method: "DELETE",
			data: dataStr,
			success: function() {
				$("#name").val("");
				$("#code").val("");
				$("#sectors").val("");
				$("#product").val("");
				insertStock.loadStockList();
				insertStock.loadNoneExistsStockList();
			}, // end success
			error: function() {
				alert("종목 삭제에 실패하였습니다.");
			}//end error
		}); // end ajax

	});
}

InsertStock.prototype.setStockList = function(list) {
	this._stockList = list;
}

