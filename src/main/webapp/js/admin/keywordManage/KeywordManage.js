/**
 * 
 */

function KeywordManage() {
	this._init();
}

KeywordManage.prototype._init = function() {
	this._htmlTag = "<option class=\"stockArg\" value=\"(stockCode)\" data-keyword=\'(keywordList)\'>(StockName)</option>";
	this._keywordDetailHtmlTag = "<option>(StockName) / (keywordsList)</option>";
	this._keywordHtmlTag = "<option class=\"keywordArg\" value=\"(KID)\">(keyword)</option>";
	this._currentStockList = new Array();
	this.loadKeywordList();
	this.loadStockList();
	this.addInsertBtnClickEvent();
	this.addClearBtnClickEvent();
	this.addDeleteBtnClickEvent();

	$("#insertKeyword").on("keydown", function(e) {
		if (e.keyCode == 13) {
			setTimeout(function() {
				if ($("#autocomplete-list div").length == 0) {
					setTimeout(function() {
						if ($("#insertKeyword").val().trim() == "") {
							alert("키워드를 입력해주세요.");
							return;
						}
						if ($("#selectStockList option").size() == 0) {
							alert("현재 선택된 종목이 없습니다.");
							return;
						}
						var keywordName = $("#insertKeyword").val().trim();

						if (checkConsonant(keywordName)) {
							alert("키워드에 자음, 모음, \' 은 포함될 수 없습니다.");
							return;
						}

						keywordName.replace(/\'/gim, "");
						keywordName.replace(/(\s)/gim, "");

						$("#insertKeyword").val("");
						$("#keywordList").append(keywordManage._keywordHtmlTag.replace("(keyword)", keywordName)
							.replace("(KID)", -100)
							.replace("(addedKeyword)", "addedKeyword"));

						keywordManage.addAddedKeywordDblClickEvent();

					}, 0.01);
				}
			}, 0.5);
		} else if (e.key == " ") {
			setTimeout(function() {
				$("#insertKeyword").val($("#insertKeyword").val().replace(' ', ''));
			}, 0.01);
		}
	});
}

KeywordManage.prototype.loadStockList = function() {
	$("#stockList").empty();
	$("#selectStockList").empty();
	$("#selectStockKeywordView").empty();
	$("#keywords").empty();
	$("#keywords").append(this._keywordHtmlTag.replace("(keyword)", "종목 선택시 키워드가 나옵니다.")
		.replace("(KID)", -1));



	$.ajax({
		url: DOMAIN + "/stock/manage/allStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				keywordManage.setStockList(list);
				keywordManage.showStockList(list);
				keywordManage.loadCurrentSelectStock();
				var filterList = keywordManage.filterList(list);
				keywordManage.showStockList(filterList);
				keywordManage.addStockSearchEvent(list);
			} // end if
			else {
				alert("주식 품목이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax
}

KeywordManage.prototype.loadKeywordList = function() {
	$.ajax({
		url: DOMAIN + "/stock/manage/allKeywordList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				autocomplete.setAutocomplete(document.getElementById("insertKeyword"), list);

			} // end if
			else {
				alert("키워드 목록이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alertt("키워드 목록을 불러오지 못했습니다");
		}
	}); // end ajax
}

KeywordManage.prototype.showStockList = function(list) {

	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$("#stockList").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(StockName)", stockObj.name)
			.replace("(keywordList)", JSON.stringify(stockObj.keywordArr)));
	}


	$("#stockList").off("keydown change").on("keydown change", function(e) {
		$("#keywords").empty();
		var keywordsStr = $(e.target.options[e.target.selectedIndex]).data("keyword");

		if (keywordsStr == "" || keywordsStr == "[]") {
			$("#keywords").append(keywordManage._keywordHtmlTag.replace("(keyword)", "등록된 키워드가 없습니다.")
				.replace("(KID)", -1));
			return;
		}

		var keywordArr = keywordsStr;

		for (var i = 0; i < keywordArr.length; i++) {
			var keywordObj = keywordArr[i];
			$("#keywords").append(keywordManage._keywordHtmlTag.replace("(keyword)", keywordObj.keyword)
				.replace("(KID)", keywordObj.KID));
		}
	});

	$("#selectStockList").off("keydown change").on("keydown change", function(e) {
		$("#keywords").empty();
		var keywordsStr = $(e.target.options[e.target.selectedIndex]).data("keyword");

		if (keywordsStr == "" || keywordsStr == "[]") {
			$("#keywords").append(keywordManage._keywordHtmlTag.replace("(keyword)", "등록된 키워드가 없습니다.")
				.replace("(KID)", -1));
			return;
		}

		var keywordArr = keywordsStr;

		for (var i = 0; i < keywordArr.length; i++) {
			var keywordObj = keywordArr[i];
			$("#keywords").append(keywordManage._keywordHtmlTag.replace("(keyword)", keywordObj.keyword)
				.replace("(KID)", keywordObj.KID));
		}
	});

	$("#stockList").off("dblclick").on("dblclick", function(e) {
		if (e.target.type == "select-multiple") return;
		var stockCode = $(e.target).val();
		var keyword = $(e.target).data("keyword");
		var stockName = $(e.target).text();

		var selectStockCodeArr = $("#selectStockList").get(0).options;
		var isExists = false;
		for (var i = 0; i < selectStockCodeArr.length; i++) {
			if (selectStockCodeArr[i].value == stockCode) {
				isExists = true;
				break;
			}
		}

		if (isExists) return;

		if (!keywordManage._currentStockList.includes(stockCode)) {
			keywordManage._currentStockList.push(stockCode);
		}

		$("#selectStockList").append(keywordManage._htmlTag.replace("(stockCode)", stockCode)
			.replace("(StockName)", stockName)
			.replace("(keywordList)", JSON.stringify(keyword)));

		keywordManage.refreshKeywordDetail();
	});

	$("#selectStockList").off("dblclick").on("dblclick", function(e) {
		if (e.target.type == "select-multiple") return;
		keywordManage._currentStockList.splice(keywordManage._currentStockList.indexOf(e.target.value), 1);
		$(e.target).remove();
		keywordManage.refreshKeywordDetail();
	});
}


KeywordManage.prototype.addStockSearchEvent = function(list) {
	$("#stockName").off("keyup").keyup(function() {
		var searchTerm = $("#stockName").val();
		var list = keywordManage._stockList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = keywordManage.filterList(list);
		keywordManage.showStockList(filterList);
	});
}

KeywordManage.prototype.filterList = function(list) {
	var searchTerm = $("#stockName").val();
	var list = keywordManage._stockList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#stockList").empty();
	return filterList;
}

KeywordManage.prototype.addInsertBtnClickEvent = function() {
	$("#insertBtn").off("click").click(function() {
		var optArr = $("#selectStockList").get(0).options;
		var chooseStockArr = new Array();
		for (var i = 0; i < optArr.length; i++) {
			chooseStockArr.push(optArr[i].value);
		}

		var addKeywordListOptArr = $("#keywordList").get(0).options;

		var keywordListWords = new Array();

		for (var i = 0; i < addKeywordListOptArr.length; i++) {
			var keywordName = addKeywordListOptArr[i].text.trim();
			keywordName.replace(/\'/gim, "");
			keywordName.replace(/(\s)/gim, "");

			keywordListWords.push(keywordName);
		}

		if (chooseStockArr == undefined || chooseStockArr.length == 0) {
			alert("품목을 최소 1개 이상 선택해주세요.");
			return;
		}

		if (keywordListWords == undefined || keywordListWords.length == 0) {
			alert("키워드를 최소 1개 이상 추가 해주세요.");
			return;
		}

		var dataStr = "&stockList=" + encodeURIComponent(JSON.stringify(chooseStockArr)) + "&keywordList=" + encodeURIComponent(JSON.stringify(keywordListWords));

		$.ajax({
			url: DOMAIN + "/stock/manage/insertKeywordAndMapping",
			method: "POST",
			data: dataStr,
			success: function() {
				$("#keywordList").empty();
				keywordManage.loadStockList();
			}, // end success
			error: function() {
				alert("키워드 등록에 실패하였습니다.");
			}//end error
		}); // end ajax
	});
}

KeywordManage.prototype.addClearBtnClickEvent = function() {
	$("#clearBtn").off("click").click(function() {
		$("#selectStockList").empty();
		keywordManage._currentStockList = [];
	});
}

KeywordManage.prototype.addDeleteBtnClickEvent = function() {
	$("#deleteBtn").off("click").click(function() {
		var optArr = $("#selectStockList").get(0).options;
		var chooseStockArr = new Array();
		for (var i = 0; i < optArr.length; i++) {
			chooseStockArr.push(optArr[i].value);
		}
		var keywordListArr = $("#keywords").val();

		if (chooseStockArr == undefined || chooseStockArr.length == 0) {
			alert("품목을 최소 1개 이상 선택해주세요.");
			return;
		}


		if (keywordListArr == null) {
			alert("삭제 할 키워드를 선택해주세요.");
			return;
		}

		if (keywordListArr.length == 1 && keywordListArr[0] == -1) {
			alert("삭제할 키워드가 존재하지 않습니다.");
			return;
		}


		var dataStr = "&stockList=" + encodeURIComponent(JSON.stringify(chooseStockArr)) + "&keywordList=" + encodeURIComponent(JSON.stringify(keywordListArr));

		$.ajax({
			url: DOMAIN + "/stock/manage/deleteKeywordMapping",
			method: "DELETE",
			data: dataStr,
			success: function() {
				keywordManage.loadStockList();
			}, // end success
			error: function() {
				alert("키워드 삭제에 실패하였습니다.");
			}//end error
		}); // end ajax

	});
}

KeywordManage.prototype.addAddedKeywordDblClickEvent = function() {
	$("#keywordList option").off("dblclick").dblclick(function() {
		$(this).remove();
	});
}

KeywordManage.prototype.refreshKeywordDetail = function() {
	$("#selectStockKeywordView").empty();
	var optArr = $("#selectStockList").get(0).options;
	for (var i = 0; i < optArr.length; i++) {
		var opt = optArr[i];
		var keywordArr = $(opt).data("keyword")
		var keywordNameArr = new Array();
		for (var j = 0; j < keywordArr.length; j++) {
			var keywordObj = keywordArr[j];
			keywordNameArr.push(keywordObj.keyword);
		}
		$("#selectStockKeywordView").append(this._keywordDetailHtmlTag.replace("(StockName)", opt.text).replace("(keywordsList)", keywordNameArr.join(", ")));
	}
}

KeywordManage.prototype.loadCurrentSelectStock = function() {
	var optionArr = $("#stockList").prop("options");

	for (var i = 0; i < optionArr.length; i++) {
		var opt = optionArr[i];
		if (this._currentStockList.includes(opt.value)) $(opt).dblclick();
	}
}

KeywordManage.prototype.setStockList = function(list) {
	this._stockList = list;
}

