/**
 * 
 */

function KeywordAddRequest() {
	this._init();
}

KeywordAddRequest.prototype._init = function() {
	this._htmlTag = "<option class=\"stockArg\" value=\"(stockCode)\" data-keyword=\'(keywordList)\'>(StockName)</option>";
	this._keywordAddRequestHtmlTag = "<option class=\"keywordAddRequestArg\" value=\"(keyword)\">(keyword)</option>";
	this.loadStockList();
	this.addInsertBtnClickEvent();
	this.addDeleteBtnClickEvent();

}

KeywordAddRequest.prototype.loadStockList = function() {
	$("#stockList").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/allStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				keywordAddRequest.setStockList(list);
				keywordAddRequest.showStockList(list);
				keywordAddRequest.addStockSearchEvent();
			} // end if
			else {
				alert("주식 품목이 존재하지 않습니다.");
			}
		}, // end success
		error: function(xhr) {
			if (xhr.status == 403) {
				alert("세션이 만료되었습니다. 다시 로그인해주세요.");
				location.replace(DOMAIN + "/manage/")
			} else {
				alert("주식 품목을 불러오지 못했습니다");
			}
		}
	}); // end ajax
}

KeywordAddRequest.prototype.showStockList = function(list) {

	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$("#stockList").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(StockName)", stockObj.name)
			.replace("(keywordList)", JSON.stringify(stockObj.keywordArr)));
	}

	$("#stockList").off("change").on("change", function(e) {
		$("#keywordAddRequestList").empty();
		var code = $(e.target.options[e.target.selectedIndex]).val();
		keywordAddRequest._currentStockCode = code;

		$.ajax({
			url: DOMAIN + "/stock/manage/keywordAddRequest",
			method: "GET",
			dataType: "json",
			headers: $.extend(null, { "userId": userID, "code": code }),
			success: function(list, status) {
				if (status == "success") {
					keywordAddRequest.showKeywordAddRequestList(list);
				} else {
					$("#keywordAddRequestList").empty();
				}
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
					alert("요청 키워드 목록을 불러오지 못했습니다");
				}
			}
		}); // end ajax
	});

}

KeywordAddRequest.prototype.showKeywordAddRequestList = function(list) {
	function compare(a, b) {
		return a.title > b.title ? 1 : a.title < b.title ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var keyword = list[i];
		$("#keywordAddRequestList").append(this._keywordAddRequestHtmlTag.replace("(keyword)", keyword)
			.replace("(keyword)", keyword));
	}
}


KeywordAddRequest.prototype.addStockSearchEvent = function(list) {
	$("#stockName").off("keyup").keyup(function() {
		var searchTerm = $("#stockName").val();
		var list = keywordAddRequest._stockList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = keywordAddRequest.filterList(list);
		keywordAddRequest.showStockList(filterList);
	});
}

KeywordAddRequest.prototype.filterList = function(list) {
	var searchTerm = $("#stockName").val();
	var list = keywordAddRequest._stockList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#stockList").empty();
	return filterList;
}

KeywordAddRequest.prototype.addInsertBtnClickEvent = function() {
	$("#insertBtn").off("click").click(function() {
		var keywordArr = $("#keywordAddRequestList").val();
		
		if(keywordAddRequest._currentStockCode == undefined){
			alert("삭제시킬 키워드 요청이 있는 종목을 선택해주세요.");
			return;	
		}
		
		if(keywordArr == undefined){
			alert("등록 시킬 키워드를 선택해주세요.");
			return;	
		}
		
		var stockArr = new Array(keywordAddRequest._currentStockCode);
		
		$.ajax({
			url: DOMAIN + "/stock/manage/insertKeywordAndMapping",
			method: "POST",
			data: "&stockList="+encodeURIComponent(JSON.stringify(stockArr))+"&keywordList="+encodeURIComponent(JSON.stringify(keywordArr)),
			success: function() {
				if ($("#stockList option").eq(0).val() != undefined) $("#stockList").val(keywordAddRequest._currentStockCode).change();
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
					alert("선택한 요청 키워드를 등록하지 못했습니다");
				}
			}
		}); // end ajax
	});
}

KeywordAddRequest.prototype.addDeleteBtnClickEvent = function() {
	$("#deleteBtn").off("click").click(function() {
		var keywordArr = $("#keywordAddRequestList").val();
		
		if(keywordAddRequest._currentStockCode == undefined){
			alert("삭제시킬 키워드 요청이 있는 종목을 선택해주세요.");
			return;	
		}
		
		if(keywordArr == undefined){
			alert("삭제시킬 키워드 요청을 선택해주세요.");
			return;	
		}
		
		$.ajax({
			url: DOMAIN + "/stock/manage/keywordAddRequest",
			method: "DELETE",
			headers: $.extend(null, { "userId": userID, "code": keywordAddRequest._currentStockCode }),
			data: "&keywordList="+encodeURIComponent(JSON.stringify(keywordArr)),
			success: function() {
				if ($("#stockList option").eq(0).val() != undefined) $("#stockList").val(keywordAddRequest._currentStockCode).change();
			}, // end success
			error: function(xhr) {
				if (xhr.status == 403) {
					alert("세션이 만료되었습니다. 다시 로그인해주세요.");
					location.replace(DOMAIN + "/manage/")
				} else {
					alert("선택한 요청 키워드를 삭제하지 못했습니다");
				}
			}
		}); // end ajax
	});
}

KeywordAddRequest.prototype.setStockList = function(list) {
	this._stockList = list;
}

