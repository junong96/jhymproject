/**
 * 
 */

function KeywordManage2() {
	this._init();
}

KeywordManage2.prototype._init = function() {
	this._htmlTag = "<option class=\"stockArg\" value=\"(stockCode)\" data-keyword=\'(keywordList)\'>(StockName)</option>";
	this._keywordHtmlTag = "<option class=\"keywordArg (addedKeyword)\" value=\"(KID)\" data-stock=\'(stockList)\'>(keyword)</option>";
	this._currentStockList = new Array();
	this._currentKeyword;
	this.loadKeywordList();
	this.loadStockList();
	this.addBtnClickEvent();
	this.addInsertBtnClickEvent();
	this.addDeleteBtnClickEvent();
	this.addClearBtnClickEvent();
	this.addKeywordDeleteBtnClickEvent();

	$("#insertKeyword").on("keydown", function(e) {
		if (e.keyCode == 13) {
			setTimeout(function() {
				if ($("#autocomplete-list div").length == 0) {
					setTimeout(function() {
						if ($("#insertKeyword").val().trim() == "") {
							alert("키워드를 입력해주세요.");
							return;
						}
						if ($("#selectStockList option").size() == 0) {
							alert("현재 선택된 종목이 없습니다.");
							return;
						}
						var keywordName = $("#insertKeyword").val().trim();

						if (checkConsonant(keywordName)) {
							alert("키워드에 자음, 모음, \' 은 포함될 수 없습니다.");
							return;
						}

						keywordName.replace(/\'/gim, "");
						keywordName.replace(/(\s)/gim, "");

						$("#insertKeyword").val("");
						$("#mappingKeyword").append(keywordManage2._keywordHtmlTag.replace("(keyword)", keywordName)
							.replace("(KID)", -100)
							.replace("(addedKeyword)", "addedKeyword"));

						keywordManage2.addAddedKeywordDblClickEvent();

					}, 0.01);
				}
			}, 0.5);
		} else if (e.key == " ") {
			setTimeout(function() {
				$("#insertKeyword").val($("#insertKeyword").val().replace(' ', ''));
			}, 0.01);
		}
	});
}

KeywordManage2.prototype.loadKeywordList = function() {
	$("#keywordList").empty();
	$("#mappingKeyword").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/allKeywordList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				keywordManage2.showKeywordList(list);
				keywordManage2.setKeywordList(list);
				var filterList = keywordManage2.filterListKeyword(list);
				keywordManage2.showKeywordList(filterList);
				keywordManage2.addKeywordSearchEvent();
				autocomplete.setAutocomplete(document.getElementById("insertKeyword"), list);

			} // end if
			else {
				alert("키워드 목록이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alertt("키워드 목록을 불러오지 못했습니다");
		}
	}); // end ajax
}

KeywordManage2.prototype.loadStockList = function() {
	$("#stockList").empty();

	$.ajax({
		url: DOMAIN + "/stock/manage/allStockList",
		method: "GET",
		dataType: "json",
		success: function(list, status) {
			if (status == "success") {
				keywordManage2.setStockList(list);
				keywordManage2.showStockList(list);
				keywordManage2.loadCurrentSelectStock(); // 검색되어 있는 종목 선택
				keywordManage2.setOriginalStockList();
				var filterList = keywordManage2.filterList(list);
				keywordManage2.showStockList(filterList);
				keywordManage2.addStockSearchEvent();
			} // end if
			else {
				alert("주식 품목이 존재하지 않습니다.");
			}
		}, // end success
		error: function() {
			alert("주식 품목을 불러오지 못했습니다");
		}
	}); // end ajax
}

KeywordManage2.prototype.showStockList = function(list) {

	function compare(a, b) {
		return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var stockObj = list[i];
		$("#stockList").append(this._htmlTag.replace("(stockCode)", stockObj.code)
			.replace("(StockName)", stockObj.name)
			.replace("(keywordList)", JSON.stringify(stockObj.keywordArr)));
	}

	$("#selectStockList").off("keydown change").on("keydown change", function(e) {
		$("#mappingKeyword option:not(.addedKeyword)").remove();
		var keywordsStr = $(e.target.options[e.target.selectedIndex]).data("keyword");

		if (keywordsStr == "" || keywordsStr == "[]") {
			return;
		}

		var keywordArr = keywordsStr;

		for (var i = 0; i < keywordArr.length; i++) {
			var keywordObj = keywordArr[i];
			$("#mappingKeyword").prepend(keywordManage2._keywordHtmlTag.replace("(keyword)", keywordObj.keyword)
				.replace("(KID)", keywordObj.KID)
				.replace("(addedKeyword)", ""));
		}
	});

	$("#stockList").off("dblclick").on("dblclick", function(e) {
		if (e.target.type == "select-multiple") return;
		var stockCode = $(e.target).val();
		var keyword = $(e.target).data("keyword");
		var stockName = $(e.target).text();

		var selectStockCodeArr = $("#selectStockList").get(0).options;
		var isExists = false;
		for (var i = 0; i < selectStockCodeArr.length; i++) {
			if (selectStockCodeArr[i].value == stockCode) {
				isExists = true;
				break;
			}
		}

		if (isExists) return;

		if (!keywordManage2._currentStockList.includes(stockCode)) {
			keywordManage2._currentStockList.push(stockCode);
		}

		$("#selectStockList").append(keywordManage2._htmlTag.replace("(stockCode)", stockCode)
			.replace("(StockName)", stockName)
			.replace("(keywordList)", JSON.stringify(keyword)));

	});

	$("#selectStockList").off("dblclick").on("dblclick", function(e) {
		if (e.target.type == "select-multiple") return;
		keywordManage2._currentStockList.splice(keywordManage2._currentStockList.indexOf(e.target.value), 1);
		$(e.target).remove();
		$("#mappingKeyword").empty();
	});
}

KeywordManage2.prototype.showKeywordList = function(list) {

	function compare(a, b) {
		return a.keyword > b.keyword ? 1 : a.keyword < b.keyword ? -1 : 0;
	}

	list.sort(compare);

	for (var i = 0; i < list.length; i++) {
		var keywordObj = list[i];
		if (i == 0 && this._keywordList == undefined) this._currentKeyword = keywordObj.kid;
		$("#keywordList").append(this._keywordHtmlTag.replace("(KID)", keywordObj.kid)
			.replace("(keyword)", keywordObj.keyword)
			.replace("(stockList)", JSON.stringify(keywordObj.stockArr)));
	}

	$("#keywordList").off("keydown change").on("keydown change", function(e) {
		$("#selectStockList").empty();
		$("#mappingKeyword").empty();
		keywordManage2._currentStockList = new Array();

		keywordManage2._currentKeyword = $(e.target.options[e.target.selectedIndex]).val();
		var stockStr = $(e.target.options[e.target.selectedIndex]).data("stock");

		if (stockStr == "" || stockStr == "[]" || stockStr == undefined) {
			return;
		}

		var stockArr = stockStr;

		for (var i = 0; i < stockArr.length; i++) {
			var stockObj = stockArr[i];
			var referObj;
			keywordManage2._$originalStockOptArr.each(function(index, item) { if (item.value == stockObj.code) referObj = item });
			$("#selectStockList").append(keywordManage2._htmlTag.replace("(StockName)", stockObj.name)
				.replace("(stockCode)", stockObj.code)
				.replace("(keywordList)", JSON.stringify($(referObj).data("keyword")))
			);

			if (!keywordManage2._currentStockList.includes(stockObj.code)) {
				keywordManage2._currentStockList.push(stockObj.code);
			}
		}

		if ($("#selectStockList option").eq(0).val() != undefined) $("#selectStockList").val($("#selectStockList option").eq(0).val()).change();
	});
}

KeywordManage2.prototype.addStockSearchEvent = function(list) {
	$("#stockName").off("keyup").keyup(function() {
		var searchTerm = $("#stockName").val();
		var list = keywordManage2._stockList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = keywordManage2.filterList(list);
		keywordManage2.showStockList(filterList);
	});
}

KeywordManage2.prototype.addKeywordSearchEvent = function(list) {
	$("#keywordName").off("keyup").keyup(function() {
		var searchTerm = $("#keywordName").val();
		var list = keywordManage2._keywordList;

		var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
		var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

		if (pattern.test(searchTermLastChar)) return;

		var filterList = keywordManage2.filterListKeyword(list);
		keywordManage2.showKeywordList(filterList);
	});
}

KeywordManage2.prototype.filterList = function(list) {
	var searchTerm = $("#stockName").val();
	var list = keywordManage2._stockList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#stockList").empty();
	return filterList;
}

KeywordManage2.prototype.filterListKeyword = function(list) {
	var searchTerm = $("#keywordName").val();
	var list = keywordManage2._keywordList;

	var searchTermLastChar = searchTerm.charAt(searchTerm.length - 1);
	var pattern = /([^가-힣a-zA-Z0-9\x20])/i;

	if (pattern.test(searchTermLastChar)) return;
	var filterList = list.filter(function(obj) {
		return obj.keyword.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
	});

	$("#keywordList").empty();
	return filterList;
}

KeywordManage2.prototype.loadCurrentSelectStock = function() {
	$("#selectStockList").empty();
	var optionArr = $("#stockList").prop("options");

	for (var i = 0; i < optionArr.length; i++) {
		var opt = optionArr[i];
		if (this._currentStockList.includes(opt.value)) $(opt).dblclick();
	}

	if ($("#selectStockList option").eq(0).val() != undefined) $("#selectStockList").val($("#selectStockList option").eq(0).val()).change();
}

KeywordManage2.prototype.loadCurrentKeyword = function() {
	$("#keywordList").val(this._currentKeyword).change();
}

KeywordManage2.prototype.addKeywordDeleteBtnClickEvent = function() {
	$("#keywordDeleteBtn").off("click").click(function() {
		var keywordListArr = $("#keywordList").val();

		if (keywordListArr == undefined || keywordListArr.length == 0) {
			alert("삭제할 키워드를 선택해주세요.");
			return;
		}

		var dataStr = "&keywordList=" + encodeURIComponent(JSON.stringify(keywordListArr));

		$.ajax({
			url: DOMAIN + "/stock/manage/deleteKeyword",
			method: "DELETE",
			data: dataStr,
			success: function() {
				keywordManage2.loadStockList();
				keywordManage2.loadKeywordList();
			}, // end success
			error: function() {
				alert("키워드 삭제에 실패하였습니다.");
			}//end error
		}); // end ajax
	});
}

KeywordManage2.prototype.addBtnClickEvent = function() {
	$("#addBtn").off("click").click(function() {

	});
}

KeywordManage2.prototype.addInsertBtnClickEvent = function() {
	$("#totalInsertBtn, #selectedInsertBtn").off("click").click(function(e) {
		var stockOptArr = $("#selectStockList").get(0).options;
		var chooseStockArr = new Array();

		if (e.target.id == "totalInsertBtn") {
			for (var i = 0; i < stockOptArr.length; i++) {
				chooseStockArr.push(stockOptArr[i].value);
			}
		} else {
			chooseStockArr = $("#selectStockList").val();
		}

		var keywordOptArr = $("#mappingKeyword").get(0).options;
		var keywordListArr = new Array();
		for (var i = 0; i < keywordOptArr.length; i++) {
			var keywordName = keywordOptArr[i].text.trim();
			keywordName.replace(/\'/gim, "");
			keywordName.replace(/(\s)/gim, "");

			keywordListArr.push(keywordName);
		}

		if (chooseStockArr == undefined || chooseStockArr.length == 0) {
			alert("품목을 최소 1개 이상 선택해주세요.");
			return;
		}


		if (keywordListArr == undefined || keywordListArr.length == 0) {
			alert("키워드를 최소 1개 이상 입력해주세요.");
			return;
		}

		var dataStr = "&stockList=" + encodeURIComponent(JSON.stringify(chooseStockArr)) + "&keywordList=" + encodeURIComponent(JSON.stringify(keywordListArr));

		$.ajax({
			url: DOMAIN + "/stock/manage/insertKeywordAndMapping",
			method: "POST",
			data: dataStr,
			success: function() {
				keywordManage2.loadStockList();
				keywordManage2.loadKeywordList();
			}, // end success
			error: function() {
				alert("키워드 등록에 실패하였습니다.");
			}//end error
		}); // end ajax
	});
}

KeywordManage2.prototype.addDeleteBtnClickEvent = function() {
	$("#totalDeleteBtn, #selectedDeleteBtn").off("click").click(function(e) {
		var stockOptArr = $("#selectStockList").get(0).options;
		var chooseStockArr = new Array();

		if (e.target.id == "totalDeleteBtn") {
			for (var i = 0; i < stockOptArr.length; i++) {
				chooseStockArr.push(stockOptArr[i].value);
			}
		} else {
			chooseStockArr = $("#selectStockList").val();
		}

		var keywordListArr = $("#mappingKeyword").val();

		if (chooseStockArr == undefined || chooseStockArr.length == 0) {
			alert("품목을 최소 1개 이상 선택해주세요.");
			return;
		}


		if (keywordListArr == undefined || keywordListArr.length == 0) {
			alert("키워드를 최소 1개 이상 선택해주세요.");
			return;
		}

		var dataStr = "&stockList=" + encodeURIComponent(JSON.stringify(chooseStockArr)) + "&keywordList=" + encodeURIComponent(JSON.stringify(keywordListArr));

		$.ajax({
			url: DOMAIN + "/stock/manage/deleteKeywordMapping",
			method: "DELETE",
			data: dataStr,
			success: function() {
				keywordManage2.loadStockList();
				keywordManage2.loadKeywordList();
			}, // end success
			error: function() {
				alert("키워드 매핑 삭제에 실패하였습니다.");
			}//end error
		}); // end ajax
	});
}

KeywordManage2.prototype.addClearBtnClickEvent = function() {
	$("#clearBtn").off("click").click(function() {
		$("#selectStockList").empty();
		$("#mappingKeyword").empty();
		keywordManage._currentStockList = [];
	});
}

KeywordManage2.prototype.addAddedKeywordDblClickEvent = function() {
	$("#mappingKeyword option").off("dblclick").dblclick(function() {
		if ($(this).hasClass("addedKeyword")) $(this).remove();
	});
}

KeywordManage2.prototype.setStockList = function(list) {
	this._stockList = list;
}

KeywordManage2.prototype.setOriginalStockList = function(list) {
	this._$originalStockOptArr = $("#stockList option");
}


KeywordManage2.prototype.setKeywordList = function(list) {
	this._keywordList = list;
}

