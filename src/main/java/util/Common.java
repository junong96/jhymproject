package util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.json.JSONObject;

/**
 * @author :정준형(jhj@rinasoft.co.kr)
 * @since :2021. 4. 14.
 * @description :
 */
public class Common {
	CONFIG_VALUE values;

	public Common(CONFIG_VALUE values) {
		this.values = values;
	}

	/**
	 * 스터디그룹의 이름의 유효성을 체크하는 메서드(a-zA-Z0-9가-힣)
	 * 
	 * @param name 이름
	 * @return 특수문자 또는 한글 모음이 포함된 경우 : false, 그외의 경우 : true
	 * @since 2018. 6. 28
	 */
	public boolean isName(String name) {
		boolean isName = false;

		if (name != null && name.length() <= 20) {
			String regEx = "[^a-zA-Z0-9가-힣\\u3040-\\u30ff\\u3400-\\u4dbf\\u4e00-\\u9fff\\uf900-\\ufaff\\uff66-\\uff9f ]+";

			Pattern pattern = Pattern.compile(regEx);
			Matcher matcher = pattern.matcher(name);

			// a-zA-z0-9가-힣를 제외한 문제가 포함된 경우 false를 반환해야된다. 그러나 위 조건식으로는
			// a-zA-z0-9가-힣를 제외한 문제가 포함된 경우 true를 반환하므로 반대값을 구한다.
			isName = !matcher.find();
		}

		return isName;
	}

	/**
	 * 스터디그룹의 키워드의 유효성을 체크하는 메서드(문자 또는 숫자 그리고 _만 가능)
	 * 
	 * @param keyword 키워드
	 * @return 문자 또는 숫자 그리고 _만 포함된 경우 : true, 그외의 경우 : false
	 * @since 2018. 6. 29
	 */
	public boolean isKeyword(String keyword) {
		boolean isKeyword = false;

		if (keyword != null && keyword.length() <= 30) {
			// 한글, 영대/소문자, 숫자, _와 공백문자를 제외한 문자(_와 공백문자를 제외한 모든 특수문자 그리고 한글 모음)
			String specialRegEx = "[^a-zA-Z0-9가-힣\\u3040-\\u30ff\\u3400-\\u4dbf\\u4e00-\\u9fff\\uf900-\\ufaff\\uff66-\\uff9f ]+";

			Pattern pattern = Pattern.compile(specialRegEx);
			Matcher matcher = pattern.matcher(keyword);
			if (matcher.find() == false) {
				// 키워드 내 _와 공백문자를 제외한 특수문자와 한글 모음이 없다면

				String nonKoreanRegEx = "[a-zA-Z0-9_ ]";
				pattern = Pattern.compile(nonKoreanRegEx);
				matcher = pattern.matcher(keyword);

				int nonKoreanAmount = 0;
				while (matcher.find()) {
					nonKoreanAmount++;
				}

				if (nonKoreanAmount <= 20) {
					String koreanRegEx = "[가-힣\\u3040-\\u30ff\\u3400-\\u4dbf\\u4e00-\\u9fff\\uf900-\\ufaff\\uff66-\\uff9f ]+";
					pattern = Pattern.compile(koreanRegEx);
					matcher = pattern.matcher(keyword);

					int koreanAmount = 0;
					while (matcher.find()) {
						koreanAmount++;
					}

					if (koreanAmount <= 10) {
						isKeyword = true;
					}
				} // end if
			} // end if
		} // end if

		return isKeyword;
	}

	/**
	 * 전달받은 값이 토큰형식인지 검증([a-zA-Z0-9])
	 * 
	 * @param token 토큰
	 * @return boolean
	 * @since 2018. 7. 20
	 */
	public boolean isToken(String token) {
		if (token.length() != 28) {
			return false;
		}

		String tokenRegEx = "[a-zA-Z0-9]";

		Pattern pattern = Pattern.compile(tokenRegEx);
		Matcher matcher = pattern.matcher(token);

		return matcher.find();
	}

	/**
	 * 전달받은 토큰 목록이 토큰형식인지 확인하는 메서드
	 * 
	 * @param tokenList 토큰 목록
	 * @return 토큰 목록의 모든 토큰이 토큰형식이다 : true, 토큰 목록의 단 하나의 토큰이라도 토큰형식이 아니다 : false
	 * @since 2018. 7. 20
	 */
	public boolean isToken(ArrayList<String> tokenList) {
		boolean isToken = false;

		for (String token : tokenList) {
			isToken = isToken(token);

			if (isToken == false)
				break;
		} // end for

		return isToken;
	}

	public static String getOsByUserAgent(@Context HttpServletRequest request) {
		String OS = "";

		if (request.getHeader("user-agent") != null) {
			if (request.getHeader("user-agent").contains("Android")
					|| request.getHeader("user-agent").contains("YkTime")) {
				OS = "A";
			}
		}

		return OS;
	}

	public static String extractUrl(String content) {
		try {
			String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
			Pattern p = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(content);
			if (m.find()) {
				return m.group();
			}
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
