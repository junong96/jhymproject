package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.FailInsertException;
import exception.v1.UnknownFilenameExtensionException;

public class FileUtil {
	protected final String allowFilenameExtension = "(gif|(jpg|jpeg)|png|heic)|(avi|(mpg|mpeg)|mp4|asf|mov)|(mp3|wav)|zip|(txt|rtf|hwp|pdf|(doc|docx)|ppt)|(xlsx|xls|xml|csv|tsv)";

	/**
	 * 파일의 확장자를 추출하는 메서드
	 * 
	 * @param filename 확장자가 포함된 파일의 이름
	 * @return String((gif|(jpg|jpeg)|png)|(avi|(mpg|mpeg)|asf)|(mp3|wav)|zip|(txt|rtf|hwp|pdf|doc|ppt))
	 * @throws UnknownFilenameExtensionException 파일의 이름에 확장자가 없거나 지원하지 않는 확장자의 경우
	 * @since 2018. 8. 30
	 */
	public String extractFilenameExtension(String filename) throws UnknownFilenameExtensionException {
		String filenameExtension = null;

		String[] filenameInfo = filename.split("\\.");
		if (filenameInfo.length == 0) {
			throw new UnknownFilenameExtensionException("unkown");
		}

		filenameExtension = filenameInfo[filenameInfo.length - 1].toLowerCase();

		Pattern pattern = Pattern.compile(allowFilenameExtension);
		Matcher matcher = pattern.matcher(filenameExtension);
		if (matcher.find() == false) {
			throw new UnknownFilenameExtensionException("unkown");
		}

		return filenameExtension;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 주식 종목 데이터를 가져오는 메서드
	 * @param fileStream
	 * @return
	 * @throws FailInsertException
	 */
	public JSONArray getStockList(InputStream fileStream) throws FailInsertException {
		JSONArray stockList = new JSONArray();
		if (fileStream != null) {
			String line = "";
			try {
				BufferedReader buff = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
				boolean isFirst = true;
				while ((line = buff.readLine()) != null) {
					if (isFirst) {
						isFirst = false;
						continue;
					}
					if (!line.equals("")) {
						JSONArray stockInfoArr = new JSONArray();
						line = line.replaceAll(",", "").replaceAll("\"", "");
						String[] stockStrArr = line.split("\t");

						for (int i = 0; i < stockStrArr.length; i++) {
							if (i == stockStrArr.length - 1) {
								if (stockStrArr[i].equals("kosdaq")) {
									stockStrArr[i] = "KD";
								} else if (stockStrArr[i].equals("kospi")) {
									stockStrArr[i] = "KP";
								} else {
									stockStrArr[i] = "ND";
								}
							}
							stockInfoArr.put(stockStrArr[i]);
						}
						stockList.put(stockInfoArr);
					}
				}
			} catch (IOException e) {
				throw new FailInsertException("파일 로드 실패");
			}
		}
		return stockList;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 06.
	 * @description : 주식 매매(수량 데이터를 파일로 부터 가져오는 메서드
	 * @param fileStream
	 * @return
	 * @throws FailInsertException
	 */
	public JSONObject getInvestorAmountList(InputStream fileStream) throws FailInsertException {
		JSONObject resultObj = new JSONObject();
		JSONArray investorList = new JSONArray();
		JSONArray investorListedSharesList = new JSONArray();
		if (fileStream != null) {
			String line = "";
			try {
				BufferedReader buff = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
				boolean isSkip = true;
				int index = 0;
				while ((line = buff.readLine()) != null) {
					if (isSkip) {
						index++;
						if (index > 1) {
							isSkip = false;
						}
						continue;
					}

					if (!line.equals("")) {
						JSONArray investorInfoArr = new JSONArray();
						line = line.replaceAll(",", "").replaceAll("\"", "");
						String[] investorInfoStrArr = line.split("\t");
						
						if(investorInfoStrArr.length < 15) continue;
						
						for (int i = 0; i < investorInfoStrArr.length; i++) {
							if ( i == 2 || i == 9 || i == 10 || i == 11)
								continue;
							if (i == 4)
								investorInfoStrArr[i] = investorInfoStrArr[i].replaceAll("%", "");

							investorInfoArr.put(investorInfoStrArr[i]);
						}

						JSONObject listedSharesObj = new JSONObject();
						listedSharesObj.put("name", investorInfoStrArr[0]);
						listedSharesObj.put("listedShares", investorInfoStrArr[10]);

						investorList.put(investorInfoArr);
						investorListedSharesList.put(listedSharesObj);
					}

				}

				resultObj.put("investorList", investorList);
				resultObj.put("investorListedSharesList", investorListedSharesList);
			} catch (IOException e) {
				throw new FailInsertException("파일 로드 실패");
			}
		}
		return resultObj;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 06.
	 * @description : 주식 매매(금액) 데이터를 파일로 부터 가져오는 메서드
	 * @param fileStream
	 * @return
	 * @throws FailInsertException
	 */
	public JSONObject getInvestorPriceList(InputStream fileStream) throws FailInsertException {
		JSONObject resultObj = new JSONObject();
		JSONArray investorList = new JSONArray();
		JSONArray investorMarketCapitalList = new JSONArray();
		if (fileStream != null) {
			String line = "";
			try {
				BufferedReader buff = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
				boolean isSkip = true;
				int index = 0;
				while ((line = buff.readLine()) != null) {
					if (isSkip) {
						index++;
						if (index > 1) {
							isSkip = false;
						}
						continue;
					}
					
					if (!line.equals("")) {
						JSONArray investorInfoArr = new JSONArray();
						line = line.replaceAll(",", "").replaceAll("\"", "");
						String[] investorInfoStrArr = line.split("\t");
						
						if(investorInfoStrArr.length < 15) continue;
						
						for (int i = 0; i < investorInfoStrArr.length; i++) {
							if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 9 || i == 10 || i == 11)
								continue;
							investorInfoArr.put(investorInfoStrArr[i]);
						}

						JSONObject marketCapitalObj = new JSONObject();
						marketCapitalObj.put("name", investorInfoStrArr[0]);
						marketCapitalObj.put("marketCapital", investorInfoStrArr[10]);

						investorList.put(investorInfoArr);
						investorMarketCapitalList.put(marketCapitalObj);
					}

				}
				resultObj.put("investorList", investorList);
				resultObj.put("investorMarketCapitalList", investorMarketCapitalList);
			} catch (IOException e) {
				throw new FailInsertException("파일 로드 실패");
			}
		}
		return resultObj;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 오늘의 키워드 데이터를 파일로 부터 가져오는 메서드
	 * @param fileStream
	 * @return
	 * @throws FailInsertException
	 */
	public JSONArray getTodayKeywordList(InputStream fileStream) throws FailInsertException {
		JSONArray todayOROvertimeKeywordList = new JSONArray();
		if (fileStream != null) {
			String line = "";
			try {
				BufferedReader buff = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
				boolean isFirst = true;
				while ((line = buff.readLine()) != null) {
					if (isFirst) {
						isFirst = false;
						continue;
					}
					if (!line.equals("")) {
						JSONArray todayOROvertimeKeywordInfoArr = new JSONArray();
						line = line.replaceAll(",", "").replaceAll("\"", "");
						String[] todayOROvertimeKeywordStrArr = line.split("\t");
						
						for (int i = 0; i < todayOROvertimeKeywordStrArr.length; i++) {
							if (i == 0 || i == 1 || i == 4 || i == 8 || i == 9 || i == 10 || i == 11)
								continue;
							
							todayOROvertimeKeywordInfoArr.put(todayOROvertimeKeywordStrArr[i]);
						}
						todayOROvertimeKeywordList.put(todayOROvertimeKeywordInfoArr);
					}

				}
			} catch (IOException e) {
				throw new FailInsertException("파일 로드 실패");
			}
		}
		return todayOROvertimeKeywordList;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 시간외 급상승 키워드 데이터를 파일로 부터 가져오는 메서드
	 * @param fileStream
	 * @return
	 * @throws FailInsertException
	 */
	public JSONArray getOvertimeKeywordList(InputStream fileStream) throws FailInsertException {
		JSONArray todayOROvertimeKeywordList = new JSONArray();

		if (fileStream != null) {
			String line = "";
			try {
				BufferedReader buff = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
				boolean isFirst = true;
				while ((line = buff.readLine()) != null) {
					if (isFirst) {
						isFirst = false;
						continue;
					}
					if (!line.equals("")) {
						JSONArray todayOROvertimeKeywordInfoArr = new JSONArray();
						line = line.replaceAll(",", "").replaceAll("\"", "");
						String[] todayOROvertimeKeywordStrArr = line.split("\t");
						
						for (int i = 0; i < todayOROvertimeKeywordStrArr.length; i++) {
							if (i == 0 || i == 1 || i == 4 || i == 9 || i == 10)
								continue;
							todayOROvertimeKeywordInfoArr.put(todayOROvertimeKeywordStrArr[i]);
						}
						todayOROvertimeKeywordList.put(todayOROvertimeKeywordInfoArr);
					}

				}
			} catch (IOException e) {
				throw new FailInsertException("파일 로드 실패");
			}
		}
		return todayOROvertimeKeywordList;
	}
}
