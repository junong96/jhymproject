package util;

import org.json.JSONObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;

/**
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 9. 18
 */
public class CONFIG_VALUE {
	public final String JQUERY_URL = "https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js";
	public final String JQUERY_MOBILE_URL = "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js";
	public final String JQUERY_UI_URL = "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js";
	public final String GOOGLE_PLAY_STORE_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=kr.co.rinasoft.yktime";
	public final String STOCK_DB_NAME = "stockinfo/kr";
	public final static String S3_URL = "https://s3-ap-northeast-1.amazonaws.com";
	public final static String S3_YKTIME_URL = S3_URL + "/yktime";
	public final static String S3_YKTIME_PROFILE_URL = S3_YKTIME_URL + "/image/profile";
	public final static String S3_YKTIM_BUTKET_NAME = "yktime";

	public static String getS3Url() {
		return S3_URL;
	}

	public static String getS3YktimeUrl() {
		return S3_YKTIME_URL;
	}

	public static String getS3YktimeProfileUrl() {
		return S3_YKTIME_PROFILE_URL;
	}

	public String getJQUERY_URL() {
		return JQUERY_URL;
	}

	public String getJQUERY_MOBILE_URL() {
		return JQUERY_MOBILE_URL;
	}

	public String getJQUERY_UI_URL() {
		return JQUERY_UI_URL;
	}

	public String getGOOGLE_PLAY_STORE_DOWNLOAD_URL() {
		return GOOGLE_PLAY_STORE_DOWNLOAD_URL;
	}

	public CONFIG_VALUE(String localAddr) {
		if (localAddr.equals("192.168.0.9")) {
			rootPath = "/Users/rinasoft/workspace/stockInfo";
			rootUrl = "http://192.168.0.9:18080/stockInfo";
			DBName = "stockinfo/kr";
			serverLanguageCode = "KO";
			S3BucketName = "stockInfo";
			S3URL = "https://s3-ap-northeast-1.amazonaws.com/" + S3BucketName;
			redisWriteUrl = "192.168.0.9";
			redisReadUrl = "192.168.0.9";
		} else if (localAddr.equals("192.168.0.11")) {
			// 실제 웹 서버
			rootPath = "/var/lib/tomcat8/webapps/stockInfo";
			rootUrl = "http://stock.flipfocus.co.kr/stockInfo";
			DBName = "stockinfo/kr";
			serverLanguageCode = "KO";
			S3BucketName = "stockInfo";
			S3URL = "https://s3-ap-northeast-1.amazonaws.com/" + S3BucketName;
			redisWriteUrl = "192.168.0.9";
			redisReadUrl = "192.168.0.9";
		}
	}

	private String rootPath;
	private String rootUrl;
	private String DBName;
	private String S3BucketName;
	private String S3URL;
	private String serverLanguageCode;
	private String redisWriteUrl;
	private String redisReadUrl;

	private Jedis jedisRead;
	private JedisPool jedisReadPool;

	private Jedis jedisWrite;
	private JedisPool jedisWritePool;
	private String os = "";

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	private void createJedisRead() {
//		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		try {
//			this.jedisReadPool = new JedisPool(jedisPoolConfig, this.redisReadUrl, 6379);
			this.jedisRead = new Jedis(redisReadUrl, 6379);
		} catch (JedisException e) {
			e.printStackTrace();
			System.out.println("레디스 연결에 실패하였습니다.");
			this.jedisRead = null;
		}
	}

	private void createJedis() {
//		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		try {
//			this.jedisWritePool = new JedisPool(jedisPoolConfig, this.redisWriteUrl, 6379);
			this.jedisWrite = new Jedis(redisWriteUrl, 6379);
		} catch (JedisException e) {
			e.printStackTrace();
			System.out.println("레디스 연결에 실패하였습니다.");
			this.jedisWrite = null;
		}
	}

	public void closeJedis() {
		if (jedisRead != null) {
			jedisRead.close();
			jedisRead = null;
		}

		if (jedisReadPool != null) {
			jedisReadPool.close();
			jedisReadPool = null;
		}

		if (jedisWrite != null) {
			jedisWrite.close();
			jedisWrite = null;
		}

		if (jedisWritePool != null) {
			jedisWritePool.close();
			jedisWritePool = null;
		}
	}

	public Jedis getJedisReadOnly() {
		if (jedisRead == null) {
			createJedisRead();
		}
		return jedisRead;
	}

	public Jedis getJedis() {
		if (jedisWrite == null) {
			createJedis();
		}
		return jedisWrite;
	}

	public String getServerLanguageCode() {
		return serverLanguageCode;
	}

	public void setServerLanguageCode(String serverLanguageCode) {
		this.serverLanguageCode = serverLanguageCode;
	}

	public String getRootPath() {
		return rootPath;
	}

	public String getRootUrl() {
		return rootUrl;
	}

	public String getDBName() {
		return DBName;
	}

	public String getS3BucketName() {
		return S3BucketName;
	}

	public String getS3URL() {
		return S3URL;
	}

	public String getRedisUrl() {
		return redisWriteUrl;
	}

	public String toString() {
		JSONObject json = new JSONObject();
		json.put("rootPath", rootPath);
		json.put("rootUrl", rootUrl);
		json.put("DBName", DBName);
		json.put("S3BucketName", S3BucketName);
		json.put("S3URL", S3URL);

		return json.toString();
	}

}
