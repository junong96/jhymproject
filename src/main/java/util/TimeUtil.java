package util;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 11. 5.
 *
 */
public class TimeUtil {
	/**
	 * 하루 시작 기준시간을 사용해 서버 날짜와 시간을 반환하는 메서드
	 * 
	 * @param dayStartTime
	 *            하루 시작 기준시간
	 * @return LocalDateTime
	 * @since 2018. 11. 5.
	 */
	public LocalDateTime getLocalDateTimeByDayStartTime(int dayStartTime) {
		LocalDateTime serverDateTime = LocalDateTime.now();
		if(dayStartTime > 0) {
			
			serverDateTime = serverDateTime.minusHours((long) dayStartTime);
			
			
		}
		
		return serverDateTime;
	}
	
	/**
	 * 하루 시작 기준시간을 사용해 서버 날짜를 반환하는 메서드
	 * 
	 * @param dayStartTime
	 *            하루 시작 기준시간
	 * @return LocalDate
	 * @since 2018. 11. 5.
	 */
	public LocalDate getLocalDateByDayStartTime(int dayStartTime) {
		return getLocalDateTimeByDayStartTime(dayStartTime).toLocalDate();
	}
	
	/**
	 * 하루 시작 기준시간을 사용해 서버 날짜를 반환하는 메서드
	 * 
	 * @param dayStartTime
	 *            하루 시작 기준시간
	 * @return LocalDate
	 * @since 2018. 11. 5.
	 */
	public LocalDate getLocalDateByDayStartTime(String dayStartTime) {
		return getLocalDateTimeByDayStartTime(Integer.valueOf(dayStartTime)).toLocalDate();
	}
}
