package util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Enumeration;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import exception.v1.FailGenerateTokenException;

public class EnDecryptionTool {
	private String iv;
	private Key keySpec;

	public EnDecryptionTool() {
		try {
			String key = "key-7e6ds7v47c45-ra0oavglvqx0jc5qlk2";
			iv = key.substring(0, 16);
			
			byte[] keyBytes = new byte[16];
			byte[] b = key.getBytes("UTF-8");
			int len = b.length;
			if (len > keyBytes.length) {
				len = keyBytes.length;
			}
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			
			this.keySpec = keySpec;
		} catch(IndexOutOfBoundsException | ArrayStoreException | NullPointerException | IllegalArgumentException | UnsupportedEncodingException e) {
			this.keySpec = null;
		}
	}

	public String aesEncode(String str) {
		String enStr = "";
		
		try {
			Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c.init(1, keySpec, new IvParameterSpec(iv.getBytes()));

			byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
			Encoder encoder = Base64.getEncoder();

			enStr = new String(encoder.encodeToString(encrypted));
			enStr = URLEncoder.encode(enStr, "UTF-8");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | InvalidKeyException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
		
		return enStr;
	}
	
	public String aesEncode(int str) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		return aesEncode(String.valueOf(str));
	}
	
	public String aesEncodeWithNotURLEncode(int str)throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
	InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		return aesEncodeWithNotURLEncode(String.valueOf(str));
	}
	
	public String aesEncodeWithNotURLEncode(String str)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(1, keySpec, new IvParameterSpec(iv.getBytes()));

		byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
		Encoder encoder = Base64.getEncoder();

		String enStr = new String(encoder.encodeToString(encrypted));

		return enStr;
	}
	
	public String aesDecode(String str)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(2, keySpec, new IvParameterSpec(iv.getBytes("UTF-8")));

		Decoder decoder = Base64.getDecoder();
		byte[] byteStr = decoder.decode(str);
		
		return new String(c.doFinal(byteStr), "UTF-8");
	}
	
	/** 암호화된 파라미터를 복호화 한 뒤 분리해서 반환할 메서드 **/
	public ArrayList<String> decrpytParameter(HttpServletRequest request, ArrayList<String> extractList){
		ArrayList<String> parameterList = new ArrayList<String>();
		
		/* 전달받은 암호화 된 파라미터 */
		Enumeration<String> encrpytParameter = request.getParameterNames();
		/* 암호화 된 파라미터를 복호화해 저장할 문자열 */
		String decryptParameter;
		/* 복호화 된 파라미터를 &로 분리한 파라미터 목록을 저장할 배열 */
		String[] parameterGroup;
		/* &로 분리된 파라미터 목록을 =로 분리한 목록을 저장할 배열 */
		String[] parameters;
		/*  */
		String extractName;
		/* */
		int extractRoofAmount = extractList.size();
		
		try {
			/* get파라미터 중 첫 번째 파라미터값을 가져온다. */
			decryptParameter = aesDecode(encrpytParameter.nextElement());

			/* 첫 번쨰 파라미터를 &로 분리한 뒤 저장한다. */
			parameterGroup = decryptParameter.split("&");
			for(int i=0; i<parameterGroup.length; i++){
				/* &로 분리된 파라미터 목록에서 i번쨰 파라미터를 =로 분리한다. */
				parameters = parameterGroup[i].split("=");
				
				for(int j=0; j<extractRoofAmount; j++) {
					extractName = extractList.get(j);

					if(parameters[0].equals(extractName))
						parameterList.add(parameters[1]);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		if(parameterList.isEmpty())
			return null;
		else
			return parameterList;
	}
	
	/**
	 * 28자리 식별값(토큰)을 생성하는 메서드(고유한 값임을 보장하지 않는다.)
	 * 
	 * @param source
	 *            식별값 생성에 쓰일 값
	 * @return String
	 * @throws FailGenerateTokenException
	 *             서버 문제로 식별값을 생성하지 못했을 경우
	 * @since 2018. 12. 5.
	 */
	public String generateToken(String source) throws FailGenerateTokenException {
		String token = null;
		
		try {
			token = aesEncodeWithNotURLEncode(source);
			String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
			token = token.replaceAll(match, "");
			token = token.substring(0, 28);
			
			return token;
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			throw new FailGenerateTokenException("fail generate token");
		} catch(StringIndexOutOfBoundsException e) {
			return generateToken(System.currentTimeMillis()+token);
		}
	}
	
	/**
	 * 28자리 식별값(토큰)을 생성하는 메서드(고유한 값임을 보장하지 않는다.)
	 * 
	 * @param source
	 *            식별값 생성에 쓰일 값
	 * @return String
	 * @throws FailGenerateTokenException
	 *             서버 문제로 식별값을 생성하지 못했을 경우
	 * @since 2018. 12. 5.
	 */
	public String generateToken(int source) throws FailGenerateTokenException {
		return generateToken(String.valueOf(source));
	}
	
	/**
	 * 28자리 식별값(토큰)을 생성하는 메서드(고유한 값임을 보장하지 않는다.)
	 * 
	 * @param source
	 *            식별값 생성에 쓰일 값
	 * @return String
	 * @throws FailGenerateTokenException
	 *             서버 문제로 식별값을 생성하지 못했을 경우
	 * @since 2018. 12. 5.
	 */
	public String generateToken(long source) throws FailGenerateTokenException {
		return generateToken(String.valueOf(source));
	}
}
