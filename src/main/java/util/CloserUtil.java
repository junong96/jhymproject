package util;

import java.sql.SQLException;

public class CloserUtil {
	
	public static void closeable(AutoCloseable... closeArr) {
		for(AutoCloseable closeObj : closeArr) {
			if(closeObj != null){ try { closeObj.close(); } catch (Exception e) { e.printStackTrace(); } }
		}
	}
}
