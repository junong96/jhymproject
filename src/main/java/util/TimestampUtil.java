package util;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimestampUtil {
	/**
	 * 특정 시간대 영역의 날짜와 시간을 갖고 있는 인스턴스를 반환
	 * 
	 * @param timezone
	 *            시간대 영역
	 * @return localDateTime
	 * @see LocalDateTime
	 * @since 2018. 9. 5
	 */
	public static LocalDateTime getLocalDateTimeAtTimeZone(String timezone) {
		ZoneId zoneId;
		try {
			zoneId = ZoneId.of(timezone);
		} catch(NullPointerException | DateTimeException e) {
			zoneId = ZoneId.of("Asia/Seoul");
		}
		
		// 현재날짜와 시간을 가지고 있는 인스턴스 생성
		LocalDateTime localDateTime = LocalDateTime.now(zoneId);
		
		return localDateTime;
	}
	/**
	 * 밀리초를 LocalDateTime의 인스턴스로 바꿔준다.
	 * 
	 * @param millisecond
	 *            밀리초
	 * @return 밀리초의 년, 월, 일, 시, 분, 초 정보를 갖고 있는 LocalDateTime의 인스턴스
	 * @since 2018. 5. 21
	 */
	public static LocalDateTime millisecondToLocalDateTime(Long millisecond) throws DateTimeException {
		LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millisecond), ZoneId.systemDefault());
		return localDateTime;
	}
	
	/**
	 * 밀리초를 LocalDate의 인스턴스로 바꿔준다.
	 * 
	 * @param millisecond
	 *            밀리초
	 * @return
	 * @since 2018. 6. 14
	 */
	public static LocalDate millisecondToLocalDate(Long millisecond) throws DateTimeException {
		LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millisecond), ZoneId.systemDefault());
		return localDateTime.toLocalDate();
	}
	
	/**
	 * LocalDateTime의 인스턴스를 밀리초로 바꾼다.
	 * 
	 * @param ldt
	 *            LocalDateTime.class의 인스턴스
	 * @param zoneID
	 *            바꿀 지역 식별값
	 * @return 13자리 정수
	 * @since 2018. 5. 17
	 * @see java.time.ZoneId
	 */
	public static long localDateTimeToMillisecond(LocalDateTime ldt, String zoneID) {
		ZonedDateTime zdt = ldt.atZone(ZoneId.of(zoneID));
		
		return zdt.toInstant().toEpochMilli();
	}
	
	/**
	 * LocalDateTime의 인스턴스를 서울 시간대의 밀리초로 바꾼다.
	 * 
	 * @param ldt
	 *            LocalDateTime.class의 인스턴스
	 * @return 13자리 정수
	 * @since 2018. 5. 17
	 */
	public static long localDateTimeToMillisecond(LocalDateTime ldt) {
		return localDateTimeToMillisecond(ldt, "Asia/Seoul");
	}
	
	/**
	 * YYYY-MM-DDTHH:mm:ss를 LocalDateTime의 인스턴스로 바꾼다.
	 * 
	 * @param dateTime
	 *            YYYY-MM-DDTHH:mm:ss
	 * @return YYYY-MM-DDTHH:mm:ss의 정보를 갖고 있는 LocalDateTime의 인스턴스
	 * @since 2018. 5. 17
	 * @see java.time.format.DateTimeFormatter
	 */
	public static LocalDateTime dateTimeToLocalDateTime(String dateTime) {
		return LocalDateTime.parse(dateTime.replace(" ", "T"));
	}

	/**
	 * LocalDateTime의 인스턴스를 Mysql의 DateTime형식의 문자열로 바꾼다.
	 * 
	 * @param LocalDateTime
	 *            LocalDateTime.class의 인스턴스
	 * @return YYYY-MM-DD HH:mm:ss형식의 문자열
	 * @since 2018. 5. 17
	 */
	public static String convertToDateTime(LocalDateTime localDateTime) {
		String dateTime = localDateTime.toString();
		
		return dateTime.replace("T", " ").substring(0, dateTime.length()-4);
	}
}
