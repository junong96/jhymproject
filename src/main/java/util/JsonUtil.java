package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
	public static JSONArray getJsonStringFromMap( TreeMap<String, Integer> dateList )
    {
		JSONArray list = new JSONArray();
        for( Entry<String, Integer> entry : dateList.entrySet() ) {
        	   JSONObject jsonObject = new JSONObject();
            String key = entry.getKey();
            Object value = entry.getValue();
            jsonObject.put(key, value);
            list.put(jsonObject);
        }
        
        return list;
    }
	public static JSONArray jsonSort(JSONArray list) {

	    JSONArray sortedJsonArray = new JSONArray();

	    List<JSONObject> jsonValues = new ArrayList<JSONObject>();
	    for (int i = 0; i < list.length(); i++) {
	        jsonValues.add(list.getJSONObject(i));
	    }
	    Collections.sort( jsonValues, new Comparator<JSONObject>() {
	        //You can change "Name" with "ID" if you want to sort by ID
	        private static final String KEY_NAME = "Name";

	        @Override
	        public int compare(JSONObject a, JSONObject b) {
	            String valA = new String();
	            String valB = new String();

	            try {
	                valA = (String) a.get(KEY_NAME);
	                valB = (String) b.get(KEY_NAME);
	            } 
	            catch (JSONException e) {
	                //do something
	            }

	            return valA.compareTo(valB);
	            //if you want to change the sort order, simply use the following:
	            //return -valA.compareTo(valB);
	        }
	    });

	    for (int i = 0; i < list.length(); i++) {
	        sortedJsonArray.put(jsonValues.get(i));
	    }
		return sortedJsonArray;
	}
}
