package util;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


/**
 * @author	:정준형(jhj@rinasoft.co.kr)
 * @since 	:2021. 4. 14.
 * @description : DB접속과 관련된 클래스
 */
public class DBManager {
	public static Connection getConnection(String value) {
		Connection conn;
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup(value);
			
			conn = ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
		
		return conn;
	}
	
	public static Connection getConnection() {
		return getConnection("mariadb/yktime");
	}
}
