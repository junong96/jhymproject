package entrance;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.media.multipart.MultiPartFeature;

import keywordManage.ManageService;
import main.MainService;
import stock.StockService;
import stockNote.StockNoteService;




/**
 * @author	:정준형(jhj@rinasoft.co.kr)
 * @since 	:2021. 4. 14.
 * @description : API의 진입점
 */
@ApplicationPath("/stock")
public class Enterance extends Application {
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(MultiPartFeature.class);
		classes.add(MainService.class);
		classes.add(ManageService.class);
		classes.add(StockService.class);
		classes.add(StockNoteService.class);
		
		return classes;
	}
}
