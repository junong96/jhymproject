package keywordManage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import dao.AdminStockDao;
import dao.AdminStockNoteDao;
import dao.StockDao;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import exception.v1.FailUpdateException;
import util.CONFIG_VALUE;

/**
 * @author :정준형(jhj@rinasoft.co.kr)
 * @since :2021. 4. 14.
 * @description : 주식 품목에 키워드 등록, 삭제 하기 위한 서비스
 */
@Path("/manage")
public class ManageService {

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 7.
	 * @description : 주식 품목리스트를 가져옴
	 * @param request
	 * @return
	 */
	@Path("/onlyStockList")
	@GET
	public Response getOnlyStockList(@Context HttpServletRequest request) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			JSONArray stockList = stockDao.getOnlyStockList();
			if (stockList.length() != 0) {
				return Response.ok().entity(stockList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 14.
	 * @description : 주식 품목리스트를 키워드와 함 가져옴
	 * @param request
	 * @return
	 */
	@Path("/allStockList")
	@GET
	public Response getAllStockList(@Context HttpServletRequest request) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject stockListObj;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			stockListObj = stockDao.getAllStockList();
			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			JSONArray stockList = stockListObj.toJSONArray(stockListObj.names());
			if (stockListObj.length() != 0) {
				return Response.ok().entity(stockList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 14.
	 * @description : 주식 키워드 리스트를 품목과 함께 가져옴
	 * @param request
	 * @return
	 */
	@Path("/allKeywordList")
	@GET
	public Response getAllKeywordList(@Context HttpServletRequest request) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject keywordListObj;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			keywordListObj = stockDao.getAllKeywordList();
			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			JSONArray keywordList = keywordListObj.toJSONArray(keywordListObj.names());
			if (keywordListObj.length() != 0) {
				return Response.ok().entity(keywordList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 3.
	 * @description : 오늘의 키워드 데이터 등록 과정에서 존재하지 않는 종목에 대한 리스트
	 * @param request
	 * @return
	 */
	@Path("/noneExistsStockList")
	@GET
	public Response getNoneExistsStockList(@Context HttpServletRequest request) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONArray noneExistsStockList;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			noneExistsStockList = stockDao.getNoneExistsStockList();
			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			if (noneExistsStockList.length() != 0) {
				return Response.ok().entity(noneExistsStockList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 3.
	 * @description : 관리자 페이지에서 종목 추가하는 API
	 * @param request
	 * @param name
	 * @param code
	 * @param sectors
	 * @param product
	 * @param type
	 * @return
	 */
	@Path("/insertStock")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertStock(@Context HttpServletRequest request, @FormParam("name") String name,
			@FormParam("code") String code, @FormParam("sectors") String sectors, @FormParam("product") String product,
			@FormParam("type") String type) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		if (name == null || name.length() == 0)
			return Response.status(Status.BAD_REQUEST).build();
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if ((!type.equals("KD") && !type.equals("KP") && !type.equals("ND")) || type == null)
			return Response.status(Status.BAD_REQUEST).build();

		try {
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			boolean result = stockDao.insertStock(name, code, sectors, product, type);
			if (result) {
				return Response.status(Status.CREATED).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailInsertException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 3.
	 * @description : 관리자 페이지에서 종목 추가하는 API
	 * @param request
	 * @param name
	 * @param code
	 * @param sectors
	 * @param product
	 * @param type
	 * @return
	 */
	@Path("/deleteStock")
	@DELETE
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteStock(@Context HttpServletRequest request, @FormParam("code") String code) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();

		try {
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			boolean result = stockDao.deleteStock(code);
			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 14.
	 * @description : 키워드를 품목에 매핑시키는 메서드
	 * @param request
	 * @param stockList
	 * @param keywordList
	 * @return
	 */
	@Path("/insertKeywordAndMapping")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertKeywordAndMapping(@Context HttpServletRequest request,
			@FormParam("stockList") String stockList, @FormParam("keywordList") String keywordList) {
		if ((stockList != null && stockList.length() == 0) || stockList == null)
			return Response.status(Status.BAD_REQUEST).build();
		if ((keywordList != null && keywordList.length() == 0) || keywordList == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {

			JSONArray stockArr = new JSONArray(stockList);
			JSONArray keywordArr = new JSONArray(keywordList);
			if (stockArr.length() == 0 || keywordArr.length() == 0)
				return Response.status(Status.BAD_REQUEST).build();

			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			boolean result = stockDao.insertKeywordAndMapping(stockArr, keywordArr);

			if (result) {
				return Response.ok().build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailInsertException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 키워드를 품목이랑 연결 해제하는 API
	 * @param request
	 * @param stockList
	 * @param keywordList
	 * @return
	 */
	@Path("/deleteKeywordMapping")
	@DELETE
	public Response deleteKeywordMapping(@Context HttpServletRequest request, @FormParam("stockList") String stockList,
			@FormParam("keywordList") String keywordList) {
		if ((stockList != null && stockList.length() == 0) || stockList == null)
			return Response.status(Status.BAD_REQUEST).build();
		if ((keywordList != null && keywordList.length() == 0) || keywordList == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONArray stockArr = new JSONArray(stockList);
			JSONArray keywordArr = new JSONArray(keywordList);

			if (stockArr.length() == 0 || keywordArr.length() == 0)
				return Response.status(Status.BAD_REQUEST).build();

			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			boolean result = stockDao.DeleteKeywordMapping(stockArr, keywordArr);

			if (result) {
				return Response.ok().build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 07.
	 * @description : 키워드를 삭제하는 API
	 * @param request
	 * @param stockList
	 * @param keywordList
	 * @return
	 */
	@Path("/deleteKeyword")
	@DELETE
	public Response deleteKeyword(@Context HttpServletRequest request, @FormParam("keywordList") String keywordList) {
		if ((keywordList != null && keywordList.length() == 0) || keywordList == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONArray keywordArr = new JSONArray(keywordList);
			if (keywordArr.length() == 0)
				return Response.status(Status.BAD_REQUEST).build();

			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			boolean result = stockDao.DeleteKeyword(keywordArr);

			if (result) {
				return Response.ok().build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 07.
	 * @description : 특정 종목에 대해 추가요청이 들어온 키워드를 조회하는 API
	 * @param request
	 * @param stockList
	 * @param keywordList
	 * @return
	 */
	@Path("/keywordAddRequest")
	@GET
	public Response getKeywordAddRequest(@Context HttpServletRequest request, @HeaderParam("userId") int userId, @HeaderParam("code") String code) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {

			// 주식 품목을 가져오기 위한 DAO 객체
			AdminStockDao stockDao = new AdminStockDao(values);
			JSONArray keywordList = stockDao.getKeywordAddRequest(code);
			if(keywordList.length() != 0) {
				return Response.ok().entity(keywordList.toString()).build();
			}else {
				return Response.noContent().build();
			}
			
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 07.
	 * @description : 특정 종목에 대해 추가요청이 들어온 키워드를 삭제하는 API
	 * @param request
	 * @param stockList
	 * @param keywordList
	 * @return
	 */
	@Path("/keywordAddRequest")
	@DELETE
	public Response deleteKeywordAddRequest(@Context HttpServletRequest request,  @HeaderParam("userId") int userId, @HeaderParam("code") String code,
			@FormParam("keywordList") String keywordList) {
		if ((keywordList != null && keywordList.length() == 0) || keywordList == null)
			return Response.status(Status.BAD_REQUEST).build();
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONArray keywordArr = new JSONArray(keywordList);
			if (keywordArr.length() == 0)
				return Response.status(Status.BAD_REQUEST).build();

			// 주식 품목을 가져오기 위한 DAO 객체
			AdminStockDao stockDao = new AdminStockDao(values);
			boolean result = stockDao.DeleteKeywordAddRequest(code, keywordArr);

			if (result) {
				return Response.ok().build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 운영자가 등록한 종목 노트,
	 * @param request
	 * @param userId
	 * @param code
	 * @return
	 */
	@Path("stockNote")
	@GET
	public Response getStockNoteList(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject publicAndPrivateStockNoteListObj;
			// 주식 종목 노트를 가져오기 위한 DAO 객체
			AdminStockNoteDao stockNoteDao = new AdminStockNoteDao(values);

			publicAndPrivateStockNoteListObj = stockNoteDao.getStockNoteList(code);

			return Response.ok().entity(publicAndPrivateStockNoteListObj.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 12.
	 * @description : 관리자 전용 종목 노트 추가
	 * @param request
	 * @param userId
	 * @param code
	 * @param title
	 * @param contents
	 * @return
	 */
	@Path("stockNote")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertStockNote(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code, @FormParam("title") String title,
			@FormParam("contents") String contents) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		try {
			// 주식 종목 노트를 삽입하기 위한 DAO 객체
			AdminStockNoteDao stockNoteDao = new AdminStockNoteDao(values);

			int noteId = stockNoteDao.insertStockNote(userId, code, title, contents);

			return Response.status(Status.CREATED).entity(noteId).build();
		} catch (FailInsertException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 12.
	 * @description : 관리자 전용 종목 노트 수정
	 * @param request
	 * @param stockNoteId
	 * @param userId
	 * @param title
	 * @param contents
	 * @return
	 */
	@Path("stockNote")
	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response modifyStockNote(@Context HttpServletRequest request, @HeaderParam("stockNoteId") int stockNoteId,
			@HeaderParam("userId") int userId, @FormParam("title") String title,
			@FormParam("contents") String contents) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		if (stockNoteId < 1)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId < 1)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		try {
			AdminStockNoteDao stockNoteDao = new AdminStockNoteDao(values);

			boolean result = stockNoteDao.updateStockNote(stockNoteId, title, contents);
			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailUpdateException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 12.
	 * @description : 관리자 전용 종목 노트 삭제
	 * @param request
	 * @param userId
	 * @param stockNoteId
	 * @return
	 */
	@Path("stockNote")
	@DELETE
	public Response deleteStockNote(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("stockNoteId") int stockNoteId) {
		if (stockNoteId < 1)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId < 1)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			AdminStockNoteDao stockNoteDao = new AdminStockNoteDao(values);

			boolean result = stockNoteDao.deleteStockNote(stockNoteId);
			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 세션 객체에 저장된 정보랑 대조하여 유효한 요청인지 검사하는 부분
	 * @param request
	 * @param userId
	 * @return
	 */
	private String checkValidation(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Boolean isLogin = (Boolean) session.getAttribute("isLogin");
		Integer id = (Integer) session.getAttribute("id");
		if (isLogin == null) { // 로그인이 되어있지 않을 경우
			return "noLogin";
		} else if (!id.equals(1) && !id.equals(2)) { // 유효하지 않을 경우
			return "noValid";
		} else {
			return "valid";
		}
	}
}
