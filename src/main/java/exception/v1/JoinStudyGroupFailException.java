package exception.v1;

/**
 * 서버문제로 스터디그룹에 가입하지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 5
 */
public class JoinStudyGroupFailException extends Exception {
	private static final long serialVersionUID = 7531910307865534034L;

	public JoinStudyGroupFailException(String message) {
		super(message);
	}
}
