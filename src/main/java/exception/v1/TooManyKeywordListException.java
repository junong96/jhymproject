package exception.v1;

/**
 * 키워드의 개수가 너무 많을때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class TooManyKeywordListException extends Exception {
	private static final long serialVersionUID = 1252055805344901213L;

	public TooManyKeywordListException(String message) {
		super(message);
	}
}
