package exception.v1;

/**
 * 스터디그룹 생성시 가입 가능한 멤버의 한계를 제한할 떄 설정 가능한 한계를 넘어섰을 경우 발생하는 예외<br>
 * - 체험판의 경우 : 최대 3명까지 한계 설정가능<br>
 * - 일반판의 경우 : 최대 30명까지 한계 설정가능<br>
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadMemberLimitException extends Exception {
	private static final long serialVersionUID = 120885748783260277L;

	public BadMemberLimitException(String message) {
		super(message);
	}
}
