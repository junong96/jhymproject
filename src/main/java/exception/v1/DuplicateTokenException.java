package exception.v1;

/**
 * 스터디 그룹 생성시 토큰이 중복됬을 때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 29
 */
public class DuplicateTokenException extends Exception {
	private static final long serialVersionUID = -1112357917631067840L;

	public DuplicateTokenException(String message) {
		super(message);
	}
}
