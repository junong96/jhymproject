package exception.v1;

/**
 * 리워드로 지급할 포인트가 남은 회비보다 많은 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 25
 */
public class TooManyPointException extends Exception {
	private static final long serialVersionUID = -7873183427985263320L;

	public TooManyPointException(String message) {
		super(message);
	}
}
