package exception.v1;

/**
 * 목표 인덱스가 0미만 또는 15초과일 때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 15
 */
public class BadGoalIndexException extends Exception {
	private static final long serialVersionUID = 3529157154253104086L;

	public BadGoalIndexException(String message) {
		super(message);
	}
}
