package exception.v1;

/**
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 24
 */
public class FailRequestLeaderDelegationException extends Exception {
	private static final long serialVersionUID = 5760224501289950069L;

	public FailRequestLeaderDelegationException(String message) {
		super(message);
	}
}
