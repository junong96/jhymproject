package exception.v1;

/**
 * 스터디 그룹 생성시 그룹 이름이 중복됬을 때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 29
 */
public class DuplicateNameException extends Exception {
	private static final long serialVersionUID = -8588888637807425239L;

	public DuplicateNameException(String message) {
		super(message);
	}
}
