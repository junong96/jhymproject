package exception.v1;

/**
 * DB에 데이터를 INSERT하는 중 DB문제로 INSERT를 하지 못했을 경우 발생하는 예외
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 1
 */
public class FailInsertException extends Exception {
	private static final long serialVersionUID = -5954723546688966920L;

	public FailInsertException(String message) {
		super(message);
	}
}
