package exception.v1;

/**
 * DB서버 문제로 페널티 지급에 실패했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 25
 */
public class FailGivePenaltyException extends Exception {
	private static final long serialVersionUID = -750143976715743452L;

	public FailGivePenaltyException(String message) {
		super(message);
	}
}