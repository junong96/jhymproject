package exception.v1;

/**
 * UPDATE 쿼리 수행 중 DB문제로 쿼리 수행을 하지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 4
 */
public class FailUpdateException extends Exception {
	private static final long serialVersionUID = -8571954274381777877L;

	public FailUpdateException(String message) {
		super(message);
	}
}
