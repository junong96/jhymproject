package exception.v1;

/**
 * 이미 가입한 스터디 그룹에 다시 가입 신청을 했을 경우
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 5
 */
public class AlreadyJoinStudyGroupException extends Exception {
	private static final long serialVersionUID = 5962825594873899036L;

public AlreadyJoinStudyGroupException(String message) {
	 super(message);
 }
}