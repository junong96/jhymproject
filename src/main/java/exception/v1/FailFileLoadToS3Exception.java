package exception.v1;

/**
 * S3에서 파일을 가져오지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 17
 */
public class FailFileLoadToS3Exception extends RuntimeException {
	private static final long serialVersionUID = -3508974545166261189L;

	public FailFileLoadToS3Exception(String message) {
		super(message);
	}
}
