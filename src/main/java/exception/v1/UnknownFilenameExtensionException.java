package exception.v1;

/**
 * 파일의 확장자를 알 수 없을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 17
 */
public class UnknownFilenameExtensionException extends Exception {
	private static final long serialVersionUID = 800432685798724421L;

	public UnknownFilenameExtensionException(String message) {
		super(message);
	}
}
