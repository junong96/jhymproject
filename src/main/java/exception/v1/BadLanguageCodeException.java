package exception.v1;

/**
 * 지원하지 않는 언어코드를 전달받았을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2019. 3. 12.
 *
 */
public class BadLanguageCodeException extends Exception {
	private static final long serialVersionUID = -8842746950983096999L;

	public BadLanguageCodeException(String message) {
		super(message);
	}
}
