package exception.v1;

/**
 * 이메일이 비었거나 올바른 형식이 아닐 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 15
 */
public class BadEmailException extends Exception {
	private static final long serialVersionUID = 1313266220587407671L;
	
	public BadEmailException(String message) {
		super(message);
	}
}
