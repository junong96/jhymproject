package exception.v1;

/**
 * 사업자 번호가 올바르지 않을 경우 발생하는 예외
 * @author 이창신(ycs318@naver.com)
 * @since 2019. 5. 21.
 *
 */
public class DuplicateArgumentException extends Exception {
	private static final long serialVersionUID = -6806972268650222909L;

	public DuplicateArgumentException(String message) {
		super(message);
	}
}
