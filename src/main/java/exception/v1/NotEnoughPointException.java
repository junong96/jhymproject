package exception.v1;

/**
 * 포인트로 상품 구매 시 포인트가 부족할 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 21
 */
public class NotEnoughPointException extends Exception {
	private static final long serialVersionUID = 1903443669593028266L;

	public NotEnoughPointException(String message) {
		super(message);
	}
}
