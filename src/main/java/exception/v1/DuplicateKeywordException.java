package exception.v1;

/**
 * 스터디 그룹의 키워드 삽입 시 중복된 키워드가 있을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 2
 */
public class DuplicateKeywordException extends Exception {
	private static final long serialVersionUID = -1255786740181778399L;

	public DuplicateKeywordException(String message) {
		super(message);
	}
}
