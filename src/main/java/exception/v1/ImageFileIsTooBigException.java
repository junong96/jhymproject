package exception.v1;

/**
 * 이미지 파일의 크기가 100KB를 초과했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 18
 */
public class ImageFileIsTooBigException extends Exception {
	private static final long serialVersionUID = 5307396435598125587L;

	public ImageFileIsTooBigException(String message) {
		super(message);
	}
}
