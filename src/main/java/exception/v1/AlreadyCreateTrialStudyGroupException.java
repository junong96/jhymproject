package exception.v1;

/**
 * 체험판 스터디 그룹을 이미 개설한 사용자가 다시 체험판 스터디 그룹을 개설하려고할 경우
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 2
 */
public class AlreadyCreateTrialStudyGroupException extends Exception {
	private static final long serialVersionUID = 6803740296217423007L;

	public AlreadyCreateTrialStudyGroupException(String message) {
		super(message);
	}
}
