package exception.v1;

/**
 * 파일의 크기가 10MB를 초과했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 17
 */
public class FileIsTooBigException extends Exception {
	private static final long serialVersionUID = 3524601052357319438L;

	public FileIsTooBigException(String message) {
		super(message);
	}
}
