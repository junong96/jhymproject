package exception.v1;

/**
 * DB서버 문제로 리더 위임 요청을 거절하지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 24
 */
public class FailRejectLeaderDelegationException extends Exception {
	private static final long serialVersionUID = 4035096494618805838L;
	
	public FailRejectLeaderDelegationException(String message) {
		super(message);
	}
}
