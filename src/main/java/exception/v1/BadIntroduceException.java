package exception.v1;

/**
 * 스터디그룹의 한 줄 소개가 50자를 초과했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadIntroduceException extends Exception {
	private static final long serialVersionUID = 7489020718360093251L;

	public BadIntroduceException(String message) {
		super(message);
	}
}
