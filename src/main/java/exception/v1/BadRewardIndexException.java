package exception.v1;

/**
 * 리워드 지급 시 등록된 리워드가 없거나 리워드 번호가 등록된 리워드의 개수 이상인 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 25
 */
public class BadRewardIndexException extends Exception {
	private static final long serialVersionUID = 7859507416446907994L;

	public BadRewardIndexException(String message) {
		super(message);
	}
}
