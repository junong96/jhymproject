package exception.v1;

/**
 * DELETE 쿼리 또는 DELETE의 역할을 하는 쿼리를 수행하는 도중 DB서버에 문제가 발생했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 25
 */
public class FailDeleteException extends Exception {
	private static final long serialVersionUID = -926238591540384295L;

	public FailDeleteException(String message) {
		super(message);
	}
}
