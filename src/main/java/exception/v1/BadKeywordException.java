package exception.v1;

/**
 * 키워드의 형식이 올바르지 않을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadKeywordException extends Exception {
	private static final long serialVersionUID = -1257006958529708173L;

	public BadKeywordException(String message) {
		super(message);
	}
}
