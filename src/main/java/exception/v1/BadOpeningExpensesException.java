package exception.v1;

/**
 * 개설 비용 ID가 올바르지 않을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 23
 */
public class BadOpeningExpensesException extends Exception {
	private static final long serialVersionUID = 5606239077427399172L;

	public BadOpeningExpensesException(String message) {
		super(message);
	}
}
