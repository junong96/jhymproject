package exception.v1;

/**
 * DB에 어떤 데이터를 INSERT할 때 또는 하기 전에 데이터가 이미 존재한다면 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 30
 */
public class AlreadyException extends Exception {
	private static final long serialVersionUID = 2520302571871075120L;

	public AlreadyException(String message) {
		super(message);
	}
}
