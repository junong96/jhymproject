package exception.v1;

/**
 * 비공개 스터디 그룹 검색 시 code가 잘못됬을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 4
 */
public class BadCodeException extends Exception {
	private static final long serialVersionUID = -1937973702328698845L;

	public BadCodeException(String message) {
		super(message);
	}
}
