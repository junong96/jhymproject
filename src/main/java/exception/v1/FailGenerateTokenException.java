package exception.v1;

/**
 * 토큰 만들기를 실패했을 때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 11
 */
public class FailGenerateTokenException extends RuntimeException {
	private static final long serialVersionUID = 3209444645091611557L;

	public FailGenerateTokenException(String message) {
		super(message);
	}
}
