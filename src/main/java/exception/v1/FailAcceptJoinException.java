package exception.v1;

/**
 * 가입 신청 수락 실패 시 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 20
 */
public class FailAcceptJoinException extends Exception {
	private static final long serialVersionUID = 7474188370569628551L;

	public FailAcceptJoinException(String message) {
		super(message);
	}
}
