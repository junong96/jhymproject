package exception.v1;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * 인증받지 못한 영역의 작업을 하려고 할때 발생하는 예외<br>
 * 별도의 예외처리를 하지 않으면 401 응답코드를 반환한다.
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 26
 * @see javax.ws.rs.core.Response.Status.UNAUTHORIZED
 */
public class UnauthorizedException extends WebApplicationException{
	private static final long serialVersionUID = -4777460570537947941L;

	public UnauthorizedException() {
		super((Throwable) null, Response.status(Status.UNAUTHORIZED).build());
	}
}
