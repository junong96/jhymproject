package exception.v1;

/**
 * 목표시간이 0미만 또는 86400을 초과했다면 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadGoalTimeException extends Exception {
	private static final long serialVersionUID = -8567485594922320575L;

	public BadGoalTimeException(String message) {
		super(message);
	}
}
