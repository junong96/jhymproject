package exception.v1;

/**
 * DB서버 문제로 리워드 지급에 실패했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 24
 */
public class FailGiveRewardException extends Exception {
	private static final long serialVersionUID = -1570204198736223700L;

	public FailGiveRewardException(String message) {
		super(message);
	}
}