package exception.v1;

/**
 * 비공개 스터디 그룹만 할 수 있는 행동을 공개 스터디 그롭이 하려고 할 경우
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 20
 */
public class NotPrivateStudyGroupException extends Exception {
	private static final long serialVersionUID = 6666758414397271496L;

	public NotPrivateStudyGroupException(String message) {
		super(message);
	}
}
