package exception.v1;

/**
 * 페널티 지급 시 등록된 페널티가 없거나 페널티 번호가 등록된 페널티의 개수를 초과한 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 25
 */
public class BadPenaltyIndexException extends Exception {
	private static final long serialVersionUID = 2752195480387461472L;

	public BadPenaltyIndexException(String message) {
		super(message);
	}
}
