package exception.v1;

/**
 * DB서버 문제로 강제탈퇴를 하지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 24
 */
public class FailForcedWithdrawalException extends Exception {
	private static final long serialVersionUID = 128191028616814994L;

	public FailForcedWithdrawalException(String message) {
		super(message);
	}
}
