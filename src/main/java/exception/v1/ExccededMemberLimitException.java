package exception.v1;

/**
 * 정원이 초과된 스터디 그룹에 가입할 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 4
 */
public class ExccededMemberLimitException extends Exception {
	private static final long serialVersionUID = -7735375081840764249L;

	public ExccededMemberLimitException(String message) {
		super(message);
	}
}
