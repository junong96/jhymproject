package exception.v1;

/**
 * DB서버 문제로 리더 위임을 수락하지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 24
 */
public class FailAcceptLeaderDelegationException extends Exception {
	private static final long serialVersionUID = 2228385573563051052L;

	public FailAcceptLeaderDelegationException(String message) {
		super(message);
	}
}
