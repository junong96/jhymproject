package exception.v1;

/**
 * 키워드 삽입 시 서버에 문제가 생겼을 때 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 5
 */
public class KeywordInsertException extends Exception {
	private static final long serialVersionUID = 5193981516608258120L;

	public KeywordInsertException(String message) {
		super(message);
	}
}
