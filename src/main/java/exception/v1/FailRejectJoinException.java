package exception.v1;

/**
 * 가입 신청 거절 실패 시 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 20
 */
public class FailRejectJoinException extends Exception {
	private static final long serialVersionUID = -1103814534954988822L;

	public FailRejectJoinException(String message) {
		super(message);
	}
}
