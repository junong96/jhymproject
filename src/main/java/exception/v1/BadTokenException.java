package exception.v1;

/**
 * userToken으로 사용자 정보를 찾지 못했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 5. 30
 */
public class BadTokenException extends RuntimeException {
	private static final long serialVersionUID = 6013418782776179608L;

	public BadTokenException(String message) {
		super(message);
	}
}
