package exception.v1;

/**
 * 구글 API로 푸시 요청을 실패했을 경우
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 20
 */
public class FailPushRequestException extends RuntimeException {
	private static final long serialVersionUID = 5504096595709549128L;

	public FailPushRequestException(String message) {
		super(message);
	}
}
