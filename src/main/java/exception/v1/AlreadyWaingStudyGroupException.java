package exception.v1;

/**
 * 이미 스터디 그룹에 가입 신청을 했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 5
 */
public class AlreadyWaingStudyGroupException extends Exception {
	private static final long serialVersionUID = 332409295899056221L;

	public AlreadyWaingStudyGroupException(String message) {
		super(message);
	}
}
