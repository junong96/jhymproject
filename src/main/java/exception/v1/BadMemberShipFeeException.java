package exception.v1;

/**
 * 스터디그룹의 회비가 1000원 미만일 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 29
 */
public class BadMemberShipFeeException extends Exception {
	private static final long serialVersionUID = 4093821174127623854L;

	public BadMemberShipFeeException(String message) {
		super(message);
	}
}
