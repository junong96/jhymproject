package exception.v1;

/**
 * 스터디 그룹 생성 시 이름이 올바르지 않을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadNameException extends Exception {
	private static final long serialVersionUID = -2270934437261183036L;

	public BadNameException(String message) {
		super(message);
	}
}
