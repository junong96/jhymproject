package exception.v1;

/**
 * DB SELECT에 실패했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 7. 23
 */
public class FailSelectException extends Exception {
	private static final long serialVersionUID = 6196336228468457937L;

	public FailSelectException(String message) {
		super(message);
	}
}
