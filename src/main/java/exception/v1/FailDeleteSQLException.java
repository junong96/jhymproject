package exception.v1;

/**
 * DB의 DELETE 작업 수행에 실패했을 경우 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018 .7. 19
 */
public class FailDeleteSQLException extends Exception {
	private static final long serialVersionUID = -539641550345047268L;

	public FailDeleteSQLException(String message) {
		super(message);
	}
}
