package exception.v1;

/**
 * 하루시작 기준시간이 0미만 또는 24를 초과했다면 발생하는 예외
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 28
 */
public class BadDayStartTimeException extends Exception {
	private static final long serialVersionUID = 9191214588320474037L;

	public BadDayStartTimeException(String message) {
		super(message);
	}
}
