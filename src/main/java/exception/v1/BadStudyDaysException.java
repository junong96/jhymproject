package exception.v1;

/**
 * 
 * @author 이창신(ycs318@naver.com)
 * @since 2018. 6. 29
 */
public class BadStudyDaysException extends Exception {
	private static final long serialVersionUID = 7873182124395211162L;

	public BadStudyDaysException(String message) {
		super(message);
	}
}
