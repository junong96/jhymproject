package stockNote;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import dao.StockNoteDao;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import exception.v1.FailUpdateException;
import io.umehara.ogmapper.DefaultOgMapper;
import io.umehara.ogmapper.domain.OgTags;
import io.umehara.ogmapper.jsoup.JsoupOgMapperFactory;
import util.CONFIG_VALUE;
import util.Common;

/**
 * @author :정준형(jhj@rinasoft.co.kr)
 * @since :2021. 4. 14.
 * @description : 주식 종목 노트 관련 서비스 API
 */
@Path("/stockNote")
public class StockNoteService {

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 사용자 개인이 등록한 종목 노트,
	 * @param request
	 * @param userId
	 * @param code
	 * @return
	 */
	@Path("stockNoteAmount")
	@GET
	public Response getStockNoteAmount(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject publicAndPrivateStockNoteAmountObj;
			// 주식 종목 노트를 가져오기 위한 DAO 객체
			StockNoteDao stockNoteDao = new StockNoteDao(values);
			publicAndPrivateStockNoteAmountObj = stockNoteDao.getStockNoteAmount(userId, code);

			return Response.ok().entity(publicAndPrivateStockNoteAmountObj.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 사용자 개인이 등록한 종목 노트,
	 * @param request
	 * @param userId
	 * @param code
	 * @return
	 */
	@Path("stockNoteList")
	@GET
	public Response getStockNoteList(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code, @QueryParam("offset") int offset) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject publicAndPrivateStockNoteListObj;
			// 주식 종목 노트를 가져오기 위한 DAO 객체
			StockNoteDao stockNoteDao = new StockNoteDao(values);
			int size = 10;
			publicAndPrivateStockNoteListObj = stockNoteDao.getStockNoteList(userId, code, offset, size);

			// JSONArray privateStockNoteList =
			// publicAndPrivateStockNoteListObj.getJSONArray("privateStockNoteList");
			// URLOgDataMapping(privateStockNoteList);

			return Response.ok().entity(publicAndPrivateStockNoteListObj.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertStockNote(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code, @FormParam("title") String title, @FormParam("contents") String contents,
			@FormParam("type") String type) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();
		if (type == null || (!type.equals("memo") && !type.equals("url")))
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		String checkValid = checkValidation(request, userId);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		try {
			// 주식 종목 노트를 삽입하기 위한 DAO 객체
			StockNoteDao stockNoteDao = new StockNoteDao(values);

			int noteId = stockNoteDao.insertStockNote(userId, code, title, contents, type);

			return Response.status(Status.CREATED).entity(noteId).build();
		} catch (FailInsertException e) {
			return Response.serverError().build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response modifyStockNote(@Context HttpServletRequest request, @HeaderParam("stockNoteId") int stockNoteId,
			@HeaderParam("userId") int userId, @HeaderParam("code") String code, @FormParam("title") String title,
			@FormParam("contents") String contents) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (stockNoteId < 1)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId < 1)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request, userId);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		try {
			StockNoteDao stockNoteDao = new StockNoteDao(values);

			boolean result = stockNoteDao.updateStockNote(stockNoteId, userId, code, title, contents);
			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailUpdateException e) {
			return Response.serverError().build();
		}
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteStockNote(@Context HttpServletRequest request, @HeaderParam("stockNoteId") int stockNoteId,
			@HeaderParam("userId") int userId) {
		if (stockNoteId < 1)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId < 1)
			return Response.status(Status.BAD_REQUEST).build();

		String checkValid = checkValidation(request, userId);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			StockNoteDao stockNoteDao = new StockNoteDao(values);

			boolean result = stockNoteDao.deleteStockNote(stockNoteId, userId);
			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}
		} catch (FailDeleteException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 세션 객체에 저장된 정보랑 대조하여 유효한 요청인지 검사하는 부분
	 * @param request
	 * @param userId
	 * @return
	 */
	private String checkValidation(HttpServletRequest request, int userId) {
		HttpSession session = request.getSession();
		Boolean isLogin = (Boolean) session.getAttribute("isLogin");
		Integer id = (Integer) session.getAttribute("id");

		if (isLogin == null) { // 로그인이 되어있지 않을 경우
			return "noLogin";
		} else if (!id.equals(userId)) { // 유효하지 않을 경우
			return "noValid";
		} else {
			return "valid";
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : OG 데이터를 가져온다.
	 * @param privateStockNoteList
	 */
	/**
	 * @param request
	 * @param url
	 * @return
	 */
	@Path("ogDataMapping")
	@GET
	public Response URLOgDataMapping(@Context HttpServletRequest request, @HeaderParam("url") String url) {
		JSONObject ogDataObj = new JSONObject();

		String extractUrl = Common.extractUrl(url);
		if (extractUrl.equals("")) {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}

		DefaultOgMapper ogMapper = new JsoupOgMapperFactory().build();
		OgTags ogTags;
		try {
			ogTags = ogMapper.process(new URL(extractUrl));
			if (ogTags != null) {
				ogDataObj.put("ogTitle", ogTags.getTitle());
				ogDataObj.put("ogUrl", ogTags.getUrl());
				ogDataObj.put("ogImageUrl", ogTags.getImage());
			} else {
				ogDataObj.put("ogTitle", "No Info");
				ogDataObj.put("ogUrl", "No Info");
				ogDataObj.put("ogImageUrl", "");
			}
			
			return Response.status(Status.OK).entity(ogDataObj.toString()).build();
		} catch (MalformedURLException e) {
			return Response.serverError().build();
		} // end catch
	}

}
