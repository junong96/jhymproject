package dao;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.DuplicateNameException;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import exception.v1.FailUpdateException;
import io.umehara.ogmapper.DefaultOgMapper;
import io.umehara.ogmapper.domain.OgTags;
import io.umehara.ogmapper.jsoup.JsoupOgMapperFactory;
import util.CONFIG_VALUE;
import util.CloserUtil;
import util.DBManager;

public class AdminStockNoteDao {
	CONFIG_VALUE values;

	public AdminStockNoteDao() {

	}

	public AdminStockNoteDao(CONFIG_VALUE values) {
		this.values = values;
	}

	public JSONObject getStockNoteList(String code) throws FailSelectException {
		JSONObject publicAndPrivateStockNoteListObj = new JSONObject();
		JSONArray publicStockNoteList = new JSONArray();
		JSONArray privateStockNoteList = new JSONArray();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM stockNote WHERE code = ? AND isAdmin = ? ORDER BY dateTime DESC";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			pstmt.setInt(2, 1);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				JSONObject stockNote = new JSONObject();
				stockNote.put("id", rs.getInt("id"));
				stockNote.put("title", rs.getString("title"));
				stockNote.put("isURL", rs.getBoolean("isURL"));
				stockNote.put("contents", rs.getString("contents"));
				stockNote.put("dateTime", rs.getString("dateTime"));

				publicStockNoteList.put(stockNote);
			}

			publicAndPrivateStockNoteListObj.put("publicStockNoteList", publicStockNoteList);
			publicAndPrivateStockNoteListObj.put("privateStockNoteList", privateStockNoteList);

		} catch (SQLException e) {
			throw new FailSelectException("검색 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return publicAndPrivateStockNoteListObj;
	}

	public int insertStockNote(int userId, String code, String title, String contents)
			throws FailInsertException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int noteId;

		try {
			LocalDateTime dateTime = LocalDateTime.now();

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = " INSERT INTO stockNote(`uid`,`code`,`title`,`contents`,`dateTime`,`isAdmin`) VALUES(?, ?, ?, ?, ?, ?) ";

			pstmt = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, userId);
			pstmt.setString(2, code);
			if (title == null)
				pstmt.setNull(3, Types.VARCHAR);
			else
				pstmt.setString(3, title);

			if (contents == null)
				pstmt.setNull(4, Types.VARCHAR);
			else
				pstmt.setString(4, contents);

			pstmt.setString(5, dateTime.toString());
			pstmt.setInt(6, 1);
			pstmt.executeUpdate();

			rs = pstmt.getGeneratedKeys();
			rs.next();

			noteId = rs.getInt(1);

			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("종목 노트 삽입 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return noteId;
	}

	public boolean deleteStockNote(int stockNoteId) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "DELETE FROM `stockNote` WHERE id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, stockNoteId);
			pstmt.executeUpdate();

			result = true;
		} catch (SQLException e) {
			throw new FailDeleteException("종목 노트 삭제 실패");
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}
		return result;
	}

	public boolean updateStockNote(int stockNoteId, String title, String contents) throws FailUpdateException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "UPDATE `stockNote` SET `stockNote`.title = ?, `stockNote`.contents = ? WHERE id = ?";
			pstmt = conn.prepareStatement(sql);
			if (title == null)
				pstmt.setNull(1, Types.VARCHAR);
			else
				pstmt.setString(1, title);

			if (contents == null)
				pstmt.setNull(2, Types.VARCHAR);
			else
				pstmt.setString(2, contents);
			pstmt.setInt(3, stockNoteId);

			pstmt.executeUpdate();

			result = true;
		} catch (SQLException e) {
			throw new FailUpdateException("종목 노트 업데이트 실패");
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}
		return result;
	}

}