package dao;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.DuplicateNameException;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import exception.v1.FailUpdateException;
import io.umehara.ogmapper.DefaultOgMapper;
import io.umehara.ogmapper.domain.OgTags;
import io.umehara.ogmapper.jsoup.JsoupOgMapperFactory;
import util.CONFIG_VALUE;
import util.CloserUtil;
import util.DBManager;

public class StockNoteDao {
	CONFIG_VALUE values;

	public StockNoteDao() {

	}

	public StockNoteDao(CONFIG_VALUE values) {
		this.values = values;
	}

	public JSONObject getStockNoteAmount(int userId, String code) throws FailSelectException {
		JSONObject publicAndPrivateStockNoteAmountObj = new JSONObject();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT count(*) amount FROM stockNote WHERE uid = ? AND code = ? AND isAdmin = 0";

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setString(2, code);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				publicAndPrivateStockNoteAmountObj.put("privateAmount", rs.getInt("amount"));
			} else {
				publicAndPrivateStockNoteAmountObj.put("privateAmount", 0);
			}

			CloserUtil.closeable(rs, pstmt);

			sql = " SELECT count(*) amount FROM stockNote WHERE isAdmin = 1 AND code = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				publicAndPrivateStockNoteAmountObj.put("publicAmount", rs.getInt("amount"));
			} else {
				publicAndPrivateStockNoteAmountObj.put("publicAmount", 0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검색 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return publicAndPrivateStockNoteAmountObj;
	}

	public JSONObject getStockNoteList(int userId, String code, int offset, int size) throws FailSelectException {
		JSONObject publicAndPrivateStockNoteListObj = new JSONObject();
		JSONArray publicStockNoteList = new JSONArray();
		JSONArray privateStockNoteList = new JSONArray();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM stockNote WHERE uid = ? AND code = ? AND isAdmin = 0 ORDER BY dateTime DESC Limit ?, ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setString(2, code);
			pstmt.setInt(3, offset);
			pstmt.setInt(4, size);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				JSONObject stockNote = new JSONObject();
				stockNote.put("id", rs.getInt("id"));
				stockNote.put("title", rs.getString("title"));
				stockNote.put("isURL", rs.getBoolean("isURL"));
				stockNote.put("contents", rs.getString("contents"));
				stockNote.put("dateTime", rs.getString("dateTime"));

				privateStockNoteList.put(stockNote);
			}

			CloserUtil.closeable(rs, pstmt);
			if (offset == 0) {

				sql = " SELECT * FROM stockNote WHERE isAdmin = 1 AND code = ? ORDER BY dateTime";

				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, code);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					JSONObject stockNote = new JSONObject();
					stockNote.put("id", rs.getInt("id"));
					stockNote.put("title", rs.getString("title"));
					stockNote.put("isURL", rs.getBoolean("isURL"));
					stockNote.put("contents", rs.getString("contents"));
					stockNote.put("dateTime", rs.getString("dateTime"));

					publicStockNoteList.put(stockNote);
				}
				
				publicAndPrivateStockNoteListObj.put("publicStockNoteList", publicStockNoteList);
			}
			
			publicAndPrivateStockNoteListObj.put("privateStockNoteList", privateStockNoteList);

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검색 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return publicAndPrivateStockNoteListObj;
	}

	public int insertStockNote(int userId, String code, String title, String contents, String type)
			throws FailInsertException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int noteId;

		try {
			LocalDateTime dateTime = LocalDateTime.now();

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = " INSERT INTO stockNote(`uid`,`code`,`title`,`contents`,`dateTime`, `isURL`) VALUES(?, ?, ?, ?, ?, ?) ";

			pstmt = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, userId);
			pstmt.setString(2, code);
			if (title == null)
				pstmt.setNull(3, Types.VARCHAR);
			else
				pstmt.setString(3, title);

			if (contents == null)
				pstmt.setNull(4, Types.VARCHAR);
			else
				pstmt.setString(4, contents);

			pstmt.setString(5, dateTime.toString());
			if (type.equals("memo")) {
				pstmt.setInt(6, 0);
			} else {
				pstmt.setInt(6, 1);
			}
			pstmt.executeUpdate();

			rs = pstmt.getGeneratedKeys();
			rs.next();

			noteId = rs.getInt(1);

			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("종목 노트 삽입 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return noteId;
	}

	public boolean deleteStockNote(int stockNoteId, int userId) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "DELETE FROM `stockNote` WHERE id = ? AND uid = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, stockNoteId);
			pstmt.setInt(2, userId);
			pstmt.executeUpdate();

			result = true;
		} catch (SQLException e) {
			throw new FailDeleteException("종목 노트 삭제 실패");
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}
		return result;
	}

	public boolean updateStockNote(int stockNoteId, int userId, String code, String title, String contents)
			throws FailUpdateException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "UPDATE `stockNote` SET `stockNote`.title = ?, `stockNote`.contents = ? WHERE id = ? AND uid = ? AND code = ?";
			pstmt = conn.prepareStatement(sql);
			if (title == null)
				pstmt.setNull(1, Types.VARCHAR);
			else
				pstmt.setString(1, title);

			if (contents == null)
				pstmt.setNull(2, Types.VARCHAR);
			else
				pstmt.setString(2, contents);
			pstmt.setInt(3, stockNoteId);
			pstmt.setInt(4, userId);
			pstmt.setString(5, code);

			pstmt.executeUpdate();

			result = true;
		} catch (SQLException e) {
			throw new FailUpdateException("종목 노트 삭제 실패");
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}
		return result;
	}

}