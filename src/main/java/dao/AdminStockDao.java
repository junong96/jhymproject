package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.BadRequestException;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.DuplicateNameException;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import util.CONFIG_VALUE;
import util.CloserUtil;
import util.DBManager;

public class AdminStockDao {
	CONFIG_VALUE values;

	public AdminStockDao() {

	}

	public AdminStockDao(CONFIG_VALUE values) {
		this.values = values;
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 키워드를 삭제시킨다..
	 * @param stockArr
	 * @param keywordArr
	 * @return
	 * @throws FailDeleteException
	 */
	public boolean DeleteKeywordAddRequest(String code, JSONArray keywordArr) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			StringBuffer codeValueStr = new StringBuffer();
			StringBuffer keywordValueStr = new StringBuffer();
			
			codeValueStr.append("(\'").append(code).append("\')");
			
			keywordValueStr.append("(");
			for (int i = 0; i < keywordArr.length(); i++) {
				String keyword = keywordArr.getString(i);
				keywordValueStr.append("\'").append(keyword).append("\',");
			}
			
			keywordValueStr.deleteCharAt(keywordValueStr.length() - 1);
			keywordValueStr.append(")");
			

			String sql = "DELETE FROM KeywordAddRequest WHERE code IN " + codeValueStr.toString() + " AND keyword IN "
					+ keywordValueStr.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			
			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailDeleteException("키워드 요청 삭제 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	public JSONArray getKeywordAddRequest(String code) throws FailSelectException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray keywordArr = new JSONArray();
		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "SELECT DISTINCT keyword FROM KeywordAddRequest WHERE code = ? ";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				keywordArr.put(rs.getString("keyword"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("키워드 요청 조회 실패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return keywordArr;
	}
	
}
