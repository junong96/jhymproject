package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.BadRequestException;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import util.CONFIG_VALUE;
import util.CloserUtil;
import util.DBManager;

public class StockDao {
	CONFIG_VALUE values;

	public StockDao() {

	}

	public StockDao(CONFIG_VALUE values) {
		this.values = values;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @param keywords
	 * @param searchWord
	 * @param logic
	 * @param type
	 * @param size
	 * @param offset
	 * @since :2021. 4. 20.
	 * @description :
	 * @return
	 * @throws FailSelectException
	 */
	public JSONObject getStockAmount(String type, String logic, String searchWord, String keywords)
			throws FailSelectException {
		JSONObject stockList = new JSONObject(); // 반환 ??종목 리스??
		Connection conn = null;
		//PreparedStatement pstmt = null;
		//
		ResultSet rs = null;
		JSONArray searchKeywordArr = null; // 검?�한 ?�워??배열
		String searchStockNameWhere = ""; // ?�름?�로 검?�시 where 조건문에 ?�어��??�트��?
		boolean isNoneSearchWord = false; //
		String sql = null;
		StringBuffer stockCodesSb = new StringBuffer();

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			if (type.equals("keyword")) {
				StringBuffer keywordIDsSb = new StringBuffer();
				if (keywords != null && keywords.length() != 0) {
					searchKeywordArr = new JSONArray(keywords);
					keywordIDsSb.append("(");
					for (int i = 0; i < searchKeywordArr.length(); i++) {
						keywordIDsSb.append("\'").append(searchKeywordArr.getInt(i)).append("\',");
					}
					keywordIDsSb.deleteCharAt(keywordIDsSb.length() - 1);
					keywordIDsSb.append(")");
				}
//
				if (logic.equals("and")) {
					if (keywords != null && keywords.length() != 0) {
						sql = "SELECT count(*) amount, name, code FROM (SELECT `stockKeywordMapping`.KID,`Stock`.code, `Stock`.name FROM Stock JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.CODE WHERE `stockKeywordMapping`.KID IN "
								+ keywordIDsSb.toString() + ") temp GROUP BY `Stock`.name HAVING amount = "
								+ searchKeywordArr.length();
					} else {
						isNoneSearchWord = true;
					}
				} // end of if
				else { // logic ??or ??경우
					if (keywords != null && keywords.length() != 0) {
						sql = "SELECT DISTINCT `Stock`.code FROM Stock JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.CODE WHERE `stockKeywordMapping`.KID IN "
								+ keywordIDsSb.toString();
					} else {
						isNoneSearchWord = true;
					}
				} // end of else
			} else { // type ??stock
				if (logic.equals("and"))
					throw new BadRequestException("검???�션 ?�러");
				if (searchWord != null && searchWord.length() != 0) {
					searchStockNameWhere = "WHERE `Stock`.name LIKE \'%" + searchWord + "%\'";
				}
			}

			HashSet<String> stockCodeFilterSet = new HashSet<String>();
			// ?�워??검??조건??충족?�는 종목 코드가 ?�기???�이?? 조건??충족?�는 종목???�을 경우 ?�이즈는 0

			// ?�?�이 ?�워?�이��??�워?��? 1��??�상?�라???�함?�여 검???�을 경우
			if (!isNoneSearchWord && type.equals("keyword")) {
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					stockCodeFilterSet.add(rs.getString("code"));
				}

				Iterator<String> stockCodeIt = stockCodeFilterSet.iterator();
				if (stockCodeFilterSet.size() != 0) {
					stockCodesSb.append("(");
					while (stockCodeIt.hasNext()) {
						stockCodesSb.append("\'").append(stockCodeIt.next()).append("\',");
					}
					stockCodesSb.deleteCharAt(stockCodesSb.length() - 1);
					stockCodesSb.append(")");
				} else {
					// TODO WORK
					// 조건??맞는 종목???�을 경우???�떻��??�현?�건지 ?�해????
				}
			}
			CloserUtil.closeable(rs, pstmt);

			sql = " SELECT * FROM Stock LEFT JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.code "
					+ " LEFT JOIN keyword ON stockKeywordMapping.KID = keyword.ID " + searchStockNameWhere
					+ (stockCodeFilterSet.size() == 0 ? ""
							: (searchStockNameWhere.equals("") ? "WHERE `Stock`.code IN " + stockCodesSb.toString()
									: " AND `Stock`.code IN " + stockCodesSb.toString()));
			// TODO WORK 조건??맞는 결과가 ?�을???�무 리스??출력?��? ?�으?�면 ?�에 ��?? 조건��??�거
			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();

			while (rs.next()) {
				JSONArray keywordArr;
				String code = rs.getString("code");
				if (stockCodeSet.contains(code)) {
					JSONObject stockObj = stockList.getJSONObject(code);
					keywordArr = stockObj.getJSONArray("keywordArr");
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
				} else {
					JSONObject stockObj = new JSONObject();
					stockObj.put("code", code);
					stockObj.put("name", rs.getString("name"));
					keywordArr = new JSONArray();
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
					stockObj.put("keywordArr", keywordArr);
					stockList.put(code, stockObj);
				}
				stockCodeSet.add(code);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockList;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @param keywords
	 * @param searchWord
	 * @param logic
	 * @param type
	 * @param size
	 * @param offset
	 * @since :2021. 4. 20.
	 * @description :
	 * @return
	 * @throws FailSelectException
	 */
	public JSONObject getStockList(String type, String logic, String searchWord, String keywords)
			throws FailSelectException {
		JSONObject stockList = new JSONObject(); // 반환 ??종목 리스??
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray searchKeywordArr = new JSONArray(); // 검?�한 ?�워???�이??배열
		JSONArray searchNameArr = null; // 검?�한 ?�워???�름 배열
		String searchStockNameWhere = ""; // ?�름?�로 검?�시 where 조건문에 ?�어��??�트��?
		boolean isNoneSearchWord = false; //
		String sql = null;
		StringBuffer stockCodesSb = new StringBuffer();
		stockCodesSb.append("(").append("\'-1\'").append(")");

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			if (type.equals("keyword")) {
				StringBuffer keywordNamesSb = new StringBuffer();
				if (keywords != null && keywords.length() != 0) {
					searchNameArr = new JSONArray(keywords);
					keywordNamesSb.append("(");
					for (int i = 0; i < searchNameArr.length(); i++) {
						keywordNamesSb.append("\'").append(searchNameArr.getString(i)).append("\',");
					}
					keywordNamesSb.deleteCharAt(keywordNamesSb.length() - 1);
					keywordNamesSb.append(")");

					sql = "SELECT * FROM keyword WHERE keyword IN " + keywordNamesSb.toString();

					pstmt = conn.prepareStatement(sql);
					rs = pstmt.executeQuery();

					while (rs.next()) {
						searchKeywordArr.put(rs.getInt("ID"));
					}

					CloserUtil.closeable(rs, pstmt);
				}

				StringBuffer keywordIDsSb = new StringBuffer();
				if (searchKeywordArr != null && searchKeywordArr.length() != 0) {
					keywordIDsSb.append("(");
					for (int i = 0; i < searchKeywordArr.length(); i++) {
						keywordIDsSb.append("\'").append(searchKeywordArr.getInt(i)).append("\',");
					}
					keywordIDsSb.deleteCharAt(keywordIDsSb.length() - 1);
					keywordIDsSb.append(")");
				}

				if (logic.equals("and")) {
					if (searchKeywordArr != null && searchKeywordArr.length() != 0) {
						sql = "SELECT count(*) amount, name, code FROM (SELECT `stockKeywordMapping`.KID,`Stock`.code, `Stock`.name FROM Stock JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.CODE WHERE `stockKeywordMapping`.KID IN "
								+ keywordIDsSb.toString() + ") temp GROUP BY name HAVING amount = "
								+ searchNameArr.length();
					} else {
						isNoneSearchWord = true;
					}
				} // end of if
				else { // logic ??or ??경우
					if (searchKeywordArr != null && searchKeywordArr.length() != 0) {
						sql = "SELECT DISTINCT `Stock`.code FROM Stock JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.CODE WHERE `stockKeywordMapping`.KID IN "
								+ keywordIDsSb.toString();
					} else {
						isNoneSearchWord = true;
					}
				} // end of else
			} else { // type ??stock
				if (logic.equals("and"))
					throw new BadRequestException("검???�션 ?�러");
				if (searchWord != null && searchWord.length() != 0) {
					searchStockNameWhere = "WHERE `Stock`.name LIKE \'%" + searchWord + "%\'";
				}
			}
			HashSet<String> stockCodeFilterSet = new HashSet<String>();
			// ?�워??검??조건??충족?�는 종목 코드가 ?�기???�이?? 조건??충족?�는 종목???�을 경우 ?�이즈는 0

			// ?�?�이 ?�워?�이��??�워?��? 1��??�상?�라???�함?�여 검???�을 경우
			if (!isNoneSearchWord && type.equals("keyword")) {
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					stockCodeFilterSet.add(rs.getString("code"));
				}

				Iterator<String> stockCodeIt = stockCodeFilterSet.iterator();
				if (stockCodeFilterSet.size() != 0) {
					stockCodesSb.delete(0, stockCodesSb.length());
					stockCodesSb.append("(");
					while (stockCodeIt.hasNext()) {
						stockCodesSb.append("\'").append(stockCodeIt.next()).append("\',");
					}
					stockCodesSb.deleteCharAt(stockCodesSb.length() - 1);
					stockCodesSb.append(")");
				}

				CloserUtil.closeable(rs, pstmt);
			}

			if (type.equals("keyword") && isNoneSearchWord && keywords == null) {
				stockCodesSb.delete(0, stockCodesSb.length());
			}

			if (type.equals("stock") && (searchWord == null || searchWord.length() == 0)) {
				stockCodesSb.delete(0, stockCodesSb.length());
			}

			sql = " SELECT * FROM Stock LEFT JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.code "
					+ " LEFT JOIN keyword ON stockKeywordMapping.KID = keyword.ID " + searchStockNameWhere
					+ (searchStockNameWhere.equals("") ? stockCodesSb.toString().equals("") ? ""
							: "WHERE `Stock`.code IN " + stockCodesSb.toString() : "");
			// TODO WORK 조건??맞는 결과가 ?�을???�무 리스??출력?��? ?�으?�면 ?�에 ��?? 조건��??�거
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();

			while (rs.next()) {
				JSONArray keywordArr;
				String code = rs.getString("code");
				if (stockCodeSet.contains(code)) {
					JSONObject stockObj = stockList.getJSONObject(code);
					keywordArr = stockObj.getJSONArray("keywordArr");
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
				} else {
					JSONObject stockObj = new JSONObject();
					stockObj.put("code", code);
					stockObj.put("name", rs.getString("name"));
					keywordArr = new JSONArray();
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
					stockObj.put("keywordArr", keywordArr);
					stockList.put(code, stockObj);
				}
				stockCodeSet.add(code);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockList;
	}

	public JSONObject getStockAndKeywordList() throws FailSelectException {
		JSONObject stockAndKeywordList = new JSONObject();
		JSONArray stockList = new JSONArray();
		JSONArray keywordList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM Stock";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			while (rs.next()) {
				stockList.put(rs.getString("name"));
			}

			stockAndKeywordList.put("stockList", stockList);

			CloserUtil.closeable(rs, pstmt);

			sql = " SELECT * FROM keyword";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			while (rs.next()) {
				keywordList.put(rs.getString("keyword"));
			}

			stockAndKeywordList.put("keywordList", keywordList);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockAndKeywordList;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�목��??�목???�워??��???��??가?�온?? 관리자 ?�이지 ?�용 모든종목 가?�옴
	 * @return
	 * @throws FailSelectException
	 */
	public JSONObject getAllStockList() throws FailSelectException {
		JSONObject stockList = new JSONObject();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM Stock LEFT JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.code "
					+ " LEFT JOIN keyword ON stockKeywordMapping.KID = keyword.ID";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();

			while (rs.next()) {
				JSONArray keywordArr;
				String code = rs.getString("code");
				if (stockCodeSet.contains(code)) {
					JSONObject stockObj = stockList.getJSONObject(code);
					keywordArr = stockObj.getJSONArray("keywordArr");
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
				} else {
					JSONObject stockObj = new JSONObject();
					stockObj.put("code", code);
					stockObj.put("name", rs.getString("name"));
					keywordArr = new JSONArray();
					if (rs.getString("keyword") != null) {
						JSONObject keywordObj = new JSONObject();
						keywordObj.put("KID", rs.getString("KID"));
						keywordObj.put("keyword", rs.getString("keyword"));
						keywordArr.put(keywordObj);
					}
					stockObj.put("keywordArr", keywordArr);
					stockList.put(code, stockObj);
				}
				stockCodeSet.add(code);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockList;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 모든 ?�목��?가?�옴
	 * @return
	 * @throws FailSelectException
	 */
	public JSONArray getOnlyStockList() throws FailSelectException {
		JSONArray stockList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM Stock";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			while (rs.next()) {
				JSONObject stockObj = new JSONObject();
				stockObj.put("code", rs.getString("code"));
				stockObj.put("name", rs.getString("name"));
				stockObj.put("sectors", rs.getString("sectors"));
				stockObj.put("product", rs.getString("product"));
				stockObj.put("type", rs.getString("type"));
				stockObj.put("marketCapital", rs.getLong("marketCapital"));
				stockObj.put("listedShares", rs.getLong("listedShares"));
				stockList.put(stockObj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockList;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�목��??�목???�워??��???��??가?�온?? 관리자 ?�이지 ?�용 모든종목 가?�옴
	 * @return
	 * @throws FailSelectException
	 */
	public JSONObject getAllKeywordList() throws FailSelectException {
		JSONObject keywordList = new JSONObject();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT `keyword`.ID KID, `keyword`.keyword, `Stock`.CODE, `Stock`.name "
					+ " FROM keyword LEFT JOIN stockKeywordMapping ON stockKeywordMapping.KID = keyword.ID LEFT "
					+ " JOIN Stock ON Stock.code = stockKeywordMapping.code ";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> keywordIDSet = new HashSet<String>();

			while (rs.next()) {
				JSONArray stockArr;
				String kid = rs.getString("KID");
				if (keywordIDSet.contains(kid)) {
					JSONObject keywordObj = keywordList.getJSONObject(kid);
					stockArr = keywordObj.getJSONArray("stockArr");
					if (rs.getString("name") != null) {
						JSONObject stockObj = new JSONObject();
						stockObj.put("code", rs.getString("code"));
						stockObj.put("name", rs.getString("name"));
						stockArr.put(stockObj);
					}
				} else {
					JSONObject keywordObj = new JSONObject();
					keywordObj.put("kid", kid);
					keywordObj.put("keyword", rs.getString("keyword"));
					stockArr = new JSONArray();

					if (rs.getString("name") != null) {
						JSONObject stockObj = new JSONObject();
						stockObj.put("code", rs.getString("code"));
						stockObj.put("name", rs.getString("name"));
						stockArr.put(stockObj);
					}

					keywordObj.put("stockArr", stockArr);
					keywordList.put(kid, keywordObj);
				}
				keywordIDSet.add(kid);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return keywordList;
	}

	public JSONObject getTodayKeywordList(LocalDate localDate) throws FailSelectException {
		JSONObject topKeywordAndKeywordDataListObj = new JSONObject();
		JSONObject topKeywordList = new JSONObject();
		JSONArray todayKeywordDataList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM `todayKeyword` JOIN `Stock` ON `todayKeyword`.name = `Stock`.name WHERE `todayKeyword`.trade_date = ? ORDER BY updown_rate DESC";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();
			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();
			while (rs.next()) {
				stockCodeSet.add(rs.getString("code"));
				JSONObject todayKeywordData = new JSONObject();
				todayKeywordData.put("name", rs.getString("name"));
				todayKeywordData.put("code", rs.getString("code"));
				todayKeywordData.put("curPrice", rs.getInt("cur_price"));
				todayKeywordData.put("updownPrice", rs.getInt("updown_price"));
				todayKeywordData.put("updownRate", rs.getDouble("updown_rate"));
				todayKeywordData.put("volume", rs.getInt("volume"));
				todayKeywordData.put("tradeDate", rs.getString("trade_date"));

				todayKeywordDataList.put(todayKeywordData);
			}

			topKeywordAndKeywordDataListObj.put("todayKeywordDataList", todayKeywordDataList);
			CloserUtil.closeable(rs, pstmt);
			if (stockCodeSet.size() != 0) {
				Iterator<String> codeIt = stockCodeSet.iterator();
				StringBuffer stockCodeSb = new StringBuffer();
				stockCodeSb.append("(");
				while (codeIt.hasNext()) {
					stockCodeSb.append("\"").append(codeIt.next()).append("\",");
				}
				stockCodeSb.deleteCharAt(stockCodeSb.length() - 1);
				stockCodeSb.append(")");
				sql = "SELECT COUNT(*) as amount, keyword, ID, sum(updown_rate) as sumRate FROM `keyword` JOIN `stockKeywordMapping` ON `keyword`.ID = `stockKeywordMapping`.KID JOIN Stock ON stockKeywordMapping.CODE = Stock.code JOIN todayKeyword ON Stock.name = todayKeyword.name WHERE `stockKeywordMapping`.CODE IN "
						+ stockCodeSb.toString()
						+ " AND trade_date = ? GROUP BY `keyword`.ID ORDER BY amount DESC, sumRate DESC";

				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, localDate.toString());
				rs = pstmt.executeQuery();

				while (rs.next()) {
					JSONObject keywordObj = new JSONObject();
					keywordObj.put("amount", rs.getInt("amount"));
					keywordObj.put("id", rs.getInt("ID"));
					keywordObj.put("keyword", rs.getString("keyword"));
					keywordObj.put("sumRate", rs.getDouble("sumRate"));
					topKeywordList.put(String.valueOf(rs.getInt("ID")), keywordObj);
				}
			}

			topKeywordAndKeywordDataListObj.put("topKeywordList", topKeywordList);
			topKeywordAndKeywordDataListObj.put("stockCodeSet", stockCodeSet);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("todayKeyword 검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return topKeywordAndKeywordDataListObj;
	}

	public JSONObject getOvertimeKeywordList(LocalDate localDate) throws FailSelectException {
		JSONObject topKeywordAndKeywordDataListObj = new JSONObject();
		JSONObject topKeywordList = new JSONObject();
		JSONArray overtimeKeywordDataList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM `overtimeKeyword` JOIN `Stock` ON `overtimeKeyword`.name = `Stock`.name WHERE `overtimeKeyword`.trade_date = ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();

			while (rs.next()) {
				stockCodeSet.add(rs.getString("code"));
				JSONObject overTimeObj = new JSONObject();
				overTimeObj.put("name", rs.getString("name"));
				overTimeObj.put("code", rs.getString("code"));
				overTimeObj.put("curPrice", rs.getInt("cur_price"));
				overTimeObj.put("updownPrice", rs.getInt("updown_price"));
				overTimeObj.put("updownRate", rs.getDouble("updown_rate"));
				overTimeObj.put("volume", rs.getInt("volume"));
				overTimeObj.put("tradeDate", rs.getString("trade_date"));

				overtimeKeywordDataList.put(overTimeObj);
			}

			topKeywordAndKeywordDataListObj.put("overtimeKeywordDataList", overtimeKeywordDataList);
			CloserUtil.closeable(rs, pstmt);

			if (stockCodeSet.size() != 0) {
				Iterator<String> codeIt = stockCodeSet.iterator();

				StringBuffer stockCodeSb = new StringBuffer();
				stockCodeSb.append("(");
				while (codeIt.hasNext()) {
					stockCodeSb.append("\"").append(codeIt.next()).append("\",");
				}
				stockCodeSb.deleteCharAt(stockCodeSb.length() - 1);
				stockCodeSb.append(")");

				sql = "SELECT COUNT(*) as amount, keyword, ID, sum(updown_rate) as sumRate FROM `keyword` JOIN `stockKeywordMapping` ON `keyword`.ID = `stockKeywordMapping`.KID JOIN Stock ON stockKeywordMapping.CODE = Stock.code JOIN overtimeKeyword ON Stock.name = overtimeKeyword.name WHERE `stockKeywordMapping`.CODE IN "
						+ stockCodeSb.toString()
						+ " AND trade_date = ? GROUP BY `keyword`.ID ORDER BY amount DESC, sumRate DESC";

				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, localDate.toString());
				rs = pstmt.executeQuery();

				while (rs.next()) {
					JSONObject keywordObj = new JSONObject();
					keywordObj.put("amount", rs.getInt("amount"));
					keywordObj.put("id", rs.getInt("ID"));
					keywordObj.put("keyword", rs.getString("keyword"));
					keywordObj.put("sumRate", rs.getDouble("sumRate"));
					topKeywordList.put(String.valueOf(rs.getInt("ID")), keywordObj);
				}
			}
			topKeywordAndKeywordDataListObj.put("topKeywordList", topKeywordList);
			topKeywordAndKeywordDataListObj.put("stockCodeSet", stockCodeSet);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return topKeywordAndKeywordDataListObj;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�워?��? 받아???�워???�이블에 ?�로 ?�록????주식 종목?�랑 ??��준??
	 * @param stockArr
	 * @param keywordArr
	 * @return
	 * @throws FailInsertException
	 */
	public boolean insertKeywordAndMapping(JSONArray stockArr, JSONArray keywordArr) throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			StringBuffer keywordSb = new StringBuffer(); // ?�입???�트��?버퍼

			StringBuffer selectKeywordSb = new StringBuffer(); // 검?�용 ?�트��?버퍼

			// ?�워??문자?�을 쿼리 ?�입문으��?변경하??부��?
			// 받�? ?�워???�이즈�? 0 ?�라��?false 리턴
			if (keywordArr.length() > 0) {
				selectKeywordSb.append("(");
				for (int i = 0; i < keywordArr.length(); i++) {
					keywordSb.append("(\"").append(keywordArr.get(i)).append("\"),");
					selectKeywordSb.append("\"").append(keywordArr.get(i)).append("\"").append(",");
				}
				selectKeywordSb.deleteCharAt(selectKeywordSb.length() - 1);
				selectKeywordSb.append(")");
			} else {
				return false;
			}

			keywordSb.deleteCharAt(keywordSb.length() - 1);

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = "INSERT IGNORE INTO keyword(keyword) VALUES " + keywordSb.toString();

			pstmt = conn.prepareStatement(sql);

			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);
			sql = "SELECT * FROM keyword WHERE keyword IN " + selectKeywordSb.toString();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			List<Integer> keywordIdList = new ArrayList<Integer>();
			while (rs.next()) {
				keywordIdList.add(rs.getInt("ID"));
			}

			CloserUtil.closeable(rs, pstmt);

			StringBuffer mappingValuesStr = new StringBuffer(); // 검?�용 ?�트��?버퍼

			StringBuffer codeValueStr = new StringBuffer();
			StringBuffer keywordValueStr = new StringBuffer();

			codeValueStr.append("(");
			for (int i = 0; i < stockArr.length(); i++) {
				String stockCode = stockArr.getString(i);
				codeValueStr.append("\'").append(stockCode).append("\',");
				for (int j = 0; j < keywordIdList.size(); j++) {
					mappingValuesStr.append("(\'").append(stockCode).append("\',").append(keywordIdList.get(j))
							.append("),");
				}
			}
			mappingValuesStr.deleteCharAt(mappingValuesStr.length() - 1);

			codeValueStr.deleteCharAt(codeValueStr.length() - 1);
			codeValueStr.append(")");

			keywordValueStr.append("(");
			for (int i = 0; i < keywordArr.length(); i++) {
				String keyword = keywordArr.getString(i);
				keywordValueStr.append("\'").append(keyword).append("\',");
			}
			keywordValueStr.deleteCharAt(keywordValueStr.length() - 1);
			keywordValueStr.append(")");

			sql = "INSERT IGNORE INTO stockKeywordMapping(`CODE`,`KID`) VALUES " + mappingValuesStr.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			sql = "DELETE FROM KeywordAddRequest WHERE 	code IN " + codeValueStr.toString() + " AND keyword IN "
					+ keywordValueStr.toString();

			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�워???�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�워?�는 그�?��??��????�태?�서 주식 종목��???? ?�보��???��?�킨??
	 * @param stockArr
	 * @param keywordArr
	 * @return
	 * @throws FailDeleteException
	 */
	public boolean DeleteKeywordMapping(JSONArray stockArr, JSONArray keywordArr) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			StringBuffer selectStockSb = new StringBuffer();
			StringBuffer selectKeywordSb = new StringBuffer();

			if (stockArr.length() > 0) {
				selectStockSb.append("(");
				for (int i = 0; i < stockArr.length(); i++) {
					selectStockSb.append("\"").append(stockArr.get(i)).append("\"").append(",");
				}
				selectStockSb.deleteCharAt(selectStockSb.length() - 1);
				selectStockSb.append(")");
			} else {
				return false;
			}

			if (keywordArr.length() > 0) {
				selectKeywordSb.append("(");
				for (int i = 0; i < keywordArr.length(); i++) {
					selectKeywordSb.append("\"").append(keywordArr.get(i)).append("\"").append(",");
				}
				selectKeywordSb.deleteCharAt(selectKeywordSb.length() - 1);
				selectKeywordSb.append(")");
			} else {
				return false;
			}

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = "DELETE FROM stockKeywordMapping WHERE CODE IN " + selectStockSb.toString() + " AND KID IN "
					+ selectKeywordSb.toString();
			pstmt = conn.prepareStatement(sql);

			pstmt.executeUpdate();

			conn.commit();
			result = true;
		} catch (SQLException e) {
			throw new FailDeleteException("?�워??매핑 ??�� ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�워?��? ??��?�킨??.
	 * @param stockArr
	 * @param keywordArr
	 * @return
	 * @throws FailDeleteException
	 */
	public boolean DeleteKeyword(JSONArray keywordArr) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			StringBuffer selectKeywordSb = new StringBuffer(); // 검?�용 ?�트��?버퍼

			if (keywordArr.length() > 0) {
				selectKeywordSb.append("(");
				for (int i = 0; i < keywordArr.length(); i++) {
					selectKeywordSb.append("\"").append(keywordArr.get(i)).append("\"").append(",");
				}
				selectKeywordSb.deleteCharAt(selectKeywordSb.length() - 1);
				selectKeywordSb.append(")");
			} else {
				return false;
			}

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = "DELETE FROM keyword WHERE ID IN " + selectKeywordSb.toString();
			pstmt = conn.prepareStatement(sql);

			pstmt.executeUpdate();

			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailDeleteException("?�워????�� ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : tsv ?�일로�????�어?�인 주식 종목 ?�보��??�비???�록?�다.
	 * @param stockList
	 * @return
	 * @throws FailInsertException
	 */
	public boolean insertStock(JSONArray stockList) throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			StringBuffer insertValues = new StringBuffer();

			for (int i = 0; i < stockList.length(); i++) {
				JSONArray stockInfoArr = stockList.getJSONArray(i);
				boolean isFirst = true;
				String value = "(";
				for (int j = 0; j < stockInfoArr.length(); j++) {
					value += (isFirst ? "" : ",") + "\"" + stockInfoArr.getString(j) + "\"";
					isFirst = false;
				}
				value += ")";
				if (i != 0)
					insertValues.append(",");
				insertValues.append(value);
			}
			String sql = "INSERT IGNORE INTO Stock(`name`,`code`,`sectors`,`product`,`type`) VALUES "
					+ insertValues.toString();

			pstmt = conn.prepareStatement(sql);
			result = pstmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�목 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 주식 매매 ?�보��??�비???�록
	 * @param investorList
	 * @param localDate
	 * @return
	 * @throws FailInsertException
	 */
	public boolean insertInvestorAmount(JSONObject resultObj, LocalDate localDate) throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray investorList = resultObj.getJSONArray("investorList");
		JSONArray investorListedSharesList = resultObj.getJSONArray("investorListedSharesList");

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);

			String deleteSql = "DELETE FROM InvestorAmount where trade_date = ?";
			pstmt = conn.prepareStatement(deleteSql);
			pstmt.setString(1, localDate.toString());
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			StringBuffer insertValues = new StringBuffer();

			for (int i = 0; i < investorList.length(); i++) {
				JSONArray investorInfoArr = investorList.getJSONArray(i);
				boolean isFirst = true;
				String value = "(";
				for (int j = 0; j < investorInfoArr.length(); j++) {
					value += (isFirst ? "" : ",") + "\"" + investorInfoArr.getString(j) + "\"";
					isFirst = false;
				}
				value += ",\"" + localDate.toString() + "\")";
				if (i != 0)
					insertValues.append(",");
				insertValues.append(value);
			}
			String sql = "INSERT IGNORE INTO InvestorAmount(`name`,`cur_price`,`updown_price`,`updown_rate`,`amount`,`person`,`foreigner`,`org`,`kumto`,`bohum`,`tosin`,`samo`,`bank`,`etc_fi`,`pension`,`nation`,`etc_corp`,`etc_for`, `trade_date`) VALUES "
					+ insertValues.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			ArrayList<String> InvestorNameArr = new ArrayList<String>();
			ArrayList<String> noneExistsStockkNameArr = new ArrayList<String>();

			sql = "SELECT name FROM InvestorAmount WHERE trade_date = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				InvestorNameArr.add(rs.getString("name"));
			}

			if (InvestorNameArr.size() != 0) {
				for (int i = 0; i < investorList.length(); i++) {
					String stockName = investorList.getJSONArray(i).getString(0);
					if (!InvestorNameArr.contains(stockName))
						noneExistsStockkNameArr.add(stockName);
				}
			}

			// 매매 ?�일??종목 ��?Stock ?�이블에 존재?��? ?�는 종목???�?�여 noneExistsStock ?�이블에 종목 ?�름 추�?
			if (noneExistsStockkNameArr.size() != 0) {
				CloserUtil.closeable(rs, pstmt);
				StringBuffer noneExistsStockNameSb = new StringBuffer();
				boolean isFirst = true;
				for (int i = 0; i < noneExistsStockkNameArr.size(); i++) {
					if (!isFirst)
						noneExistsStockNameSb.append(",");
					noneExistsStockNameSb.append("(\"").append(noneExistsStockkNameArr.get(i)).append("\")");
					isFirst = false;
				}
				sql = "INSERT IGNORE INTO noneExistsStock(`name`) VALUES " + noneExistsStockNameSb.toString();
				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}

			CloserUtil.closeable(pstmt);

			// 매매 ?�일???�장 주식 ??��?Stock ?�이블의 ?�름??같�? 종목???�데?�트
			if (InvestorNameArr.size() != 0) {
				StringBuffer existsStockNameSb = new StringBuffer();
				existsStockNameSb.append("(");
				boolean isFirst = true;
				for (int i = 0; i < InvestorNameArr.size(); i++) {
					if (!isFirst)
						existsStockNameSb.append(",");
					existsStockNameSb.append("\"").append(InvestorNameArr.get(i)).append("\"");
					isFirst = false;
				}
				existsStockNameSb.append(")");

				StringBuffer whenSqlSb = new StringBuffer();
				for (int i = 0; i < investorListedSharesList.length(); i++) {
					JSONObject listedSharesObj = investorListedSharesList.getJSONObject(i);
					whenSqlSb.append(" WHEN name = ").append("\"").append(listedSharesObj.getString("name"))
							.append("\"").append(" THEN ").append(listedSharesObj.getString("listedShares"))
							.append(" ");
				}

				sql = " UPDATE `Stock` SET `listedShares` = CASE " + whenSqlSb.toString() + " ELSE `listedShares` "
						+ " END " + " WHERE `name` IN " + existsStockNameSb.toString();

				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			// ?�량 ?�이???�력 ?�후 ?�급 ?�위 ?�이???�력
			insertSupplyNdemand(conn, investorList, localDate);

			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("매매 ?�량 ?�보 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	// ?�급 ?�착 ?�이?��? ?�입?�다.
	public boolean insertSupplyNdemand(Connection conn, JSONArray investorList, LocalDate localDate)
			throws FailInsertException {
		boolean result = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String deleteSql = "DELETE FROM `supplyNdemand` where trade_date = ?";
			pstmt = conn.prepareStatement(deleteSql);
			pstmt.setString(1, localDate.toString());
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			StringBuffer selectValue = new StringBuffer();
			StringBuffer innerValue;

			selectValue.append("(");
			boolean isFirst = true;
			for (int i = 0; i < investorList.length(); i++) {
				JSONArray investorInfoArr = investorList.getJSONArray(i);

				innerValue = new StringBuffer();
				innerValue.append("(name = \'").append(investorInfoArr.getString(0)).append("\' AND trade_date = \'")
						.append(localDate.toString()).append("\')");

				if (!isFirst)
					selectValue.append(" OR ");

				selectValue.append(innerValue.toString());
				isFirst = false;
			}
			selectValue.append(")");

			String sql = "SELECT * FROM ( SELECT name, trade_date, foreigner, org, tosin,  samo, pension, "
					+ "        ROW_NUMBER() OVER (PARTITION BY name ORDER BY foreigner DESC) AS foreignerRank, "
					+ "        ROW_NUMBER() OVER (PARTITION BY name ORDER BY org DESC) AS orgRank, "
					+ "        ROW_NUMBER() OVER (PARTITION BY name ORDER BY tosin DESC) AS tosinRank, "
					+ "        ROW_NUMBER() OVER (PARTITION BY name ORDER BY samo DESC) AS samoRank, "
					+ "        ROW_NUMBER() OVER (PARTITION BY name ORDER BY pension DESC) AS pensionRank "
					+ "    FROM InvestorAmount WHERE trade_date <= ?) T"
					+ " WHERE (foreignerRank <= 5 || orgRank <=5 || tosinRank <=5 || samoRank <=5 || pensionRank <=5) AND "
					+ selectValue.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			ArrayList<JSONObject> supplyNdemandList = new ArrayList<JSONObject>();

			while (rs.next()) {
				JSONObject supplyNdemandObj = new JSONObject();

				supplyNdemandObj.put("name", rs.getString("name"));
				supplyNdemandObj.put("trade_date", rs.getString("trade_date"));

				int foreignerRank = rs.getInt("foreignerRank");
				supplyNdemandObj.put("foreignerRank",
						(rs.getInt("foreigner") > 0 && foreignerRank <= 5) ? foreignerRank : 0);

				int orgRank = rs.getInt("orgRank");
				supplyNdemandObj.put("orgRank", (rs.getInt("org") > 0 && orgRank <= 5) ? orgRank : 0);

				int tosinRank = rs.getInt("tosinRank");
				supplyNdemandObj.put("tosinRank", (rs.getInt("tosin") > 0 && tosinRank <= 5) ? tosinRank : 0);

				int samoRank = rs.getInt("samoRank");
				supplyNdemandObj.put("samoRank", (rs.getInt("samo") > 0 && samoRank <= 5) ? samoRank : 0);

				int pensionRank = rs.getInt("pensionRank");
				supplyNdemandObj.put("pensionRank", (rs.getInt("pension") > 0 && pensionRank <= 5) ? pensionRank : 0);

				supplyNdemandList.add(supplyNdemandObj);
			}
			CloserUtil.closeable(rs, pstmt);
			if (supplyNdemandList.size() != 0) {
				StringBuffer insertValue = new StringBuffer();

				int continueIndex = 0;
				boolean isFirst2 = true;
				for (int i = 0; i < supplyNdemandList.size(); i++) {
					JSONObject supplyNdemandObj = supplyNdemandList.get(i);

					int foreignerRank = supplyNdemandObj.getInt("foreignerRank");
					int orgRank = supplyNdemandObj.getInt("orgRank");
					int tosinRank = supplyNdemandObj.getInt("tosinRank");
					int samoRank = supplyNdemandObj.getInt("samoRank");
					int pensionRank = supplyNdemandObj.getInt("pensionRank");

					// ?��? 0 ??경우??DB???�력???�요가 ?�으???�외
					if (foreignerRank == 0 && orgRank == 0 && tosinRank == 0 && samoRank == 0 && pensionRank == 0) {
						continueIndex++;
						continue;
					}

					if (!isFirst2)
						insertValue.append(", ");

					insertValue.append("(\'").append(supplyNdemandObj.getString("name")).append("\',").append("\'")
							.append(supplyNdemandObj.getString("trade_date")).append("\',").append("\'")
							.append(foreignerRank).append("\',").append("\'").append(orgRank).append("\',").append("\'")
							.append(tosinRank).append("\',").append("\'").append(samoRank).append("\',").append("\'")
							.append(pensionRank).append("\')");

					isFirst2 = false;
				}

				if (continueIndex != supplyNdemandList.size()) {
					sql = "INSERT INTO supplyNdemand(`name`, `trade_date`, `foreignerRank`, `orgRank`, `tosinRank`, `samoRank`, `pensionRank`) VALUES "
							+ insertValue.toString();
					pstmt = conn.prepareStatement(sql);
					pstmt.executeUpdate();
				}
			}

			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�급 ?�착 ?�보 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : 주식 매매 ?�보��??�비???�록
	 * @param investorList
	 * @param localDate
	 * @return
	 * @throws FailInsertException
	 * @throws FailSelectException
	 */
	public boolean insertInvestorPrice(JSONObject resultObj, LocalDate localDate)
			throws FailInsertException, FailSelectException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray investorList = resultObj.getJSONArray("investorList");
		JSONArray investorMarketCapitalList = resultObj.getJSONArray("investorMarketCapitalList");

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);

			String deleteSql = "DELETE FROM InvestorPrice where trade_date = ?";
			pstmt = conn.prepareStatement(deleteSql);
			pstmt.setString(1, localDate.toString());
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			StringBuffer insertValues = new StringBuffer();

			for (int i = 0; i < investorList.length(); i++) {
				JSONArray investorInfoArr = investorList.getJSONArray(i);
				boolean isFirst = true;
				String value = "(";
				for (int j = 0; j < investorInfoArr.length(); j++) {
					value += (isFirst ? "" : ",") + "\"" + investorInfoArr.getString(j) + "\"";
					isFirst = false;
				}
				value += ",\"" + localDate.toString() + "\")";
				if (i != 0)
					insertValues.append(",");
				insertValues.append(value);
			}
			String sql = "INSERT IGNORE INTO InvestorPrice(`name`,`person_p`,`foreigner_p`,`org_p`,`kumto_p`,`bohum_p`,`tosin_p`,`samo_p`,`bank_p`,`etc_fi_p`,`pension_p`,`nation_p`,`etc_corp_p`,`etc_for_p`, `trade_date`) VALUES "
					+ insertValues.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			ArrayList<String> InvestorNameArr = new ArrayList<String>();

			sql = "SELECT name FROM InvestorPrice WHERE trade_date = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				InvestorNameArr.add(rs.getString("name"));
			}

			// ?�이??��?�� 검??

			CloserUtil.closeable(rs, pstmt);

			int investorAmountAmount = 0;
			int investorPriceAmount = 0;

			sql = "SELECT count(*) amount FROM InvestorAmount WHERE trade_date = ? GROUP BY trade_date";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investorAmountAmount = rs.getInt("amount");
			}
			CloserUtil.closeable(rs, pstmt);

			sql = "SELECT count(*) amount FROM InvestorPrice WHERE trade_date = ? GROUP BY trade_date";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			if (rs.next()) {
				investorPriceAmount = rs.getInt("amount");
			}
			CloserUtil.closeable(rs, pstmt);

			if (investorAmountAmount != investorPriceAmount) {
				throw new FailSelectException("?�량 ?�이?��? 금액 ?�이??개수 ?�맞??);
			}

			// 매매 ?�일??총시가총액??Stock ?�이블의 ?�름??같�? 종목???�데?�트
			if (InvestorNameArr.size() != 0) {
				StringBuffer existsStockNameSb = new StringBuffer();
				existsStockNameSb.append("(");
				boolean isFirst = true;
				for (int i = 0; i < InvestorNameArr.size(); i++) {
					if (!isFirst)
						existsStockNameSb.append(",");
					existsStockNameSb.append("\"").append(InvestorNameArr.get(i)).append("\"");
					isFirst = false;
				}
				existsStockNameSb.append(")");

				StringBuffer whenSqlSb = new StringBuffer();
				for (int i = 0; i < investorMarketCapitalList.length(); i++) {
					JSONObject marketCapitalObj = investorMarketCapitalList.getJSONObject(i);
					whenSqlSb.append(" WHEN name = ").append("\"").append(marketCapitalObj.getString("name"))
							.append("\"").append(" THEN ").append(marketCapitalObj.getString("marketCapital"))
							.append(" ");
				}

				sql = " UPDATE `Stock` SET `marketCapital` = CASE " + whenSqlSb.toString() + " ELSE `marketCapital` "
						+ " END " + " WHERE `name` IN " + existsStockNameSb.toString();

				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}

			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("매매 금액 ?�보 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�늘???�워??관???�보 ?�비??추�?
	 * @param todayOROvertimeKeywordList
	 * @param localDate
	 * @return
	 * @throws FailInsertException
	 */
	public boolean insertTodayKeywordList(JSONArray todayOROvertimeKeywordList, LocalDate localDate)
			throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);

			String deleteSql = "DELETE FROM todayKeyword WHERE trade_date = ?";
			pstmt = conn.prepareStatement(deleteSql);
			pstmt.setString(1, localDate.toString());
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			StringBuffer insertValues = new StringBuffer();
			for (int i = 0; i < todayOROvertimeKeywordList.length(); i++) {
				JSONArray investorInfoArr = todayOROvertimeKeywordList.getJSONArray(i);
				boolean isFirst = true;
				String value = "(";
				for (int j = 0; j < investorInfoArr.length(); j++) {
					value += (isFirst ? "" : ",") + "\"" + investorInfoArr.getString(j) + "\"";
					isFirst = false;
				}
				value += ",\"" + localDate.toString() + "\")";
				if (i != 0)
					insertValues.append(",");
				insertValues.append(value);
			}
			String sql = "INSERT IGNORE INTO todayKeyword(`name`,`cur_price`,`updown_price`,`updown_rate`,`volume`,`trade_date`) VALUES "
					+ insertValues.toString();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			CloserUtil.closeable(rs, pstmt);

			ArrayList<String> stockNameArr = new ArrayList<String>();
			ArrayList<String> noneExistsStockNameArr = new ArrayList<String>();

			sql = "SELECT name FROM todayKeyword WHERE trade_date = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				stockNameArr.add(rs.getString("name"));
			}

			for (int i = 0; i < todayOROvertimeKeywordList.length(); i++) {
				String stockName = todayOROvertimeKeywordList.getJSONArray(i).getString(0);
				if (!stockNameArr.contains(stockName))
					noneExistsStockNameArr.add(stockName);
			}


			ArrayList<String> similarStockNameArr = new ArrayList<String>();
			HashMap<String, String> stockNameHashMap = new HashMap<String, String>();

			if (noneExistsStockNameArr.size() != 0) {
				CloserUtil.closeable(rs, pstmt);
				sql = "SELECT name FROM Stock WHERE name LIKE ? ";
				pstmt = conn.prepareStatement(sql);
				for (int i = 0; i < noneExistsStockNameArr.size(); i++) {
					CloserUtil.closeable(rs);

					pstmt.clearParameters();
					pstmt.setString(1, noneExistsStockNameArr.get(i) + "%");
					rs = pstmt.executeQuery();

					if (rs.next()) {
						String stockName = rs.getString("name");
						// ?�시��?배열???�??
						similarStockNameArr.add(noneExistsStockNameArr.get(i));
						// ?�사 맵에 ?�??
						stockNameHashMap.put(noneExistsStockNameArr.get(i), stockName);
						noneExistsStockNameArr.remove(i);
					}
				}


				JSONArray similarTodayOROvertimeKeywordList = new JSONArray();

				for (int i = 0; i < similarStockNameArr.size(); i++) {
					String similarStockName = similarStockNameArr.get(i);
					for (int j = 0; j < todayOROvertimeKeywordList.length(); j++) {
						JSONArray dataList = todayOROvertimeKeywordList.getJSONArray(j);
						if (dataList.getString(0).equals(similarStockName)) {
							dataList.put(0, stockNameHashMap.get(similarStockName));
							similarTodayOROvertimeKeywordList.put(dataList);
							break;
						}
					}
				}//end of for

				if(similarTodayOROvertimeKeywordList.length() != 0) {
					StringBuffer similarInsertValues = new StringBuffer();
					for (int i = 0; i < similarTodayOROvertimeKeywordList.length(); i++) {
						JSONArray infoArr = similarTodayOROvertimeKeywordList.getJSONArray(i);
						boolean isFirst = true;
						String value = "(";
						for (int j = 0; j < infoArr.length(); j++) {
							value += (isFirst ? "" : ",") + "\"" + infoArr.getString(j) + "\"";
							isFirst = false;
						}
						value += ",\"" + localDate.toString() + "\")";
						if (i != 0)
							similarInsertValues.append(",");
						similarInsertValues.append(value);
					}

					sql = "INSERT IGNORE INTO todayKeyword(`name`,`cur_price`,`updown_price`,`updown_rate`,`volume`,`trade_date`) VALUES "
							+ similarInsertValues.toString();
					pstmt = conn.prepareStatement(sql);
					pstmt.executeUpdate();

					CloserUtil.closeable(rs, pstmt);
				}
			}

			if (noneExistsStockNameArr.size() != 0) {
				CloserUtil.closeable(rs, pstmt);
				StringBuffer noneExistsStockNameSb = new StringBuffer();
				boolean isFirst = true;
				for (int i = 0; i < noneExistsStockNameArr.size(); i++) {
					if (!isFirst)
						noneExistsStockNameSb.append(",");
					noneExistsStockNameSb.append("(\"").append(noneExistsStockNameArr.get(i)).append("\")");
					isFirst = false;
				}
				sql = "INSERT IGNORE INTO noneExistsStock(`name`) VALUES " + noneExistsStockNameSb.toString();
				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�늘???�워???�보 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	/**
	 * @author :?��???jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : ?�간 ??급상???�워???�이???�비??추�?
	 * @param todayOROvertimeKeywordList
	 * @param localDate
	 * @return
	 * @throws FailInsertException
	 */
	public boolean insertOvertimeKeywordList(JSONArray todayOROvertimeKeywordList, LocalDate localDate)
			throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);

			String deleteSql = "DELETE FROM overtimeKeyword WHERE trade_date = ?";
			pstmt = conn.prepareStatement(deleteSql);
			pstmt.setString(1, localDate.toString());
			pstmt.executeUpdate();

			CloserUtil.closeable(pstmt);

			StringBuffer insertValues = new StringBuffer();
			for (int i = 0; i < todayOROvertimeKeywordList.length(); i++) {
				JSONArray investorInfoArr = todayOROvertimeKeywordList.getJSONArray(i);
				boolean isFirst = true;
				String value = "(";
				for (int j = 0; j < investorInfoArr.length(); j++) {
					value += (isFirst ? "" : ",") + "\"" + investorInfoArr.getString(j) + "\"";
					isFirst = false;
				}
				value += ",\"" + localDate.toString() + "\")";
				if (i != 0)
					insertValues.append(",");
				insertValues.append(value);
			}
			String sql = "INSERT IGNORE INTO overtimeKeyword(`name`,`cur_price`,`updown_price`,`updown_rate`,`volume`,`transactionPrice`,`trade_date`) VALUES "
					+ insertValues.toString();

			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();

			CloserUtil.closeable(rs, pstmt);

			ArrayList<String> stockNameArr = new ArrayList<String>();
			ArrayList<String> noneExistsStockNameArr = new ArrayList<String>();

			sql = "SELECT name FROM overtimeKeyword WHERE trade_date = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				stockNameArr.add(rs.getString("name"));
			}

			for (int i = 0; i < todayOROvertimeKeywordList.length(); i++) {
				String stockName = todayOROvertimeKeywordList.getJSONArray(i).getString(0);
				if (!stockNameArr.contains(stockName))
					noneExistsStockNameArr.add(stockName);
			}

			ArrayList<String> similarStockNameArr = new ArrayList<String>();
			HashMap<String, String> stockNameHashMap = new HashMap<String, String>();

			if (noneExistsStockNameArr.size() != 0) {
				CloserUtil.closeable(rs, pstmt);
				sql = "SELECT name FROM Stock WHERE name LIKE ? ";
				pstmt = conn.prepareStatement(sql);
				for (int i = 0; i < noneExistsStockNameArr.size(); i++) {
					CloserUtil.closeable(rs);

					pstmt.clearParameters();
					pstmt.setString(1, noneExistsStockNameArr.get(i) + "%");
					rs = pstmt.executeQuery();

					if (rs.next()) {
						String stockName = rs.getString("name");
						// ?�시��?배열???�??
						similarStockNameArr.add(noneExistsStockNameArr.get(i));
						// ?�사 맵에 ?�??
						stockNameHashMap.put(noneExistsStockNameArr.get(i), stockName);
						noneExistsStockNameArr.remove(i);
					}
				}


				JSONArray similarTodayOROvertimeKeywordList = new JSONArray();

				for (int i = 0; i < similarStockNameArr.size(); i++) {
					String similarStockName = similarStockNameArr.get(i);
					for (int j = 0; j < todayOROvertimeKeywordList.length(); j++) {
						JSONArray dataList = todayOROvertimeKeywordList.getJSONArray(j);
						if (dataList.getString(0).equals(similarStockName)) {
							dataList.put(0, stockNameHashMap.get(similarStockName));
							similarTodayOROvertimeKeywordList.put(dataList);
							break;
						}
					}
				}//end of for

				if(similarTodayOROvertimeKeywordList.length() != 0) {
					StringBuffer similarInsertValues = new StringBuffer();
					for (int i = 0; i < similarTodayOROvertimeKeywordList.length(); i++) {
						JSONArray infoArr = similarTodayOROvertimeKeywordList.getJSONArray(i);
						boolean isFirst = true;
						String value = "(";
						for (int j = 0; j < infoArr.length(); j++) {
							value += (isFirst ? "" : ",") + "\"" + infoArr.getString(j) + "\"";
							isFirst = false;
						}
						value += ",\"" + localDate.toString() + "\")";
						if (i != 0)
							similarInsertValues.append(",");
						similarInsertValues.append(value);
					}

					sql = "INSERT IGNORE INTO overtimeKeyword(`name`,`cur_price`,`updown_price`,`updown_rate`,`volume`,`transactionPrice`,`trade_date`) VALUES "
							+ similarInsertValues.toString();
					pstmt = conn.prepareStatement(sql);
					pstmt.executeUpdate();

					CloserUtil.closeable(rs, pstmt);
				}
			}

			if (noneExistsStockNameArr.size() != 0) {
				CloserUtil.closeable(rs, pstmt);
				StringBuffer noneExistsStockNameSb = new StringBuffer();
				boolean isFirst = true;
				for (int i = 0; i < noneExistsStockNameArr.size(); i++) {
					if (!isFirst)
						noneExistsStockNameSb.append(",");
					noneExistsStockNameSb.append("(\"").append(noneExistsStockNameArr.get(0)).append("\")");
					isFirst = false;
				}
				sql = "INSERT IGNORE INTO noneExistsStock(`name`) VALUES " + noneExistsStockNameSb.toString();
				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�늘?? ?�간???�워???�보 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	public JSONArray getNoneExistsStockList() throws FailSelectException {
		JSONArray nonExistsStockList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM noneExistsStock";

			pstmt = conn.prepareStatement(sql);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				nonExistsStockList.put(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return nonExistsStockList;
	}

	public boolean insertStock(String name, String code, String sectors, String product, String type)
			throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);
			String sql = " INSERT INTO Stock(`name`,`code`,`sectors`,`product`,`type`) VALUES(?, ?, ?, ?, ?) "
					+ " ON DUPLICATE KEY UPDATE name = VALUES(name), code = VALUES(code), sectors = VALUES(sectors), product = VALUES(product), type = VALUES(type)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setString(2, code);
			if (sectors == null)
				pstmt.setNull(3, Types.VARCHAR);
			else
				pstmt.setString(3, sectors);

			if (product == null)
				pstmt.setNull(4, Types.VARCHAR);
			else
				pstmt.setString(4, product);

			pstmt.setString(5, type);
			result = pstmt.executeUpdate() > 0;

			if (result) {
				CloserUtil.closeable(pstmt);
				sql = "DELETE FROM `noneExistsStock` WHERE name = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, name);
				pstmt.executeUpdate();
			}

			conn.commit();
		} catch (SQLException e) {
			throw new FailInsertException("종목 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	public JSONObject getTopStockList(JSONArray searchCondition, String type, String stockType, LocalDate localDate)
			throws FailSelectException {
		JSONObject topStockAndKeywordDataListObj = new JSONObject();
		JSONObject topKeywordList = new JSONObject();
		JSONArray topStockDataList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuffer searchSql = new StringBuffer();
		String whereSql = "";
		try {
			boolean isFirst = true;
			for (int i = 0; i < searchCondition.length(); i++) {
				if (!isFirst) {
					searchSql.append(" + ");
				} else {
					isFirst = false;
				}

				searchSql.append(searchCondition.getString(i));
			}
			searchSql.append(" AS sum");

			if (stockType.equals("total")) {

			} else if (stockType.equals("KP")) {
				whereSql = " AND `Stock`.type = \"KP\" ";
			} else {
				whereSql = " AND `Stock`.type = \"KD\" ";
			}

			if (type.equals("amount")) {
				whereSql += " AND `InvestorAmount`.amount >= 1000 ";
			}

			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "SELECT `Stock`.name, `Stock`.code, `InvestorAmount`.cur_price, `InvestorAmount`.updown_price, `InvestorAmount`.updown_rate, `InvestorAmount`.trade_date,"
					+ searchSql.toString()
					+ " FROM InvestorAmount JOIN InvestorPrice ON `InvestorAmount`.name = `InvestorPrice`.name AND `InvestorAmount`.trade_date = `InvestorPrice`.trade_date "
					+ " JOIN Stock ON `InvestorAmount`.name = `Stock`.name WHERE `InvestorAmount`.trade_date = ? AND `InvestorAmount`.cur_price > 0 "
					+ whereSql + " ORDER BY sum DESC limit 0, 100";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();
			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockCodeSet = new HashSet<String>();
			while (rs.next()) {
				if (type.equals("price") && rs.getInt("sum") == 0)
					continue;

				stockCodeSet.add(rs.getString("code"));
				JSONObject topStockData = new JSONObject();
				topStockData.put("name", rs.getString("name"));
				topStockData.put("code", rs.getString("code"));
				topStockData.put("curPrice", rs.getInt("cur_price"));
				topStockData.put("updownPrice", rs.getInt("updown_price"));
				topStockData.put("updownRate", rs.getDouble("updown_rate"));
				topStockData.put("tradeDate", rs.getString("trade_date"));
				topStockData.put("sum", rs.getInt("sum"));

				topStockDataList.put(topStockData);
			}

			topStockAndKeywordDataListObj.put("topStockDataList", topStockDataList);
			CloserUtil.closeable(rs, pstmt);
			if (stockCodeSet.size() != 0) {
				Iterator<String> codeIt = stockCodeSet.iterator();
				StringBuffer stockCodeSb = new StringBuffer();
				stockCodeSb.append("(");
				while (codeIt.hasNext()) {
					stockCodeSb.append("\"").append(codeIt.next()).append("\",");
				}
				stockCodeSb.deleteCharAt(stockCodeSb.length() - 1);
				stockCodeSb.append(")");
				sql = "SELECT COUNT(*) as amount, keyword, ID, sum(updown_rate) as sumRate FROM `keyword` JOIN `stockKeywordMapping` ON `keyword`.ID = `stockKeywordMapping`.KID JOIN Stock ON stockKeywordMapping.CODE = Stock.code JOIN InvestorAmount ON Stock.name = InvestorAmount.name WHERE `stockKeywordMapping`.CODE IN "
						+ stockCodeSb.toString()
						+ " AND trade_date = ? GROUP BY `keyword`.ID ORDER BY amount DESC, sumRate DESC";

				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, localDate.toString());
				rs = pstmt.executeQuery();

				while (rs.next()) {
					JSONObject keywordObj = new JSONObject();
					keywordObj.put("amount", rs.getInt("amount"));
					keywordObj.put("id", rs.getInt("ID"));
					keywordObj.put("keyword", rs.getString("keyword"));
					keywordObj.put("sumRate", rs.getDouble("sumRate"));
					topKeywordList.put(String.valueOf(rs.getInt("ID")), keywordObj);
				}
			}

			topStockAndKeywordDataListObj.put("topKeywordList", topKeywordList);
			topStockAndKeywordDataListObj.put("stockCodeSet", stockCodeSet);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("todayKeyword 검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return topStockAndKeywordDataListObj;
	}

	public boolean deleteStock(String code) throws FailDeleteException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = "DELETE FROM `Stock` WHERE code = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			pstmt.executeUpdate();

			result = true;
		} catch (SQLException e) {
			throw new FailDeleteException("종목 ??�� ?�패");
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}
		return result;
	}

	public boolean insertKeywordAddRequest(int userId, String code, JSONArray requestKeywordArr)
			throws FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			StringBuffer insertSb = new StringBuffer(); // ?�입???�트��?버퍼

			// ?�워??문자?�을 쿼리 ?�입문으��?변경하??부��?
			// 받�? ?�워???�이즈�? 0 ?�라��?false 리턴
			if (requestKeywordArr.length() > 0) {
				for (int i = 0; i < requestKeywordArr.length(); i++) {
					insertSb.append("(").append(userId).append(",\"").append(code).append("\",\"")
							.append(requestKeywordArr.get(i)).append("\"),");
				}
			} else {
				return false;
			}

			insertSb.deleteCharAt(insertSb.length() - 1);

			conn = DBManager.getConnection(values.STOCK_DB_NAME);
			conn.setAutoCommit(false);

			String sql = "INSERT IGNORE INTO KeywordAddRequest(`uid`,`code`,`keyword`) VALUES " + insertSb.toString();

			pstmt = conn.prepareStatement(sql);

			pstmt.executeUpdate();

			conn.commit();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailInsertException("?�워??추�? ?�청 ?�입 ?�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return result;
	}

	public JSONArray getKeywordIncludStockList(int kid) throws FailSelectException {
		JSONArray stockList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM `stockKeywordMapping` WHERE `stockKeywordMapping`.KID = ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, kid);
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			while (rs.next()) {
				stockList.put(rs.getString("code"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return stockList;
	}

	public void setKeywordIncludStockLists(JSONObject keywordArr, JSONArray stockCodeArr2) throws FailSelectException {
		JSONArray keywordList = keywordArr.toJSONArray(keywordArr.names());
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			StringBuffer keywordIdSb = new StringBuffer();
			if (keywordList.length() != 0) {
				keywordIdSb.append("(");
				for (int i = 0; i < keywordList.length(); i++) {
					JSONObject keywordObj = keywordList.getJSONObject(i);
					keywordIdSb.append("").append(keywordObj.getInt("id")).append(",");
				}
				keywordIdSb.deleteCharAt(keywordIdSb.length() - 1);
				keywordIdSb.append(")");

				conn = DBManager.getConnection(values.STOCK_DB_NAME);

				String sql = " SELECT * FROM `stockKeywordMapping` WHERE `stockKeywordMapping`.KID IN "
						+ keywordIdSb.toString();

				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();

				// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
				HashSet<String> existsKeywordIdSet = new HashSet<String>();
				List<String> stockCodeList = new ArrayList<String>();
				for (int j = 0; j < stockCodeArr2.length(); j++) {
					stockCodeList.add(stockCodeArr2.getString(j));
				}
				while (rs.next()) {
					if (!stockCodeList.contains(rs.getString("CODE"))) {
						continue;
					}

					String kid = String.valueOf(rs.getInt("kid"));

					JSONObject keywordObj = (JSONObject) keywordArr.get(kid);
					JSONArray stockCodeArr;
					if (existsKeywordIdSet.contains(kid)) {
						stockCodeArr = keywordObj.getJSONArray("stockCodeArr");
						if (rs.getString("CODE") != null) {
							stockCodeArr.put(rs.getString("CODE"));
						}
					} else {
						stockCodeArr = new JSONArray();
						if (rs.getString("CODE") != null) {
							stockCodeArr.put(rs.getString("CODE"));
						}
						keywordObj.put("stockCodeArr", stockCodeArr);
					}
					existsKeywordIdSet.add(kid);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
	}

	public JSONArray getSelectStockKeywordList(String code) throws FailSelectException {
		JSONArray keywordList = new JSONArray();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM `keyword` JOIN `stockKeywordMapping` ON `keyword`.ID = `stockKeywordMapping`.KID WHERE `stockKeywordMapping`.CODE = ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			while (rs.next()) {
				keywordList.put(rs.getString("keyword"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return keywordList;
	}

	public JSONObject getSupplyNdemandDataAmount(LocalDate localDate) throws FailSelectException {
		JSONObject amountInfo = new JSONObject();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT count(*) amount FROM supplyNdemand WHERE trade_date = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set

			amountInfo.put("amount", 0);

			if (rs.next()) {
				amountInfo.put("amount", rs.getInt("amount"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("?�급?�착 ?�이???�량 검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return amountInfo;
	}

	public JSONObject getSupplyNdemandData(LocalDate localDate, int offset, int size) throws FailSelectException {
		JSONObject supplyNdemandDataListObj = new JSONObject();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection(values.STOCK_DB_NAME);

			String sql = " SELECT * FROM supplyNdemand WHERE trade_date = ? ORDER BY name LIMIT ?, ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, localDate.toString());
			pstmt.setInt(2, offset);
			pstmt.setInt(3, size);
			rs = pstmt.executeQuery();

			// 리스?�에 ?�입??종목?�의 코드��??�?�하??set
			HashSet<String> stockNameSet = new HashSet<String>();

			while (rs.next()) {
				JSONObject supplyObj = new JSONObject();
				String name = rs.getString("name");
				stockNameSet.add(name);

				supplyObj.put("name", name);
				supplyObj.put("trade_date", name);
				supplyObj.put("foreignerRank", rs.getInt("foreignerRank"));
				supplyObj.put("orgRank", rs.getInt("orgRank"));
				supplyObj.put("tosinRank", rs.getInt("tosinRank"));
				supplyObj.put("samoRank", rs.getInt("samoRank"));
				supplyObj.put("pensionRank", rs.getInt("pensionRank"));

				supplyNdemandDataListObj.put(name, supplyObj);
			}

			CloserUtil.closeable(rs, pstmt);

			if (stockNameSet.size() != 0) {
				Iterator<String> it = stockNameSet.iterator();

				StringBuffer stockNameSb = new StringBuffer();
				stockNameSb.append("(");
				boolean isFirst = true;

				while (it.hasNext()) {

					if (!isFirst)
						stockNameSb.append(",");

					stockNameSb.append("\'").append(it.next()).append("\'");

					isFirst = false;
				}
				stockNameSb.append(")");

				sql = "SELECT * FROM Stock LEFT JOIN stockKeywordMapping ON Stock.code = stockKeywordMapping.code LEFT JOIN keyword ON stockKeywordMapping.KID = keyword.ID "
						+ " WHERE `Stock`.name IN " + stockNameSb.toString();
				pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();

				HashSet<String> existsStockNameSet = new HashSet<String>();
				while (rs.next()) {
					String name = rs.getString("name");

					JSONObject supplyObj = (JSONObject) supplyNdemandDataListObj.get(name);
					JSONArray keywordArr;
					if (existsStockNameSet.contains(name)) {
						keywordArr = supplyObj.getJSONArray("keywordArr");
						if (rs.getString("keyword") != null) {
							JSONObject keywordObj = new JSONObject();
							keywordObj.put("keyword", rs.getString("keyword"));
							keywordObj.put("kid", rs.getInt("KID"));
							keywordArr.put(keywordObj);
						}
					} else {
						keywordArr = new JSONArray();
						if (rs.getString("keyword") != null) {
							JSONObject keywordObj = new JSONObject();
							keywordObj.put("keyword", rs.getString("keyword"));
							keywordObj.put("kid", rs.getInt("KID"));
							keywordArr.put(keywordObj);
						}
						supplyObj.put("code", rs.getString("CODE"));
						supplyObj.put("keywordArr", keywordArr);
					}
					existsStockNameSet.add(name);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new FailSelectException("?�급 ?�착 ?�이??검???�패");
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}
		return supplyNdemandDataListObj;
	}
}
