package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.BadRequestException;

import org.json.JSONArray;
import org.json.JSONObject;

import exception.v1.DuplicateNameException;
import exception.v1.FailDeleteException;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import util.CONFIG_VALUE;
import util.CloserUtil;
import util.DBManager;

public class AccountDao {
	CONFIG_VALUE values;

	public AccountDao() {

	}

	public AccountDao(CONFIG_VALUE values) {
		this.values = values;
	}

	/**
	 * 관리자의 아이디와 비밀번호를 확인하는 메서드
	 * 
	 * @param id       아이디
	 * @param password 비밀번호
	 * @return 아이디와 비밀번호가 DB에 존재 : true, 아이디가 없거나 비밀번호가 일치하지 않을 경우 : false
	 * @since 2018. 9. 14
	 */
	public JSONObject checkIDNPassword(String email, String password) {
		JSONObject resultObj = new JSONObject();
		boolean correct = false;
		resultObj.put("correct", correct);

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBManager.getConnection(values.getDBName());

			String sql = "SELECT * FROM userInfo WHERE email = ?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);

			rs = pstmt.executeQuery();
			if (rs.next()) {
				String storedPassword = rs.getString("password");
				correct = storedPassword.equals(password);

				boolean isAdmin = rs.getBoolean("isAdmin");
				resultObj.put("correct", correct);
				resultObj.put("id", rs.getInt("id"));
				resultObj.put("isAdmin", isAdmin);
				resultObj.put("nickname", rs.getString("nickname"));
				resultObj.put("phoneNumber", rs.getString("phoneNumber"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			CloserUtil.closeable(rs, pstmt, conn);
		}

		return resultObj;
	}

	public boolean signupUser(String email, String password, String nickname, String phoneNumber)
			throws DuplicateNameException, FailInsertException {
		boolean result = false;
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection(values.getDBName());

			String sql = "INSERT INTO userInfo(`email`,`password`,`nickname`,`phoneNumber`) VALUES(?, ?, ?, ?)";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			pstmt.setString(2, password);
			pstmt.setString(3, nickname);
			pstmt.setString(4, phoneNumber);

			result = pstmt.executeUpdate() > 0;

		} catch (SQLException e) {
			if (e.getErrorCode() == 1062) {
				throw new DuplicateNameException("duplicate name");
			} else {
				e.printStackTrace();
				throw new FailInsertException("종목 삽입 실패");
			}
		} finally {
			CloserUtil.closeable(pstmt, conn);
		}

		return result;
	}
}
