package main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;

import dao.AccountDao;
import exception.v1.DuplicateNameException;
import exception.v1.FailInsertException;
import util.CONFIG_VALUE;
import util.EnDecryptionTool;

/**
 * @author :정준형(jhj@rinasoft.co.kr)
 * @since :2021. 4. 14.
 * @description : 로그인과 관련된 서비스
 */
@Path("/main")
public class MainService {
	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 4.
	 * @description : 로그아웃 시 호출
	 * 
	 * @param request  요청 정보
	 * @param id       아이디
	 * @param password 비밀번호
	 * @return
	 *         <ul>
	 *         <li>응답코드 200</li>
	 *         <li>응답코드 400 : 아이디 또는 비밀번호를 전달받지 못했을 경우 또는 비밀번호를 암호화하지 못했을 경우</li>
	 *         <li>응답코드 401 : 아이디가 없거나, 비밀번호가 일치하지 않을 경우</li>
	 *         </ul>
	 * @since 2021. 5. 04
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(@Context HttpServletRequest request, @FormParam("email") String email,
			@FormParam("password") String password) {
		if (email == null || password == null) {
			// 아이디 또는 비밀번호를 전달받지 못했을 경우 응답코드 400
			return Response.status(Status.BAD_REQUEST).build();
		}

		// 비밀번호를 암호화할 인스턴스 생성
		EnDecryptionTool tool = new EnDecryptionTool();

		try {
			// 비밀번호를 암호화
			password = tool.aesEncodeWithNotURLEncode(password);

			// 아이디와 비밀번호를 확인할 DAO
			AccountDao accountDao = new AccountDao(new CONFIG_VALUE(request.getLocalAddr()));

			// 아이디와 비밀번호 확인
			JSONObject resultObj = accountDao.checkIDNPassword(email, password);

			if (resultObj.getBoolean("correct")) {
				// 아이디와 비밀번호가 일치한다면
				// 세션
				HttpSession session = request.getSession();

				// 세션에 로그인 상태와 어드민 값 저장

				session.setAttribute("isLogin", true);
				session.setAttribute("id", resultObj.getInt("id"));
				session.setAttribute("isAdmin", resultObj.getBoolean("isAdmin"));
				session.setAttribute("nickname", resultObj.getString("nickname"));
				session.setAttribute("phoneNumber", resultObj.getString("phoneNumber"));
				session.setMaxInactiveInterval(3600);

				return Response.ok().build();
			} else {
				// 아이디가 없거나 비밀번호가 일치하지 않는다면 응답코드 401
				return Response.status(Status.UNAUTHORIZED).build();
			}
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			// 비밀번호가 잘못된 형식을 전달됬을 경우 응답코드 400
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 4.
	 * @description : 로그아웃 시 호출
	 * @param request
	 * @return
	 */
	@POST
	@Path("/logout")
	public Response logout(@Context HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();

			// 세션에 로그인 상태와 어드민 값 저장

			session.invalidate();

			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
	}

	@POST
	@Path("/signup")
	public Response signup(@Context HttpServletRequest request, @FormParam("email") String email,
			@FormParam("password") String password, @FormParam("nickname") String nickname,
			@FormParam("phoneNumber") String phoneNumber) {
		if (email == null || password == null) {
			// 아이디 또는 비밀번호를 전달받지 못했을 경우 응답코드 400
			return Response.status(Status.BAD_REQUEST).build();
		}

		// 비밀번호를 암호화할 인스턴스 생성
		EnDecryptionTool tool = new EnDecryptionTool();

		try {
			// 비밀번호를 암호화
			password = tool.aesEncodeWithNotURLEncode(password);

			// 아이디와 비밀번호를 확인할 DAO
			AccountDao accountDao = new AccountDao(new CONFIG_VALUE(request.getLocalAddr()));

			boolean result = accountDao.signupUser(email, password, nickname, phoneNumber);

			if (result) {
				return Response.status(Status.CREATED).build();
			} else {
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}

		} catch (FailInsertException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			return Response.status(Status.FORBIDDEN).build();
		} catch (DuplicateNameException e) {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
	}
}
