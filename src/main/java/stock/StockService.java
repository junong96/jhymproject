package stock;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.JSONArray;
import org.json.JSONObject;

import dao.StockDao;
import exception.v1.FailInsertException;
import exception.v1.FailSelectException;
import exception.v1.UnknownFilenameExtensionException;
import util.CONFIG_VALUE;
import util.FileUtil;

/**
 * @author :정준형(jhj@rinasoft.co.kr)
 * @since :2021. 4. 14.
 * @description : 주식 품목 호출, 디비에 데이터 저장하기 위한 서비스
 */
@Path("/stockManage")
public class StockService {

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 14.
	 * @description : 주식 품목리스트, 키워드 리스트가져옴
	 * @param request
	 * @return
	 */
	@Path("/stockAndKeywordList")
	@GET
	public Response getAllStockAndKeywordList(@Context HttpServletRequest request) {
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject stockAndKeywordListObj;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);

			stockAndKeywordListObj = stockDao.getStockAndKeywordList();

			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			if (stockAndKeywordListObj.length() != 0) {
				return Response.ok().entity(stockAndKeywordListObj.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 주식 종목 검색에 대한 결과를 가져옴
	 * @param request
	 * @param type       - keyword || stock
	 * @param logic      - and || or
	 * @param searchWord - type 이 stock 일때 String 으로 날라옴, 이때 keywords 는 null
	 * @param keywords   - type 이 keyword 일때 JSONArray String 으로 날라옴, 이때 searchWord
	 *                   는 null
	 * @return
	 */
	@Path("/stockList")
	@GET
	public Response getStockList(@Context HttpServletRequest request, @QueryParam("type") String type,
			@QueryParam("logic") String logic, @QueryParam("searchWord") String searchWord,
			@QueryParam("keywords") String keywords) {
		if ((!type.equals("keyword") && !type.equals("stock")) || type == null)
			return Response.status(Status.BAD_REQUEST).build();
		if ((!logic.equals("and") && !logic.equals("or")) || logic == null)
			return Response.status(Status.BAD_REQUEST).build();
		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		try {
			JSONObject stockListObj;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			stockListObj = stockDao.getStockList(type, logic, searchWord, keywords);
			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			JSONArray stockList = stockListObj.toJSONArray(stockListObj.names());
			if (stockList != null) {
				return Response.ok().entity(stockList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	@Path("/keywordAdd")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertKeywordAddRequest(@Context HttpServletRequest request, @HeaderParam("userId") int userId,
			@HeaderParam("code") String code, @FormParam("keywords") String keywords) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();
		if (userId == 0)
			return Response.status(Status.BAD_REQUEST).build();
		if (keywords == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		String checkValid = checkValidation(request, userId);

		switch (checkValid) {
		case "noLogin":
			return Response.status(Status.FORBIDDEN).build();
		case "noValid":
			return Response.status(Status.NOT_ACCEPTABLE).build();
		default:
		}

		try {
			// 주식 종목 노트를 삽입하기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			JSONArray requestKeywordArr = new JSONArray(keywords);

			if (requestKeywordArr.length() < 1) {
				return Response.status(Status.BAD_REQUEST).build();
			}

			boolean result = stockDao.insertKeywordAddRequest(userId, code, requestKeywordArr);

			if (result) {
				return Response.status(Status.OK).build();
			} else {
				return Response.serverError().build();
			}

		} catch (FailInsertException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 오늘의 키워드, 시간외 급등 키워드 데이터를 가져옴
	 * @param request
	 * @param type
	 * @return
	 */
	@Path("/{type}/keywordData")
	@GET
	public Response getKeywordInfoData(@Context HttpServletRequest request, @PathParam("type") String type,
			@QueryParam("date") String date) {
		if ((!type.equals("todayKeyword") && !type.equals("overtimeKeyword")) || type == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject keywordDataListObj;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			LocalDate localDate;

			if (date == null) {
				localDate = LocalDate.now();
			} else {
				localDate = LocalDate.parse(date);
			}
			switch (type) {
			case "todayKeyword":
				keywordDataListObj = stockDao.getTodayKeywordList(localDate);
				break;
			default: // overtimeKeyword
				keywordDataListObj = stockDao.getOvertimeKeywordList(localDate);
				break;
			}
			
			JSONObject tempTopKeywordListObj = keywordDataListObj.getJSONObject("topKeywordList");
			JSONArray stockCodeArr = keywordDataListObj.getJSONArray("stockCodeSet");
			
			if (tempTopKeywordListObj.length() != 0) {
				stockDao.setKeywordIncludStockLists(tempTopKeywordListObj, stockCodeArr);

				keywordDataListObj.put("topKeywordList",
						tempTopKeywordListObj.toJSONArray(tempTopKeywordListObj.names()));
			}
			
			return Response.ok().entity(keywordDataListObj.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 오늘의 키워드, 시간외 급등 키워드 데이터를 가져옴
	 * @param request
	 * @param type
	 * @return
	 */
	@Path("/topStock")
	@GET
	public Response getTopStockInfoData(@Context HttpServletRequest request, @QueryParam("type") String type,
			@QueryParam("stockType") String stockType, @QueryParam("searchConditionArr") String searchConditionArr,
			@QueryParam("date") String date) {
		if (searchConditionArr == null || searchConditionArr == "")
			return Response.status(Status.BAD_REQUEST).build();

		if ((!type.equals("price") && !type.equals("amount")) || type == null)
			return Response.status(Status.BAD_REQUEST).build();

		if ((!stockType.equals("total") && !stockType.equals("KD") && !stockType.equals("KP")) || stockType == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONObject stockDataListObj;
			JSONArray searchCondition = new JSONArray(searchConditionArr);
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			LocalDate localDate;

			if (date == null) {
				localDate = LocalDate.now();
			} else {
				localDate = LocalDate.parse(date);
			}

			stockDataListObj = stockDao.getTopStockList(searchCondition, type, stockType, localDate);

			JSONObject tempTopKeywordListObj = stockDataListObj.getJSONObject("topKeywordList");
			JSONArray stockCodeArr = stockDataListObj.getJSONArray("stockCodeSet");
			
			if (tempTopKeywordListObj.length() != 0) {
				stockDao.setKeywordIncludStockLists(tempTopKeywordListObj, stockCodeArr);

				stockDataListObj.put("topKeywordList",
						tempTopKeywordListObj.toJSONArray(tempTopKeywordListObj.names()));
			}

			return Response.ok().entity(stockDataListObj.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 수급포착 데이터 수량을 가져옴
	 * @param request
	 * @param type
	 * @return
	 */
	@Path("/supplyNdemandAmount")
	@GET
	public Response getSupplyNdemandData(@Context HttpServletRequest request, @QueryParam("date") String date) {

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			LocalDate localDate;

			if (date == null) {
				localDate = LocalDate.now();
			} else {
				localDate = LocalDate.parse(date);
			}

			JSONObject amountInfo = stockDao.getSupplyNdemandDataAmount(localDate);

			return Response.ok().entity(amountInfo.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 05. 14
	 * @description : 수급 포착 데이터를 가져옴
	 * @param request
	 * @param type
	 * @return
	 */
	@Path("/supplyNdemand")
	@GET
	public Response getSupplyNdemandData(@Context HttpServletRequest request, @QueryParam("date") String date,
			@QueryParam("offset") int offset) {

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());

		try {
			JSONArray SupplyNdemandDataList = new JSONArray();
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			LocalDate localDate;

			if (date == null) {
				localDate = LocalDate.now();
			} else {
				localDate = LocalDate.parse(date);
			}

			int size = 50;
			JSONObject SupplyNdemandDataListObj = stockDao.getSupplyNdemandData(localDate, offset, size);
			if (SupplyNdemandDataListObj.length() > 0) {
				SupplyNdemandDataList = SupplyNdemandDataListObj.toJSONArray(SupplyNdemandDataListObj.names());
			}
			return Response.ok().entity(SupplyNdemandDataList.toString()).build();
		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 특정 키워드를 포함한 종목 code 만 가져옴
	 * @param request
	 * @param type       - keyword || stock
	 * @param logic      - and || or
	 * @param searchWord - type 이 stock 일때 String 으로 날라옴, 이때 keywords 는 null
	 * @param keywords   - type 이 keyword 일때 JSONArray String 으로 날라옴, 이때 searchWord
	 *                   는 null
	 * @return
	 */
	@Path("/keywordIncludStockList")
	@GET
	public Response getKeywordIncludStockList(@Context HttpServletRequest request, @QueryParam("kid") int kid) {
		if (kid < 1)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		try {
			JSONArray stockList;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			stockList = stockDao.getKeywordIncludStockList(kid);
			// 스크립트에서 편리하게 다루기 위하여 JSONObject -> JSONArray 형태로 변환
			if (stockList.length() != 0) {
				return Response.ok().entity(stockList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 20.
	 * @description : 특정 키워드를 포함한 종목 code 만 가져옴
	 * @param request
	 * @param type       - keyword || stock
	 * @param logic      - and || or
	 * @param searchWord - type 이 stock 일때 String 으로 날라옴, 이때 keywords 는 null
	 * @param keywords   - type 이 keyword 일때 JSONArray String 으로 날라옴, 이때 searchWord
	 *                   는 null
	 * @return
	 */
	@Path("/selectStockKeywordList")
	@GET
	public Response getSelectStockKeywordList(@Context HttpServletRequest request, @QueryParam("code") String code) {
		if (code == null || code.length() == 0 || code.length() > 6)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		try {
			JSONArray keywordList;
			// 주식 품목을 가져오기 위한 DAO 객체
			StockDao stockDao = new StockDao(values);
			keywordList = stockDao.getSelectStockKeywordList(code);
			if (keywordList.length() != 0) {
				return Response.ok().entity(keywordList.toString()).build();
			} else {
				return Response.noContent().build();
			}

		} catch (FailSelectException e) {
			return Response.serverError().build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 4. 19.
	 * @description : tsv 파일을 첨부하여 DB에 데이터를 등록시킨다. tsv 파일이 아닐 경우에는 리턴
	 * @param request
	 * @param type                    : stock : 주식 품목, investor : 주식 매매 정보,
	 *                                todayKeyword : 오늘의 키워드, overtimeKeyword : 시간외
	 *                                급등
	 * @param fileStream
	 * @param contentDispositionHeade
	 * @param date
	 * @return
	 */
	@Path("/{type}/insert")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response inserTodayOROvertimeKeywordFromFile(@Context HttpServletRequest request,
			@PathParam("type") String type, @FormDataParam("file") InputStream fileStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeade,
			@FormDataParam("date") String date) {
		if ((!type.equals("todayKeyword") && !type.equals("overtimeKeyword") && !type.equals("stock")
				&& !type.equals("investorAmount") && !type.equals("investorPrice")) || type == null)
			return Response.status(Status.BAD_REQUEST).build();

		CONFIG_VALUE values = new CONFIG_VALUE(request.getLocalAddr());
		try {
			JSONArray todayOROvertimeKeywordList;
			JSONObject resultObj = new JSONObject();
			JSONArray stockList;
			StockDao stockDao = new StockDao(values);
			LocalDate localDate;
			boolean result = false;
			if (date == null || date.equals("")) {
				localDate = LocalDate.now();
			} else {
				localDate = LocalDate.parse(date);
			}
			if (fileStream != null) {
				FileUtil fileUtil = new FileUtil();
				String extension = fileUtil.extractFilenameExtension(contentDispositionHeade.getFileName());
				if (!extension.equals("txt") && !extension.equals("tsv") && !extension.equals("csv"))
					return Response.status(Status.BAD_REQUEST).build();

				switch (type) {
				case "todayKeyword":
					todayOROvertimeKeywordList = fileUtil.getTodayKeywordList(fileStream);
					result = stockDao.insertTodayKeywordList(todayOROvertimeKeywordList, localDate);
					break;
				case "overtimeKeyword":
					todayOROvertimeKeywordList = fileUtil.getOvertimeKeywordList(fileStream);
					result = stockDao.insertOvertimeKeywordList(todayOROvertimeKeywordList, localDate);
					break;
				case "stock":
					// 주식 품목 정보를 JSONArray 로 가져옴
					stockList = fileUtil.getStockList(fileStream);
					result = stockDao.insertStock(stockList);
					break;
				case "investorAmount": // investorAmount
					resultObj = fileUtil.getInvestorAmountList(fileStream);
					result = stockDao.insertInvestorAmount(resultObj, localDate);
					break;
				default: // investorPrice
					resultObj = fileUtil.getInvestorPriceList(fileStream);
					result = stockDao.insertInvestorPrice(resultObj, localDate);
					break;
				}

				if (result) {
					return Response.ok(resultObj.toString()).build();
				} else {
					// 디비에 추가된 데이터가 하나도 없을 시 500 에러
					return Response.serverError().build();
				}
			} else {
				// 첨부된 파일이 null 일 경우 400
				return Response.status(Status.BAD_REQUEST).build();
			}
		} catch (FailInsertException e) {
			// 쿼리 실행시 에러 발생했을 경우 406
			// type 과 파일 종류가 맞지 않았을때 발생
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (UnknownFilenameExtensionException e) {
			return Response.serverError().build();
		} catch (FailSelectException e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	/**
	 * @author :정준형(jhj@rinasoft.co.kr)
	 * @since :2021. 5. 10.
	 * @description : 세션 객체에 저장된 정보랑 대조하여 유효한 요청인지 검사하는 부분
	 * @param request
	 * @param userId
	 * @return
	 */
	private String checkValidation(HttpServletRequest request, int userId) {
		HttpSession session = request.getSession();
		Boolean isLogin = (Boolean) session.getAttribute("isLogin");
		Integer id = (Integer) session.getAttribute("id");

		if (isLogin == null) { // 로그인이 되어있지 않을 경우
			return "noLogin";
		} else if (!id.equals(userId)) { // 유효하지 않을 경우
			return "noValid";
		} else {
			return "valid";
		}
	}
}
